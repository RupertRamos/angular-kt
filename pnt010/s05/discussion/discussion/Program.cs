﻿using System;
using System.Xml.Linq;

namespace discussion
{
    class Discussion
    {

        static void Main(string[] args)
        {

            // [Section] Encapsulation
            // Encapsulation is the process of enclosing one or more items within a physical or logical package.
            // It prevents access to implementation details
            // Using the method below to define information to be used in the app is tasking and creates confusion

            /*
            string petAName = "Franky";
            string petAGender = "Female";
            string petAClassification = "Dog";
            int petAAge = 10;
            string petAAddress = "Manila, Philippines";
            string petASound = "Bark!";

            DescribePet(petAName, petAGender, petAClassification, petAAge, petAAddress);
            MakeSound(petAName, petASound);

            string petBName = "Simba";
            string petBGender = "Male";
            string petBClassification = "Lion";
            int petBAge = 1;
            string petBAddress = "Pride Lands";
            string petBSound = "Rawr!";

            DescribePet(petBName, petBGender, petBClassification, petBAge, petBAddress);
            MakeSound(petBName, petBSound);
            */

            Pet petA = new Pet("Franky", "Female", "Dog", 10, "Manila, Philippines", "Bark!");

            petA.DescribePet();
            petA.MakeSound();

            Pet petB = new Pet("Simba", "Male", "Lion", 1, "Pride Lands", "Rawr!");

            petB.DescribePet();
            petB.MakeSound();

            // [Section] Abstraction
            // Abstraction is the process where all logic and code complexity is hidden from the users/developers which helps reduce confusion and allows them to focus on the "what" of things rather than the "how"
            // Objects cannot be instantiated from interfaces
            Casio myCalculator = new Casio("Casio", "500");
            myCalculator.Compute(23, 45, "divide");

            Sharp myOtherCalculator = new Sharp("Sharp", "1000");
            myOtherCalculator.Compute(23, 45, "divide");

            useCalculator(myCalculator);
            useCalculator(myOtherCalculator);
    }

        /*
        public static void DescribePet(string name, string gender, string classification, int age, string address)
        {
            Console.WriteLine($"{name} is a {gender} {classification} who is {age} years of age and lives in {address}.");
        }

        public static void MakeSound(string name, string sound)
        {
            Console.WriteLine(name + " says " + sound);
        }
        */

        public static void useCalculator(ICalculator calculator)
        {
            calculator.Compute(23, 45, "divide");
        }
    }
}