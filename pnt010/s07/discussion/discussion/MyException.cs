﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace discussion
{
    // In cases where the pre-built exceptions of C# are inappropriate or whenever a customized exception is need in an application, user defined exceptions can be useful.
    internal class MyException : Exception
    {
        // The ": base(message)" overwrites the parent classes "message" field
        public MyException(string message): base(message) {
        }
    }
}
