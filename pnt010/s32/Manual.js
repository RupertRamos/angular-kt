/*
================
   TypeScript
================
*/

/*

References:
	Install TypeScript
		https://www.typescriptlang.org/download
	TypeScript Handbook
		https://www.typescriptlang.org/docs/handbook/intro.html
	VS Code Integrated Terminal
		https://code.visualstudio.com/docs/editor/integrated-terminal
	VS Code Keyboard Shortcuts
		https://code.visualstudio.com/shortcuts/keyboard-shortcuts-macos.pdf
	TypeScript Generics
		https://www.typescriptlang.org/docs/handbook/2/generics.html
	TypeScript Objects
		typescriptlang.org/docs/handbook/2/objects.html
	TypeScript Interfaces vs Type Alias
		https://pawelgrzybek.com/typescript-interface-vs-type/
	TypeScript Union Types
		https://www.typescriptlang.org/docs/handbook/2/everyday-types.html#union-types
	TypeScript enum
		https://www.typescriptlang.org/docs/handbook/enums.html
	TypeScript Tutorial
		https://www.youtube.com/watch?v=NjN00cM18Z4

Definition of terms:
	Batch Folder - The batch folder that will store all files of the students throughout the bootcamp. Normally located in the "Documents" folder of the device.
	Application - Root folder for the application

*/

/*
==========
Discussion
========== 
*/

/*
1. Install TypeScript.
	Terminal
*/

		npm install -g typescript

		/*
		Important Note:
			- Node JS will be required to install TypeScript.
			- Refer to "references" section of this file to find the documentation for Install Typescript.
		*/

/*
2. Check if TypeScript is installed properly.
	Terminal
*/

		npx tsc

/*
3. Create an "script.ts" file.
	Application > script.ts
*/

		function hello() {
			console.log("Hello World");
		}

		hello();

/*
4. Compile the TypeScript file into a JavaScript File.
	Terminal
*/

		/*
		Syntax
			- tsc inputFileName
		*/
		tsc script.ts

		tsc.cmd script.ts

		tsc script.ts --watch

		tsc.cmd script.ts

		/*
		Important Note:
			- This will generate an script.js file using the compiled TypeScript file.
			- Typescript compiles the code into ES5 syntax. This is true seeing that the "let" variable declared in the "script.ts" file is converted into "var" in the "script.js" file.
			- Always compile TypeScript code by running this command to ensure the changes are applied in the "script.js" file.
			- This session will be discussed using VSCode for fluidity of transition into Angular JS in the future.
			- VS Code has it's own terminal and this may be opened via the the VS Code menu:
				Terminal > New Terminal
			- Keyboard shortcuts may also be used to open the terminal.
			- Refer to "references" section of this file to find the documentations for TypeScript Handbook, VS Code Integrated Terminal and VS Code Keyboard Shortcuts.
		*/

/*
5. Create a variable and re-assign it's value.
	Application > script.ts
*/
		
		/*...*/

		hello();

		let myNumber = 1;
		myNumber = "hello";

		/*
		Important Note:
			- Typescript is a superset of JavaScript meaning that all JavaScript code is available in TypeScript but with access to newer features that help in developing an app.
			- The code above would not return any errors in the context of JavaScript. But when the code is compiled, Typescript will return an error stating that the data type of string cannot be assigned to a number value. The code will still run properly despite the error.
		*/

/*
6. Add an annotation to the variable and create additional variables for the other data types.
	Application > script.ts
*/

		/*...*/

		hello();

		/*
		[Section] Annotations
		- Helps in code readability and helps ensure that the data types provided to variables will conform to the data type declared
		- JavaScript's typeof operater would come in handy when dealing with JavaScript to determine the data type of a value
		- Syntax
		    let variableName: dataType;
		*/
		let myNum: number;
		let myString: string;
		let myBoolean: boolean;
		let unknownVar: any;
		let myArray: number[] = [1, 2, 3];
		let otherArray: any[] = ["John", 31, true];
		myString = "hello";

		console.log("Result of annotations:")
		console.log(typeof myString);
		console.log(typeof myArray);

/*
7. Add more code to demonstrate annotations with functions.
	Application > script.ts
*/

		/*...*/
		console.log(typeof myArray);

		/*
		[Section] Function Annotations
		- Annotations can also be applied to a function's parameters and the return value
		- Parameter annotations may be declared without providing return annotations and vice versa
		- Syntax
		    function functionName(parameter: paramAnnotation): returnAnnotation {
		        return 
		    }
		*/

		// Annotations applied to parameters only
		function sayMyName(name: string) {
		    return 1;
		}

		console.log("Result of function annotations:");
		sayMyName(1);

		// Annotations applied to return values
		// function sayMyName(name: string): string {
		//     return name;
		// }

		let myName = sayMyName("John");
		console.log(myName);

		/*
		Important Note:
			- Comment out the "sayMyName" function invokation after demonstraing that functions without return annotations can return any data type to avoid any future errors during compile time. 
		*/

/*
8. Create a function to demonstrate the use of generics.
	Application > script.ts
*/
	
		/* 8a. Create a function then provide it with an argument of data type string. */

			/*...*/
			console.log(myName);

			/*
			[Section] Generics
			- Allows to create a component that can work over a variety of types rather than a single one
			- Generics are good to use if you have "some" idea of what the data type could be when combined with functions
			Syntax
			    Functions
			        function functionName <genericName>(paramName: genericName) {
			            return
			        }
			- The "genericName" can be given any name where "T" is commonly used to indicate "Type"
			*/

			function genericFunction <T>(param: T) {
			    return param;
			}

			// function genericFunction <Hello>(param: Hello) {
			//     return param;
			// }

			console.log("Result of generics:");
			let result = genericFunction("cooper");
			console.log(result);

			/*
			Important Note:
				- Remember to comment out the "result = genericFunction(1)" code line to prevent any compilation errors.
				- Refer to "references" section of this file to find the documentation for TypeScript Generics.
			*/

		/* 8b. Invoke the previously created function then provide it with an argument of data type number. */

			/*
			[Section] Generics
			- Allows to create a component that can work over a variety of types rather than a single one
			- Generics are good to use if you have "some" idea of what the data type could be when combined with functions
			Syntax
			    Functions
			        function functionName <genericName>(paramName: genericName) {
			            return
			        }
				Arrays
				    type typeAlias = Array<genericName>;
				    let variableName: typeAlias = value;
			- The "genericName" can be given any name where "T" is commonly used to indicate "Type"
			*/

			/*...*/
			console.log(result);
			
			// The genericFunction was initially passed a number data type and returns an error that it cannot be assigned a string value
			result = genericFunction("cooper");

			/*
			Important Note:
				- Remember to comment out the "result = genericFunction("cooper")" code line to prevent any compilation errors. 
			*/

		/* 8b. Create an array using generics. */

			/*...*/
			result = genericFunction("cooper");

			// Other uses of Generics
			type numArray = Array<number>;
			let myNumArray: numArray = [1, 2, 3];

/*
9. Add more code to demonstrate interfaces and type aliases.
	Application > script.ts
*/

		/* 9a. Create a user object and console log to create a scenario where you would be using an object's property. */

			/*...*/
			let myNumArray: numArray = [1, 2, 3];

			const user = {
			    name: "John Smith",
			    age: "31"
			}

			let addedAge = 10 + user.age;
			console.log("In ten years your age would be: " + addedAge);

		/* 9b. Add an interface to the user object. */

			/*...*/
			let myNumArray: numArray = [1, 2, 3];

			/*
			[Section] Interfaces and Type alias
			- Interfaces exclusively represent the shape of an object-like data structure
			- Syntax
			    interface interfaceName {
			        property: value;
			    }
			- The "?" in an interface denotes an optional property meaning that it may/may not be supplied
			- Syntax
			    interface interfaceName {
			        property?: value;
			    }
			*/
			interface User {
			    name: string;
			    age: number;
			    address?: string;
			};

			// const user = {
			//     name: "John Smith",
			//     age: "31"
			// }

			const user: User = {
			    name: "John Smith",
			    age: 31
			};

			let addedAge = 10 + user.age;
			/*...*/

			/*
			Important Note:
				- In the example above, our console log will properly print out the correct age value of our user ensuring that the data type of "age" is a number.
				- Changing the number into a string by adding double quotes ("") will return an error during compilation.
				- Also highlight that since the interface is only used for type checking, it is not converted into JavaScript code maintaining code efficiency but improving development by capturing errors during compile time.
				- Refer to "references" section of this file to find the documentation for TypeScript Objects.
			*/

		/* 9c. Add a type alias. */

			/*
			[Section] Interfaces and Type alias
			- Interfaces exclusively represent the shape of an object-like data structure
			- Syntax
			    interface interfaceName {
			        property: value;
			    }
			- Type aliases can create names for all kinds of data types
			- Every type declaration that can be expressed with an interface can be done with type aliases and the reverse cannot be achieved
			- Syntax
			    type typeAlias {
			        property: value;
			    }
			*/
			// interface User {
				/*...*/
			// };

			type User = {
			    name: string;
			    age: number;
			    address?: string;
			};

			// const user = {
				/*...*/
			// }

			const user: User = {
			    name: "John Smith",
			    age: 31
			};

			let addedAge = 10 + user.age;
			console.log("In ten years your age would be: " + addedAge);

			// Functions with interfaces and types
			function printUser(): User {
			    let myUser: User = {
			        name: "Jane",
			        age: 18
			    }

			    return myUser;
			}

			console.log("Result of functions with interfaces and types:");
			console.log(printUser());

			/*
			Important Note:
				- Using a type alias instead of an interface will not return an error.
				- Refer to "references" section of this file to find the documentation for TypeScript Interfaces vs Type Alias.
			*/

		/* 9d. Add another type alias to demonstrate usage with other data types. */

			/*...*/
			console.log("In ten years your age would be: " + addedAge);

			type message = string;
			let myMessage : message = "Hello Again";
			console.log("Result of type alias: " + myMessage);

			/*...*/

/*
10. Add another array to demonstrate generics with interfaces and type aliases.
	Application > script.ts
*/		

		/*...*/
		console.log("In ten years your age would be: " + addedAge);

		// Generics with interfaces and types
		let users: Array<User> = [
		    {
		        name: "Joe",
		        age: 25
		    }
		]

		console.log("Result from generics with interfaces and types:");
		console.log(users);

		type message = string;
		/*...*/

/*
11. Refactor the previously created type alias to implement a union type.
	Application > script.ts
*/

		/*...*/
		console.log(users);

		/*
		[Section] Union Type
		- Combines different types and is a type formed from two or more data types
		- Data types defined in a union type are referred to as the union's members
		- Union Types can also be used to define a set of string or number literals that the value of a variable must be
		- Syntax
		    type typeAlias = unionMemberA | unionMemberB
		*/
		type message = string | number;
		let myMessage : message = 12;
		console.log("Result of type alias: " + myMessage);

		type LockState = "locked" | "unlocked";
		// let myLock: LockState = "broken";
		let myLock: LockState = "locked";

		type favoriteNumbers = 24 | 11 | 29;
		// let myFavNumbers: favoriteNumbers = 99;
		let myFavNumbers: favoriteNumbers = 11;

		/*
		Important Note:
			- Refer to "references" section of this file to find the documentation for TypeScript Union Types.
		*/

/*
========
Activity
========
*/

/*
1. Create an "activity" folder, an "index.html" file inside of it and link the "script.js" file.
	Batch Folder > S14 > activity > index.html
*/

		/*
		<!DOCTYPE HTML>
		<html>
		    <head>
		        <title>TypeScript</title>
		    </head>
		    <body>
		        <script src="./script.js"></script>
		    </body>
		</html>
		*/

/*
2. Create a "script.ts" file and create a type alias and and an array of staffIds.
	Application > script.ts
*/

		type employees = Array<number>;
		let staffIds: employees = [1, 2, 3];

/*
2. Create an interface for an "employee".
	Application > script.ts
*/

		/*...*/
		let staffIds: employees = [1, 2, 3];

		interface Employee {
		    id: number;
		    name: string;
		    position: string;
		    address?: string;
		};

/*
3. Create a function "checkEmployment" and a variable "message" inside of it.
	Application > script.ts
*/

		/*...*/

		interface Employee {
		    /*...*/
		};

		function checkEmployment(employee: Employee):string {
		    let message: string;

		    return message;
		};

/*
4. Create a conditional statement that if the message is "undefined" alert the user that they are not an employee.
	Application > script.ts
*/

		/*...*/

		function checkEmployment(employee: Employee):string {
		    let message: string;
		    console.log(typeof message);

		    if (typeof message === "undefined"){
	            message = "You are not an employee.";
	        };

		    return message;
		};

/*
5. Create a loop that will check if the employee's id matches a value found inside the "staffIds" array.
	Application > script.ts
*/

		/*...*/

		function checkEmployment(employee: Employee):string {
		    /*...*/
		    console.log(typeof message);

		    staffIds.forEach((id) => {
	            console.log("Checking if employee credentials are valid:");
	            console.log(id === employee.id);
	            if (id === employee.id) {
	                message = "Welcome " + employee.name + ". You are clocked in. Enjoy your day!";
	            }
	        });

		    if (typeof message === "undefined"){
	            /*...*/
	        };

		    /*...*/
		};

/*
6. Create a "manager" variable that will implement the Employee interface.
	Application > script.ts
*/

		/*...*/

		function checkEmployment(employee: Employee):string {
		    /*...*/
		};

		let manager: Employee = {
		    id: 2,
		    name: "John",
		    position: "CEO"
		};

/*
7. Invoke the "checkEmployment" function and console log the result.
	Application > script.ts
*/

		/*...*/

		let manager: Employee = {
		    /*...*/
		};

		console.log(checkEmployment(manager));