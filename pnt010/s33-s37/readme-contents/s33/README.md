# Session Objectives

At the end of the session, the students are expected to:

- learn how Angular works by setting up the basics of an Angular project.

# Resources

## Instructional Materials

- [GitLab Repository](https://gitlab.com/zuitt-coding-bootcamp-curricula/corporate-courses/pointwest/pnt010/-/tree/master/s33-s37)
- [Google Slide Presentation](https://docs.google.com/presentation/d/1dlj0XnSkgpE2p8i5ou4RRv4grS2h0IbTu9JpVnR7QVg/edit)

## Supplemental Materials

- [Angular Decorators (Dot Net Tutorials)](https://dotnettutorials.net/lesson/angular-decorators)
- [Angular Component Lifecycle](https://angular.io/guide/lifecycle-hooks)

# Lesson Proper

## What is Angular?

Angular is a JavaScript framework written in TypeScript developed and maintained by Google.

A framework in programming provides a consistent structure so that developers do not have to write the code of their projects from scratch.

As a platform, Angular includes:

- a component-based framework for building scalable web applications;
- a collection of well-integrated libraries that cover a wide variety of features, including routing, forms management, client-server communication, and more; and
- a suite of developer tools to help you develop, build, test, and update your code.

Angular is different from AngularJS. Angular is the current version based on TypeScript while AngularJS was its previous major iteration where it is based on plain JavaScript.

## Angular Alternatives

These are the alternatives to Angular:

- React.js
- Vue.js
- Ember.js
- Polymer

## Why use Angular?

Here are some of the reasons why you would want to consider using Angular:

- **Component-Based**. Like the alternatives, Angular is component-based which makes the creation of reusable user interface elements easy to use.
- **Data Binding**. The way Angular implements its data binding between the template (HTML code) and component (TypeScript code) is easier compared to other alternatives.
- **Supported by Google**. You can be assured that the development community for Angular will have your back if you ever have inquiries on how to use the framework.

## Who uses Angular?

A lot of companies use Angular for their applications:

- Google
- Forbes
- Paypal
- Deutsche Bank
- UpWork

You can also see websites made in Angular from [madewithangular.com](https://www.madewithangular.com/).

# Code Discussion

## Setting up Angular

To start using Angular, we must first install the Angular CLI globally in our machines by opening. a terminal and using the following command:

```csharp
npm install -g @angular/cli
```

After the installation is finished, go to your directory of codes and use the following command:

```bash
ng new course-booking-angular
```

The command will ask you for a few things:

```
Would you like to add Angular routing? (y/N) type N
Which stylesheet format would you like to use? choose CSS
```

After that, the command will create the initial files for an Angular project. With for the installation to finish.

## Angular Project Structure

Once the creation of initial files are finished, the command will also create the initial Git repository automatically.

Inside the project folder, most of our code will be held inside the **src** folder.

Inside the **src** folder, there are a number of important files and folders:

- **app** - a folder that will contain everything related to the Angular codebase such as components, templates, directives and services.
- **assets** - a folder where other resources can be found such as images.
- **environments** - a folder that contains the environment variables once the Angular project is started.
- **index.html** - the entry file for the whole Angular project.
- **main.ts** - the file responsible for injecting the whole Angular project into the webpage.

## Starting the Angular App

To start the Angular app, execute the `ng serve` command while inside the root folder of the project. Once started, we will be greeted with the following view.

![Untitled](readme-images/Untitled.png)

At this point, we have completed the setup of our first Angular app.

## The app.module.ts File

Under the **src/app** folder is a file named **app.module.ts**. The file has the following initial contents.

```tsx
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
```

This is where the Angular project starts. The file defines everything that an Angular project needs to get started and running.

There are some imports already in the file:

- **NgModule** - a decorator used to define the declarations and imports that can be used within the Angular project.
- **BrowserModule** - provides services that are essential to launch and run a browser application.
- **AppComponent** - the initial component that contains the code for the view we saw when we started the Angular project.

Decorators are the features of Typescript and are implemented as functions. The name of the decorator starts with **@** symbol following by brackets and arguments. That means in angular whenever you find something which is prefixed by **@** symbol, then you need to consider it as a decorator.

The decorator provides metadata to angular classes, property, value, method, etc. and decorators are going to be invoked at runtime.

## The app.component Files

In the **src/app** folder, There are other files aside from the **app.module.ts**:

- app.component.css - a file that contains the styling for the AppComponent.
- app.component.html - a file that contains the actual elements visible in the browser.
- app.component.spec.ts - a file responsible for doing the code testing for the AppComponent once testing is executed.

All these four files (including the base .ts file) make up the single AppComponent.

## Changing the Boilerplate Template

Open the **app.component.html**, remove all of its contents and write the following code inside:

```html
<h1>Online Course Booking</h1>
<p>Opportunities for everyone, everywhere!</p>
<button>Enroll Now!</button>
```

Unlike regular HTML files, Angular templates do not need to have the `<html>`, `<head>` and `<body>` tags declared since all of a template's content are injected to the **index.html** file found at the root project folder.

Once the code has been written, go to [localhost:4200](http://localhost:4200) and check the output.

![Untitled](readme-images/Untitled%201.png)

## Creating Your First Angular Component

Now, we will surely not put everything inside the **app.component.html**. For that reason, we need to create our first Angular component.

To create a component, execute the `ng generate component components/banner` command in the terminal. After that, a folder named **components** will be created and inside it will be a folder named **banner** with a group of files inside it.

![Untitled](readme-images/Untitled%202.png)

It is good practice to separate different parts of the Angular codebase into folders. What we just did is to create a component inside the **components** folder. This is also where we will create all the other components later in the course.

By default, the **banner.component.html** file contains the following code:

```html
<p>banner works!</p>
```

Remove the code of **banner.component.html** then migrate the code from **app.component.html**.

Then, change the code in **app.component.html** into the following:

```html
<app-banner></app-banner>
```

The output of the changes should be the same when viewed from the browser.

Though, you might be wondering how Angular was able to determine that `<app-banner>` is equals to BannerComponent.

This is because in the **banner.component.ts** file, there a `@Component` decorator that has a `selector` property and a value of `app-banner`. Because of that, we can use that like an HTML tag when writing code in other template files.

```tsx
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-banner',
    templateUrl: './banner.component.html',
    styleUrls: ['./banner.component.css']
})
export class BannerComponent implements OnInit {
    constructor() { }

    ngOnInit(): void { }
}
```

## Angular Component Lifecycle

To make it brief and simple, an Angular component lifecycle is composed of 3 processes:

1. **INSTANTIATION** 
- Angular instantiates the component class and renders the component view along with its child views

2. **CHANGE DETECTION**
- Angular checks to see when data-bound properties change, and updates both the view and the component instance as needed.

3. **DESTRUCTION**
- Angular destroys the component instance and removes its rendered template from the DOM.

Directives have a similar lifecycle, as Angular creates, updates, and destroys instances in the course of execution.

To tap into these key events in the component lifecycle, Angular uses HOOK METHODS. For further reading and understanding of these hook methods, you may refer to https://angular.io/guide/lifecycle-hooks.

Going back to our first component **banner.component.ts**, by default, when a component is generated, angular implements the `OnInit` interface of the component together with its corresponding hook method, `ngOnInit()`. When implementing a new interface, always follow the 3 steps:
1. Import it in the class
2. Extend the implements interface
3. Implement the hook method: `ng<Interface name>()`

Demonstrate how to implement a new interface, but make sure to REMOVE IT FROM THE CODE ONCE DONE.
For this demo, we will implement `OnChanges` interface in the `BannerComponent` (**banner.component.ts**).

Output should be:

```tsx
import { Component, OnInit, OnChanges } from '@angular/core';   // STEP 1

@Component({
    selector: 'app-banner',
    templateUrl: './banner.component.html',
    styleUrls: ['./banner.component.css']
})
export class BannerComponent implements OnInit, OnChanges {     // STEP 2
    constructor() { }

    ngOnInit(): void { }

    ngOnChanges() { }                                           // STEP 3
}
```

You don't have to implement all (or any) of the lifecycle hooks, just the ones you need.

## Applying Bootstrap 5 Styling

To make our app livelier, let's install Bootstrap 5 by executing the `npm install bootstrap` from within the root project folder.

Once the installation is finished, update the **angular.json** file found in the root project folder:

```json
{
  "$schema": "./node_modules/@angular/cli/lib/config/schema.json",
  "version": 1,
  "newProjectRoot": "projects",
  "projects": {
    "course-booking-angular": {
      "projectType": "application",
      "schematics": { ... },
      "root": "",
      "sourceRoot": "src",
      "prefix": "app",
      "architect": {
        "build": {
          "builder": "@angular-devkit/build-angular:browser",
          "options": {
            "outputPath": "dist/course-booking-angular",
            "index": "src/index.html",
            "main": "src/main.ts",
            "polyfills": "src/polyfills.ts",
            "tsConfig": "tsconfig.app.json",
            "assets": [
              "src/favicon.ico",
              "src/assets"
            ],
            "styles": [
              "./node_modules/bootstrap/dist/css/bootstrap.min.css",
              "src/styles.css"
            ],
            "scripts": [
              "./node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"
            ]
          },
          "configurations": { ... },
        "serve": { ... },
        "test": {
          "builder": "@angular-devkit/build-angular:karma",
          "options": {
            "main": "src/test.ts",
            "polyfills": "src/polyfills.ts",
            "tsConfig": "tsconfig.spec.json",
            "karmaConfig": "karma.conf.js",
            "assets": [
              "src/favicon.ico",
              "src/assets"
            ],
            "styles": [
              "./node_modules/bootstrap/dist/css/bootstrap.min.css",
              "src/styles.css"
            ],
            "scripts": [
              "./node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"
            ]
          }
        }
      }
    }
  },
  "defaultProject": "course-booking-angular"
}
```

Before going back to the browser, stop `ng serve` then start it again to apply the installed package. Once restarted, the look of the elements will have Bootstrap 5 styling to it.

![Elements with Bootstrap 5 styling.](readme-images/Untitled%203.png)

Elements with Bootstrap 5 styling.

![Elements with plain styling.](readme-images/Untitled%204.png)

Elements with plain styling.

After applying Bootstrap to our app, we will also need to update the code in the **banner.component.html** file:

```json
<div class="row mb-4">
    <div class="col text-center">
        <h1>Online Course Booking</h1>
        <p>Opportunities for everyone, everywhere!</p>
        <button class="btn btn-success">Enroll Now!</button>
    </div>
</div>
```

Go to the browser to check the changes.

![Untitled](readme-images/Untitled%205.png)

At this point, we have finished creating our first component.

## Enabling Imports Through Absolute Paths

This part is a preparation to multiple imports to be done in the proceeding sessions.

Later on, we will import a lot of files. To use absolute paths instead of relative paths when importing, we need to update the **tsconfig.json** file found in the root project folder:

```json
/* To learn more about this file see: https://angular.io/config/tsconfig. */
{
  "compileOnSave": false,
  "compilerOptions": {
    "baseUrl": "./",
    "outDir": "./dist/out-tsc",
    "forceConsistentCasingInFileNames": true,
    "strict": true,
    "noImplicitOverride": true,
    "noPropertyAccessFromIndexSignature": true,
    "noImplicitReturns": true,
    "noFallthroughCasesInSwitch": true,
    "sourceMap": true,
    "declaration": false,
    "downlevelIteration": true,
    "experimentalDecorators": true,
    "moduleResolution": "node",
    "importHelpers": true,
    "target": "es2017",
    "module": "es2020",
    "lib": [
      "es2020",
      "dom"
    ],
    "paths": {
        "@environments/*": ["./src/environments/*"],
        "@components/*": ["./src/app/components/*"],
        "@pages/*": ["./src/app/pages/*"],
        "@services/*": ["./src/app/services/*"],
        "@models/*": ["./src/app/models/*"]
    }
  },
  "angularCompilerOptions": {
    "enableI18nLegacyMessageIdFormat": false,
    "strictInjectionParameters": true,
    "strictInputAccessModifiers": true,
    "strictTemplates": true
  }
}
```

We will then use it later to import files using absolute paths.

Finally, we can also change the title of our application in the browser tab by updating the **index.html** file:

```html
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Course Booking</title>
        <base href="/">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/x-icon" href="favicon.ico">
    </head>
    <body>
        <app-root></app-root>
    </body>
</html>
```

# Activity

## Quiz

### Instructions

Answer the quiz form provided by your instructor.

For instructors, create a copy of this [quiz form](https://docs.google.com/forms/d/11UUr2cVlfbco74wZ6T2MZZZmuM1kXPIuZ3xrsSjwCh4/edit) to your own batch kit folder.

### Answers

Who made Angular?

- Google

Angular and AngularJS is the same.

- False

What is the command to install Angular CLI to your machine?

- npm install -g @angular/cli

Which file did we update to allow importing using absolute paths?

- tsconfig.json

Which file did we update to apply Bootstrap styling?

- angular.json

What is the command to create a new Angular project?

- ng new

What is the command to create an Angular component?

- ng generate component

## Coding Exercise

### Instructions

- Create two new components: **navbar** and **highlights**.

### Expected Output

![Untitled](readme-images/Untitled%206.png)

### Solution

**app/components/navbar/navbar.component.html**

```html
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container-fluid">
        <a class="navbar-brand" href="/">Course Booking</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbar-menu" aria-expanded="false">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbar-menu">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link" href="/courses">Courses</a>
                </li>
            </ul>
            <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link" href="/login">Login</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/register">Register</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
```

**app/components/highlights/highlights.component.html**

```html
<div class="row">
    <div class="col-12 col-md-4">
        <div class="card h-100">
            <div class="card-body">
                <h3>Learn from Home</h3>
                <p>Pariatur adipisicing aute do amet dolore cupidatat. Eu labore aliqua eiusmod commodo occaecat mollit ullamco labore minim. Minim irure fugiat anim ea sint consequat fugiat laboris id. Lorem elit irure mollit officia incididunt ea ullamco laboris excepteur amet. Cillum pariatur consequat adipisicing aute ex.</p>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-4">
        <div class="card h-100">
            <div class="card-body">
                <h3>Study Now, Pay Later</h3>
                <p>Ex Lorem cillum consequat ad. Consectetur enim sunt amet sit nulla dolor exercitation est pariatur aliquip minim. Commodo velit est in id anim deserunt ullamco sint aute amet. Adipisicing est Lorem aliquip anim occaecat consequat in magna nisi occaecat consequat et. Reprehenderit elit dolore sunt labore qui.</p>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-4">
        <div class="card h-100">
            <div class="card-body">
                <h3>Be Part of Our Community</h3>
                <p>Minim nostrud dolore consequat ullamco minim aliqua tempor velit amet. Officia occaecat non cillum sit incididunt id pariatur. Mollit tempor laboris commodo anim mollit magna ea reprehenderit fugiat et reprehenderit tempor. Qui ea Lorem dolor in ad nisi anim. Culpa adipisicing enim et officia exercitation adipisicing.</p>
            </div>
        </div>
    </div>
</div>
```

**app/app.component.html**

```html
<app-navbar></app-navbar>
<div class="container mt-3">
    <app-banner></app-banner>
    <app-highlights></app-highlights>
</div>
```