# Session Objectives

At the end of the session, the students are expected to:

- continue the consumption of API endpoints by implementing the Edit and Archive course feature.

# Resources

## Instructional Materials

- [GitLab Repository](https://gitlab.com/zuitt-coding-bootcamp-curricula/corporate-courses/pointwest/pnt010/-/tree/master/s33-s37)
- [Google Slide Presentation](https://docs.google.com/presentation/d/1-4AN0ZksjjxS5NUH65o1esHLnrmFEhZMYrQUs71fNd8/edit)

# Capstone Requirements

- Implement the enroll to course feature.
- If the enrollment is successful, show a success alert.
- If the user is already enrolled to the course, show an error alert.

## Coding Reminders

- Make sure that your code is indented properly.
- Use names that clearly defines a given function or variable.
    - In other words, avoid using unclear names such as **num1**, **x**, **funcA**, etc.

## Expected Output

![Dec-02-2021 10-34-17.gif](readme-images/Dec-02-2021_10-34-17.gif)

## Solution

**user.service.ts**

```tsx
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@environments/environment';

import { User } from '@models/user';
import { SessionService } from '@services/session.service';

@Injectable({
    providedIn: 'root'
})
export class UserService {
    private baseUrl: string = environment.apiUrl + '/users';
    private httpHeaders: HttpHeaders = new HttpHeaders({
        'Authorization': `Bearer ${this.sessionService.getToken()}`
    });

    constructor(
        private http: HttpClient,
        private sessionService: SessionService
    ) { }

    ...

    enroll(courseId: number): Observable<Object> {
        return this.http.post(this.baseUrl + '/enroll', { courseId }, { headers: this.httpHeaders });
    }
}
```

**course.component.ts**

```tsx
...

import { Course } from '@models/course';
import { UserService } from '@services/user.service';
import { CourseService } from '@services/course.service';
import { SessionService } from '@services/session.service';

@Component({
    selector: 'app-course',
    templateUrl: './course.component.html',
    styleUrls: ['./course.component.css']
})
export class CourseComponent implements OnInit {
    @Input() course!: Course;
    @Input() deleteFromView!: Function;

    isAdmin: boolean = false;
    hasToken: boolean = (localStorage.getItem('token') !== undefined);

    constructor(
        private courseService: CourseService,
        private userService: UserService,
        private sessionService: SessionService,
        private router: Router
    ) {
        this.isAdmin = sessionService.getIsAdmin();
    }

    ngOnInit(): void { }

    enroll(): void {
        this.userService.enroll(this.course.id!).subscribe({
            next: this.successfulEnrollment.bind(this), 
            error: this.failedEnrollment.bind(this)
        });
    }

    edit(): void {
        this.router.navigate(['/edit-course/' + this.course.id]);
    }

    archive(course: Course): void {
        Swal.fire({
            title: 'Confirm Action', 
            text: 'Do you really want to archive this course?', 
            icon: 'warning',
            showCancelButton: true
        }).then((result) => {
            if (result.isConfirmed) {
                this.courseService.archive(this.course.id!).subscribe((response: Record<string, any>) => {
                    Swal.fire('Archive Successful', 'The course has been successfully archived.', 'success');
                    this.deleteFromView(course);
                });
            }
        });
    }

    successfulEnrollment(response: Record<string, any>) {
        Swal.fire('Enrollment Successful', 'Enjoy the course!', 'success');
    }

    failedEnrollment(result: Record<string, any>) {
        let data: Record<string, any> = result['error'];

        if (data['result'] === 'already_enrolled') {
            Swal.fire('Enrollment Cancelled', 'You are already enrolled in this course.', 'error');
        }
    }
}
```