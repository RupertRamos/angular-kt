# Session Objectives

At the end of the session, the students are expected to:

- build upon the skill learned in the previous session by continuing the consumption of API endpoints that require authentication via JSON web tokens.

# Resources

## Instructional Materials

- [GitLab Repository](https://gitlab.com/zuitt-coding-bootcamp-curricula/corporate-courses/pointwest/pnt010/-/tree/master/s33-s37)
- [Google Slide Presentation](https://docs.google.com/presentation/d/1ZQxDLnlYMl6CEjnL5v5OssgRsBGONdVa0FhD2jQokd0/edit)

## Supplemental Materials

- [Component Interaction (Angular Docs)](https://angular.io/guide/component-interaction)
- [Adding and Updating Headers (Angular Docs)](https://angular.io/guide/http#adding-and-updating-headers)

# Code Discussion

## Creating a Session Service

Now that we have made the login page component consume an API endpoint. It is time for us to create a service that will hold the user session throughout the app.

Start by executing the command below:

```bash
ng generate service services/session
```

In the newly created **session.service.ts** file, add the following code:

```tsx
import { Injectable, Output, EventEmitter } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class SessionService {
    @Output() hasToken: EventEmitter<boolean> = new EventEmitter();

    constructor() {
        if (localStorage.getItem('token') !== null) {
            this.hasToken.emit(true);
        } else {
            this.hasToken.emit(false);
        }
    }

    getToken(): string {
        return localStorage.getItem('token')!;
    }

    getEmail(): string {
        return localStorage.getItem('email')!;
    }

		getIsAdmin(): boolean {
        return localStorage.getItem('isAdmin')! === 'true';
    }

    setToken(value: string): void {
        this.hasToken.emit(true);
        localStorage.setItem('token', value);
    }

    setEmail(value: string): void {
        localStorage.setItem('email', value);
    }

    setIsAdmin(value: string): void {
        localStorage.setItem('isAdmin', value);
    }

    clear(): void {
        localStorage.clear();
        this.hasToken.emit(false);
    }
}
```

In the code above, we did the following:

- Used the localStorage as the container for our session-related information.
- Created getters and setters to access and modify session-related information.
- Used the `@Output()` and `EventEmitter` to define a variable named `hasToken` of boolean type in which other parts of the Angular app can subscribe for its changes.
- Used the `hasToken.emit()` to simultaneously update its value and notify all subscribed that the value has been updated and redo operations as necessary (such as rerendering or recomputation).

The `@Output()` is a decorator used to send data out from the containing class. The `EventEmitter` is the one responsible for enabling subscription to events that are emitted from the containing class.

When used inside a service, we can implement a session functionality in which our components can use to determine what UI will be shown according to a session value.

## Applying Session Service to Login

We will need to inject the session service to the login page component by going to the **login.component.ts** file and adding the following code:

```tsx
...

import { UserService } from '@services/user.service';
import { SessionService } from '@services/session.service';

...
export class LoginComponent implements OnInit {
    email: string = '';
    password: string = '';

    constructor(
        private sessionService: SessionService,
        private userService: UserService,
        private router: Router
    ) { }

    ...

    successfulLogin(response: Record<string, any>) {
        this.sessionService.setEmail(response['email']);
        this.sessionService.setIsAdmin(response['isAdmin']);
        this.sessionService.setToken(response['token']);
        this.router.navigate(['']).then(() => {
          // Full page reload to ensure cache of localstorage is removed.
          window.location.href = "/";
        });
    }

    ...
}
```

We updated the `successfulLogin()` method to include the setting of email, isAdmin and token session variables. 

If we try logging in with the correct credentials, we should see in the Applications tab of the browser's DevTools that the user information has been to the browser's localStorage via the created session service.

![Untitled](readme-images/Untitled.png)

Aside from that, the routing from login to the homepage should work as usual.

## Conditional Rendering via Emitted Outputs

Having implemented the session service from the login page component, we should update how the navbar component displays the right-side navigations (login, register and logout).

If there is a token in the localStorage, it means a user is logged in and the app should display the logout link. Else, show the login and register links.

To achieve that, we need to update the **navbar.component.ts** file first:

```tsx
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { SessionService } from '@services/session.service';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
    hasToken: boolean = (localStorage.getItem('token') !== null);
    email: String = localStorage.getItem('email')!;

    constructor(
        private sessionService: SessionService,
        private router: Router
    ) {
        sessionService.hasToken.subscribe(hasToken => {
            this.hasToken = hasToken;
            this.email = this.sessionService.getEmail();
        });
    }

    ngOnInit(): void { }

    logout(): void {
        this.sessionService.clear();
        this.router.navigate(['/login']);
    }
}
```

In the code above, we did the following:

- Injected the session service in the navbar component.
- Updated the component's initial `hasToken` variable to look for whether the localStorage has an item named token or not.
- Added the initial `email` variable to the component, which will be displayed if a user is logged in to the app.
- Added logic in the component constructor that will subscribe to the changes on the `hasToken` variable emitted from the session service.
- Updated the `logout()` method to use the `clear()` method from the session service and then reroute to the login page component.

Then, we should also update the line where the user email is supposed to be rendered in the **navbar.component.html** file:

It is in the third line of the code provided below.

```html
<div *ngIf="hasToken; else noToken">
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="user-dropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">{{ email }}</a>
        <ul class="dropdown-menu" aria-labelledby="user-dropdown">
            <li><a class="dropdown-item" (click)="logout()">Logout</a></li>
        </ul>
    </li>
</div>
```

Once updated, we should see the following output:

![Dec-01-2021 18-59-48.gif](readme-images/Dec-01-2021_18-59-48.gif)

## Passing Data Between Components via Interaction

Before proceeding, make sure that you have course records saved in the database.

Sometimes, data retrieved from a parent component needs to be passed down to its children components. Let's demonstrate that by creating first a courses page component using the command below:

```bash
ng generate component pages/courses
```

Then, create another component for a single course:

```bash
ng generate component components/course
```

Once the components are created, let's add the code for the following files:

**course.component.ts**

```tsx
import { Component, OnInit, Input } from '@angular/core';

import { Course } from '@models/course';

@Component({
    selector: 'app-course',
    templateUrl: './course.component.html',
    styleUrls: ['./course.component.css']
})
export class CourseComponent implements OnInit {
    @Input() course!: Course;

    constructor() { }

    ngOnInit(): void {
    }
}
```

**course.component.html**

```html
<div class="card mb-3">
    <div class="card-body">
        <h4 class="card-title">{{ course.name }}</h4>
        <p>{{ course.description }}</p>
        <p>{{ course.price }} PHP</p>
    </div>
</div>
```

To pass data from parent to child component, we defined a variable in the course component with the `@Input()` decorator. Let's see how this will be used in the courses page component.

Start by creating a new service file:

```bash
ng generate service services/course
```

Then add the following code to **course.service.ts** file:

```tsx
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';

import { Course } from '@models/course';

@Injectable({
    providedIn: 'root'
})
export class CourseService {
    private baseUrl: string = environment.apiUrl + '/courses';

    constructor(
        private http: HttpClient
    ) { }

    get(): Observable<Course[]> {
        return this.http.get<Course[]>(this.baseUrl);
    }

    getOne(id: number): void { }

    add(course: Course): void { }

    update(course: Course): void { }

    archive(id: number): void { }
}
```

Add the following code to the **courses.component.ts** file:

```tsx
import { Component, OnInit } from '@angular/core';

import { Course } from '@models/course';
import { CourseService } from '@services/course.service';

@Component({
    selector: 'app-courses',
    templateUrl: './courses.component.html',
    styleUrls: ['./courses.component.css']
})
export class CoursesComponent implements OnInit {
    courses: Course[] = [];

    constructor(
        private courseService: CourseService
    ) {
        this.getCourses();
    }

    ngOnInit(): void { }

    getCourses() {
        this.courseService.get().subscribe((response: Course[]) => {
            this.courses = response;
        });
    }
}
```

Add the following code to the **courses.component.html** file:

```html
<div class="container-fluid">
    <h3>Course Catalog</h3>
    <app-course *ngFor="let course of courses" [course]="course"></app-course>
</div>
```

In the code above, we used a looping directive within the `<app-course>` and for each course in the courses array, we passed along the course object to the `[course]` input of the course component.

Finally, add a new route to the **app.module.ts** file:

```tsx
const appRoutes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'courses', component: CoursesComponent },
    { path: '**', component: NotFoundComponent }
];
```

We should see the following output in the browser:

![Untitled](readme-images/Untitled%201.png)

## Making Conditional Renders on Course Component

Now that we are able to show the courses, we must apply conditional rendering for additional actions the user can take on the page.

If a user is an admin, the add course, edit course and archive course options will be visible. Else, only the enroll option will be shown.

Furthermore, if there is no user logged in, no options will be shown.

To achieve this, we need to add the codes to the following files:

**courses.component.ts**

```tsx
import { Component, OnInit } from '@angular/core';

import { Course } from '@models/course';
import { CourseService } from '@services/course.service';
import { SessionService } from '@services/session.service';

@Component({
    selector: 'app-courses',
    templateUrl: './courses.component.html',
    styleUrls: ['./courses.component.css']
})
export class CoursesComponent implements OnInit {
    courses: Course[] = [];
    isAdmin: boolean = false;

    constructor(
        private courseService: CourseService,
        private sessionService: SessionService
    ) {
        this.getCourses();
        this.isAdmin = sessionService.getIsAdmin();
    }

		...

		onAddCourseClick(): void { }
}
```

**courses.component.html**

```html
<div class="container-fluid">
    <h3>Course Catalog</h3>
    <div *ngIf="isAdmin;" class="mb-3">
        <button class="btn btn-success" (click)="onAddCourseClick()">Add Course</button>
    </div>
    <app-course *ngFor="let course of courses" [course]="course"></app-course>
</div>
```

**course.component.ts**

```tsx
import { Component, OnInit, Input } from '@angular/core';

import { Course } from '@models/course';
import { SessionService } from '@services/session.service';

@Component({
    selector: 'app-course',
    templateUrl: './course.component.html',
    styleUrls: ['./course.component.css']
})
export class CourseComponent implements OnInit {
    @Input() course!: Course;

    isAdmin: boolean = false;
		hasToken: boolean = (localStorage.getItem('token') !== null);

    constructor(
        private sessionService: SessionService
    ) {
        this.isAdmin = sessionService.getIsAdmin();
    }

    ngOnInit(): void { }

    enroll(): void { }

    edit(): void { }

    archive(course: Course): void { }
}
```

**course.component.html**

```html
<div class="card mb-3">
    <div class="card-body">
        <h4 class="card-title">{{ course.name }}</h4>
        <p>{{ course.description }}</p>
        <p>{{ course.price }} PHP</p>
        <div *ngIf="isAdmin;">
            <button type="button" class="btn btn-primary" (click)="edit()">Edit</button>&nbsp;
            <button type="button" class="btn btn-warning" (click)="archive(course)">Archive</button>
        </div>
        <div *ngIf="hasToken && !isAdmin;">
            <button type="button" class="btn btn-primary" (click)="enroll()">Enroll</button>
        </div>
    </div>
</div>
```

After adding the codes, we should see the following outputs:

*Courses View for Non-Logged In Users*

![Untitled](readme-images/Untitled%201.png)

*Courses View for Admin Users*

![Untitled](readme-images/Untitled%202.png)

*Courses View for Non-Admin Users*

![Untitled](readme-images/Untitled%203.png)

## Updating Course Details

After the conditional rendering of action options has been achieved, we can start by implementing the course detail update feature.

Create a new page component by executing the command below:

```bash
ng generate component pages/edit-course
```

Include the newly created component to the app routes in **app.module.ts** file:

```tsx
const appRoutes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'courses', component: CoursesComponent },
    { path: 'edit-course/:id', component: EditCourseComponent },
    { path: '**', component: NotFoundComponent }
];
```

Update the **course.component.ts** file so that the user gets routed to the edit course component when the edit button is clicked:

```tsx
import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

...
export class CourseComponent implements OnInit {
    ...

    constructor(
        private sessionService: SessionService,
        private router: Router
    ) {
        this.isAdmin = sessionService.getIsAdmin();
    }

    ...

    edit(): void {
        this.router.navigate(['/edit-course/' + this.course.id]);
    }
}
```

Before editing the edit course component, we need to update the **course.service.ts** file:

```tsx
...

import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Course } from '@models/course';
import { SessionService } from './session.service';

@Injectable({
    providedIn: 'root'
})
export class CourseService {
    private baseUrl: string = environment.apiUrl + '/courses';
    private httpHeaders: HttpHeaders = new HttpHeaders({
        'Authorization': `Bearer ${this.sessionService.getToken()}`
    });

    constructor(
        private http: HttpClient,
        private sessionService: SessionService
    ) { }

    get(): Observable<Course[]> {
        return this.http.get<Course[]>(this.baseUrl);
    }

    getOne(id: number): Observable<Object> {
        return this.http.get<Course[]>(`${this.baseUrl}/${id}`);
    }

    add(course: Course): void { }

    update(course: Course): Observable<Object> {
        return this.http.put(this.baseUrl + `/${course.id}`, course, { headers: this.httpHeaders });
    }

    archive(id: number): void { }
}
```

In the code above, we have done the following:

- Defined additional HTTP headers which include the Authorization header.
- Refactored the `getOne()` method to request for data on a single course.
- Refactored the `update()` method to request for course update, wherein the second argument was the updated `course` and the third argument was the updated headers, which is required by the API since non-authenticated requests (or requests without an included token) will fail.

After that, we can add the following code to the edit course component files:

**edit-course.component.ts**

```tsx
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';

import { Course } from '@models/course';
import { CourseService } from '@services/course.service';

@Component({
    selector: 'app-edit-course',
    templateUrl: './edit-course.component.html',
    styleUrls: ['./edit-course.component.css']
})
export class EditCourseComponent implements OnInit {
    course: Course = new Course();

    constructor(
        private courseService: CourseService,
        private router: Router,
        private route: ActivatedRoute
    ) {
        let courseId: number = this.route.snapshot.params['id'];

        courseService.getOne(courseId).subscribe((response: Object) => {
            this.course = response;
        });
    }

    ngOnInit(): void { }

    onSubmit(): void {
        this.courseService.update(this.course).subscribe((response: Record<string, any>) => {
            if (response['result'] === 'updated') {
                Swal.fire({
                    title: 'Update Successful',
                    text: 'The course has been updated successfully.',
                    icon: 'success'
                }).then(() => {
                    this.router.navigate(['/courses']);
                });
            }
        });
    }
}
```

The `route` is the one responsible for providing access to the values captured in the URL of the route (e.g. the `:id` found in the edit course route).

**edit-course.component.html**

```html
<div class="row justify-content-center">
    <div class="col-12 col-md-6">
        <h3>Update Course</h3>
        <form (ngSubmit)="onSubmit()">
            <div class="mb-3">
                <label class="form-label">Name</label>
                <input type="text" class="form-control" [(ngModel)]="course.name" name="name" required>
            </div>
            <div class="mb-3">
                <label class="form-label">Description</label>
                <input type="text" class="form-control" [(ngModel)]="course.description" name="description" required>
            </div>
            <div class="mb-3">
                <label class="form-label">Price</label>
                <input type="number" class="form-control" [(ngModel)]="course.price" name="price" required>
            </div>
            <button type="submit" class="btn btn-success">Update</button>
        </form>
    </div>
</div>
```

After adding all the codes, we should have the following output:

![Dec-02-2021 09-03-28.gif](readme-images/Dec-02-2021_09-03-28.gif)

## Archiving a Course

Update the **course.service.ts** file first:

```tsx
... 
export class CourseService {
    ...

    archive(id: number): Observable<Object> {
        return this.http.delete(this.baseUrl + `/${id}`, { headers: this.httpHeaders });
    }
}
```

Then, use the updated method in the **course.component.ts** file:

```tsx
import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

import { Course } from '@models/course';
import { CourseService } from '@services/course.service';
import { SessionService } from '@services/session.service';

...
export class CourseComponent implements OnInit {
    ...

    constructor(
        private courseService: CourseService,
        private sessionService: SessionService,
        private router: Router
    ) {
        this.isAdmin = sessionService.getIsAdmin();
    }

    ...

    archive(course: Course): void {
        Swal.fire({
            title: 'Confirm Action', 
            text: 'Do you really want to archive this course?', 
            icon: 'warning',
            showCancelButton: true
        }).then((result) => {
            if (result.isConfirmed) {
                this.courseService.archive(this.course.id!).subscribe((response: Record<string, any>) => {
                    Swal.fire('Archive Successful', 'The course has been successfully archived.', 'success');
                });
            }
        });
    }
}
```

Before actually using the service method, we used SweetAlert first to confirm the action to be done by the user. If confirmed, the code will execute the method and will show a success alert.

However, if we try to actually archive a course, the UI stays the same but the data has been updated in the database.

![Dec-02-2021 09-10-34.gif](readme-images/Dec-02-2021_09-10-34.gif)

To fix this, we need to add a new method in **courses.component.ts** that will remove the archived course from view:

```tsx
...
export class CoursesComponent implements OnInit {
    ...

    deleteFromView(givenCourse: Course): void {
        this.courses = this.courses.filter(courseEntry => courseEntry !== givenCourse);
    }
}
```

Then, define a new function input in the **course.component.ts** file and use the given function input when the archive action is confirmed:

```tsx
...
export class CourseComponent implements OnInit {
    @Input() course!: Course;
    @Input() deleteFromView!: Function;

    ...

    archive(course: Course): void {
        Swal.fire({
            title: 'Confirm Action', 
            text: 'Do you really want to archive this course?', 
            icon: 'warning',
            showCancelButton: true
        }).then((result) => {
            if (result.isConfirmed) {
                this.courseService.archive(this.course.id!).subscribe((response: Record<string, any>) => {
                    Swal.fire('Archive Successful', 'The course has been successfully archived.', 'success');
                    this.deleteFromView(course);
                });
            }
        });
    }
}
```

Lastly, pass along the method by adding the following code in the **courses.component.html** file:

```html
<div class="container-fluid">
    <h3>Course Catalog</h3>
    <div *ngIf="isAdmin;" class="mb-3">
        <button class="btn btn-success" (click)="onAddCourseClick()">Add Course</button>
    </div>
    <app-course *ngFor="let course of courses" [course]="course" [deleteFromView]="deleteFromView.bind(this)"></app-course>
</div>
```

The `.bind(this)` was added to prevent the context of `this` from changing within the `deleteFromView()` method once it is passed to the course component.

After adding the code, we should now see the expected result when archiving a course:

![Dec-02-2021 09-19-38.gif](readme-images/Dec-02-2021_09-19-38.gif)

# Activity

## Quiz

### Instructions

Answer the quiz form provided by your instructor.

For instructors, create a copy of this [quiz form](https://docs.google.com/forms/d/1_glVKSH9If-fpvonC8c9ZazNFyLIgpF9Cs_4VNYyx70/edit) to your own batch kit folder.

### Answers

Which JavaScript object can be used to access session-related variables?

- localStorage

A decorator used to send data out from the containing class.

- @Output()

A decorator used to accept data from a parent component.

- @Input()

A class that allows the subscription of changes or events happening from a given class.

- EventEmitter

A class that allows the retrieval of variables found in a route URL.

- ActivatedRoute

The header name that contains the token.

- Authorization, authorization

Code used for retaining the "this" context of a function passed from parent to child component.

- .bind(this), bind(this)

Which tab in the browser DevTools can be used to view session variables?

- Application, application

## Coding Exercise

### Instructions

Implement the add course feature by doing the following.

- Create an add course page component.
- Add the component to the app routes with the path `add-course`.
- Provide the name, description and price then submit the add course form.
- After submitting the form, present a success alert then return to the course list after closing the alert.

### Expected Output

![Dec-02-2021 09-29-58.gif](readme-images/Dec-02-2021_09-29-58.gif)

### Solution

**app.module.ts**

```tsx
const appRoutes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'courses', component: CoursesComponent },
    { path: 'edit-course/:id', component: EditCourseComponent },
    { path: 'add-course', component: AddCourseComponent },
    { path: '**', component: NotFoundComponent }
];
```

**course.service.ts**

```tsx
...
export class CourseService {
    ...

    add(course: Course): Observable<Object> {
        return this.http.post(this.baseUrl, course, { headers: this.httpHeaders });
    }

    ...
}
```

**add-course.component.ts**

```tsx
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

import { Course } from '@models/course';
import { CourseService } from '@services/course.service';

@Component({
    selector: 'app-add-course',
    templateUrl: './add-course.component.html',
    styleUrls: ['./add-course.component.css']
})
export class AddCourseComponent implements OnInit {
    course: Course = new Course();

    constructor(
        private courseService: CourseService,
        private router: Router
    ) { }

    ngOnInit(): void { }

    onSubmit(): void {
        this.courseService.add(this.course).subscribe((response: Record<string, any>) => {
            if (response['result'] === 'added') {
                Swal.fire({
                    title: 'New Course Added',
                    text: 'The course has been added successfully.',
                    icon: 'success'
                }).then(() => {
                    this.router.navigate(['/courses']);
                });
            }
        });
    }
}
```

**add-course.component.html**

```html
<div class="row justify-content-center">
    <div class="col-12 col-md-6">
        <h3>Add Course</h3>
        <form (ngSubmit)="onSubmit()">
            <div class="mb-3">
                <label class="form-label">Name</label>
                <input type="text" class="form-control" [(ngModel)]="course.name" name="name" required>
            </div>
            <div class="mb-3">
                <label class="form-label">Description</label>
                <input type="text" class="form-control" [(ngModel)]="course.description" name="description" required>
            </div>
            <div class="mb-3">
                <label class="form-label">Price</label>
                <input type="number" class="form-control" [(ngModel)]="course.price" name="price" required>
            </div>
            <button type="submit" class="btn btn-success">Add</button>
        </form>
    </div>
</div>
```

**courses.component.ts**

```tsx
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

...
export class CoursesComponent implements OnInit {
    ...

    constructor(
        private courseService: CourseService,
        private sessionService: SessionService,
        private router: Router
    ) { ... }

    ...

    onAddCourseClick(): void {
        this.router.navigate(['/add-course']);
    }

    ...
}
```