# Session Objectives

At the end of the session, the students are expected to:

- learn how to extend capabilities of Angular components through the use of services and dependency injection; and
- learn how Angular can consume APIs through its HttpClientModule.

# Resources

## Instructional Materials

- [GitLab Repository](https://gitlab.com/zuitt-coding-bootcamp-curricula/corporate-courses/pointwest/pnt010/-/tree/master/s33-s37)
- [Google Slide Presentation](https://docs.google.com/presentation/d/1IYeHrKS6g1LV47fnTMe-p44aNnJ5TjGC4iSqoffG6YA/edit)

## Supplemental Materials

- [Introduction to Services and Dependency Injection (Angular Docs)](https://angular.io/guide/architecture-services)
- [Communicating with Backend Services using HTTP (Angular Docs)](https://angular.io/guide/http)

# Code Discussion

## What are Services?

The contents in this section come directly from the Angular Docs.

A *service* is a broad category encompassing any value, function, or feature that an application needs. A service is typically a class with a narrow, well-defined purpose. It should do something specific and do it well.

Angular distinguishes components from services to increase modularity and reusability. By separating a component's view-related functionality from other kinds of processing, you can make your component classes lean and efficient.

Ideally, a component's job is to enable the user experience and nothing more. A component should present properties and methods for data binding, in order to mediate between the view (rendered by the template) and the application logic (which often includes some notion of a *model*).

A component can delegate certain tasks to services, such as fetching data from the server, validating user input, or logging directly to the console. By defining such processing tasks in an *injectable service class*, you make those tasks available to any component.

## Creating a User Service

To create a service, we will need to use the following command:

```bash
ng generate service services/user
```

Then, add the following code in the **user.service.ts** file:

```tsx
import { Injectable } from '@angular/core';

import { User } from '@models/user';

@Injectable({
    providedIn: 'root'
})
export class UserService {
    constructor() { }

    login(email: string, password: string): void { }

    register(user: User): void { }

    enroll(courseId: number): void { }
}
```

The added methods represent the requests that will be sent to the server. 

## What is Dependency Injection?

DI is wired into the Angular framework and used everywhere to provide new components with the services or other things they need. 

Components consume services; that is, you can *inject* a service into a component, giving the component access to that service class.

As a basic example, if a file is imported from another file using the `import` keyword, we are *injecting* that file to the one that depends on it.

In the code added earlier, the `User` model class was injected to the `UserService` class since the service class depends on the definition provided by the model class.

## Injecting a Service to Component

To inject a service into a component, we need to update a component's constructor. Go to the **login.component.ts** file and add the following code:

```tsx
import { Component, OnInit } from '@angular/core';

import { UserService } from '@services/user.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    email: string = '';
    password: string = '';

    constructor(
        private userService: UserService
    ) { }

    ngOnInit(): void { }

    onSubmit(): void {
        console.log(this.email);
        console.log(this.password);
    }
}
```

## Implementing Login Through the User Service

In the same **login.component.ts** file, change the code inside the `onSubmit()` method:

```tsx
...

export class LoginComponent implements OnInit {
		...

    onSubmit(): void {
        this.userService.login(this.email, this.password);
    }
}
```

Then, update the `login()` method from the **user.service.ts** file:

```tsx
...

export class UserService {
    constructor() { }

    login(email: string, password: string): void {
        console.log(email);
        console.log(password);
    }

    register(user: User): void { }

    enroll(courseId: number): void { }
}
```

Now, if we test the login form again, it should behave the same way before the new codes were added and we should see the email and password written to the log console.

## Sending a Request to the API using HttpClientModule

After the user service has been created and injected into the login component, it is time to make the actual request to the API.

To achieve this, we need the HttpClientModule provided by Angular. To use the said module, we need to include it first in the **app.module.ts** file:

```tsx
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

...

@NgModule({
    declarations: [...],
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        RouterModule.forRoot(appRoutes)
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
```

Add first the following variable in **src/environments/environment.ts** file:

```tsx
export const environment = {
    production: false,
    apiUrl: 'http://localhost:8080/api'
};
```

We will use the defined `apiUrl` environment variable in our services.

The defined port for this discussion is 8080. If the port used in the API is different, use that port number instead.

After that, add the following code in the **user.service.ts** file:

```tsx
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environments/environment';

import { User } from '@models/user';

@Injectable({
    providedIn: 'root'
})
export class UserService {
    private baseUrl: string = environment.apiUrl + '/users';

    constructor(
        private http: HttpClient
    ) { }

    login(email: string, password: string): void {
        this.http.post(this.baseUrl + '/login', { email, password });
    }

    register(user: User): void { }

    enroll(courseId: number): void { }
}
```

In the code above, we did the following:

- Injected the HttpClient in the constructor and used it for making the actual API request.
- Imported the environment file and used it for defining the base URL that the service will use.
- Made a post request and included the email and password in the request body.

At this point, make sure that the API has a saved user in its database to determine if the API consumption is working as intended. For the purposes of this discussion, we will have two types of users:

- An admin user (admin@gmail.com)
- A regular user (johndoe@gmail.com)

## Using Observables and Subscription

If we try to submit the login form using the code now, nothing will happen in the UI. This is also true if we look in the Network tab in the browser's DevTools, wherein no request was logged in the tab.

To resolve this, we need to use the Observable type from Angular. An observable acts as a handler of asynchronous operations (such as the request we made earlier).

Once an asynchronous operation is defined as an observable, we will then be given the ability to subscribe to the result of the operation once it is finished.

Consider observables as an advanced custom alternative to promises made by the Angular team.

Let's update the **user.service.ts** by adding the following code:

```tsx
...
import { Observable } from 'rxjs';

...

export class UserService {
    ...

    login(email: string, password: string): Observable<Object> {
        return this.http.post(this.baseUrl + '/login', { email, password });
    }

    ...
}
```

Then in the **login.component.ts**, add the following code:

```tsx
export class LoginComponent implements OnInit {
    ...

    onSubmit(): void {
        this.userService.login(this.email, this.password).subscribe((response: Record<string, any>) => {
            console.log(response);
        });
    }
}
```

If we try to login now using the correct credentials, we should see the following result in the log console.

![Untitled](readme-images/Untitled.png)

This means that we have successfully consumed the API using our Angular app.

## Subscribing to Different Responses

If we try to send credentials with the incorrect password, we are given the following result in the console.

![Untitled](readme-images/Untitled%201.png)

If we try to send credentials with the incorrect email, we are given the following result in the console.

![Untitled](readme-images/Untitled%202.png)

These are unhandled errors by our Angular app. The 400 response code means it is a bad request (where the password is incorrect) while the 404 response code means that the resource does not exist (where email provided cannot be found).

To resolve this, we will need to update how the subscribe method is written in the login component.

Before that, let's stop the serving of the Angular app, execute the `npm install sweetalert2` to install the SweetAlert2 package, then start serving the Angular app again. The installed package is used to display better-looking alerts than the default alerts by the browser.

Next, update the **login.component.ts** by adding the following code:

```tsx
import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

import { UserService } from '@services/user.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    email: string = '';
    password: string = '';

    constructor(
        private userService: UserService
    ) { }

    ngOnInit(): void { }

    onSubmit() {
        this.userService.login(this.email, this.password).subscribe({
            next: this.successfulLogin.bind(this), 
            error: this.failedLogin.bind(this)
        });
    }

    successfulLogin(response: Record<string, any>) {
        console.log(response);
    }

    failedLogin(result: Record<string, any>) {
        let data: Record<string, any> = result['error'];

        if (data['result'] === 'incorrect_credentials') {
            Swal.fire('Login Failed', 'You have entered incorrect credentials, please try again.', 'error');
        } else if (data['result'] === 'user_not_found') {
            Swal.fire('Login Failed', 'User does not exist, please try again.', 'error');
        }
    }
}
```

The subscribe method can also take an object that has `next` and `error` properties defined with which method to execute depending on the response.

The `.bind(this)` is added after the specified component methods so that the methods will not lose the context of `this` (which should always be equal to the LoginComponent's definitions).

This time around, if we try out the error scenarios again, we should be given the alert output.

![Untitled](readme-images/Untitled%203.png)

![Untitled](readme-images/Untitled%204.png)

## Routing Programatically

If the login is successful, we must redirect the user to the homepage. To do that, we will need to add the following code in the **login.component.ts** file:

```tsx
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

...

export class LoginComponent implements OnInit {
    ...
		
		constructor(
        private userService: UserService,
        private router: Router
    ) { }

    successfulLogin(response: Record<string, any>) {
        console.log(response);
        this.router.navigate(['']);
    }

    ...
}
```

# Activity

## Quiz

### Instructions

Answer the quiz form provided by your instructor.

For instructors, create a copy of this [quiz form](https://docs.google.com/forms/d/12Ihmym6p0LSGeBXt5Lk1ihgqFA_L10fcipE5gXXy5rw/edit) to your own batch kit folder.

### Answers

A broad category encompassing any value, function, or feature that an application needs.

- service, services

It is typically a class with a narrow, well-defined purpose.

- service, services

A service is only limited to one component.

- False

What is the command used to create services?

- ng generate service

Where do we inject services?

- constructor, constructors

What is the handler used by Angular for asynchronous operations?

- observable, observables

What is the package used to trigger routes programmatically?

- @angular/router

## Coding Exercise

### Instructions

- In the register page component, remove the built-in component models (firstName, lastName, email and password) and use the User model instead.
- Update the template of the register page component to use the user model.
- Update the user service by adding the logic to send a request for registration.
- If the registration is successful, route the user to the login page.
- If the registration is unsuccessful, remain in the register page.

### Expected Output

![Untitled](readme-images/Untitled%205.png)

![Untitled](readme-images/Untitled%206.png)

### Solution

**user.service.ts**

```tsx
...

export class UserService {
    ...

    register(user: User): Observable<Object> {
        return this.http.post(this.baseUrl + '/register', user);
    }

    ...
}
```

**register.component.ts**

```tsx
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

import { User } from '@models/user';
import { UserService } from '@services/user.service';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
    user: User = new User();
    confirmPassword: string = '';

    constructor(
        private userService: UserService,
        private router: Router
    ) { }

    ngOnInit(): void { }

    onSubmit() {
        this.userService.register(this.user).subscribe({
            next: this.successfulRegister.bind(this), 
            error: this.failedRegister.bind(this)
        });
    }

    successfulRegister(result: Record<string, any>) {
        let data: Record<string, any> = result['added'];

        Swal.fire({
            title: 'Registration Successful',
            text: 'You can now login using your new account',
            icon: 'success'
        }).then(() => {
            this.router.navigate(['/login']);
        });
    }

    failedRegister(result: Record<string, any>) {
        let data: Record<string, any> = result['user_already_exist'];

        Swal.fire({
            title: 'Registration Unsuccessful',
            text: 'User already exist. Please use a different email.',
            icon: 'error'
        }).then(() => {
            this.router.navigate(['/register']);
        });
    }
}
```

**register.component.html**

```html
<div class="row justify-content-center">
    <div class="col-12 col-md-6">
        <h3>Register</h3>
        <form (ngSubmit)="onSubmit()">
            <div class="mb-3">
                <label class="form-label">First Name</label>
                <input type="text" class="form-control" [(ngModel)]="user.firstName" name="firstName" required>
            </div>
            <div class="mb-3">
                <label class="form-label">Last Name</label>
                <input type="text" class="form-control" [(ngModel)]="user.lastName" name="lastName" required>
            </div>
            <div class="mb-3">
                <label class="form-label">Email</label>
                <input type="email" class="form-control" [(ngModel)]="user.email" name="email" required>
            </div>
            <div class="mb-3">
                <label class="form-label">Password</label>
                <input type="password" class="form-control" [(ngModel)]="user.password" name="password" required>
            </div>
            <div class="mb-3">
                <label class="form-label">Confirm Password</label>
                <input type="password" class="form-control" [(ngModel)]="confirmPassword" name="confirmPassword" required>
            </div>
            <div class="alert alert-warning" role="alert" [hidden]="user.password == confirmPassword">
                Password and confirm password values do not match.
            </div>
            <button type="submit" class="btn btn-success">Register</button>
        </form>
    </div>
</div>
```