# Session Objectives

At the end of the session, the students are expected to:

- learn how to make dynamic components by creating models, applying the models and other programming concepts via directives and setting up routes.

# Resources

## Instructional Materials

- [GitLab Repository](https://gitlab.com/zuitt-coding-bootcamp-curricula/corporate-courses/pointwest/pnt010/-/tree/master/s33-s37)
- [Google Slide Presentation](https://docs.google.com/presentation/d/18M-s1fhyzkmdDugmdSjUqYVb3X4k1H7GFYneYWBBaUQ/edit)

## Supplemental Materials

- [Managing Data (Angular Docs)](https://angular.io/start/start-data)

# Code Discussion

## What are Models?

Models in programming are a concept wherein it represents a data structure, usually the structure found in databases such as tables and its columns.

By using models, developers can anticipate the data that an application needs to work with.

## Creating Models

To create models, we will need to use the `ng generate class` command in the terminal. Specifically, execute the following commands:

```bash
ng generate class models/user
ng generate class models/course
ng generate class models/course-enrollment
```

After executing the commands above, we should see a new folder named **models** inside the **src/app** folder, along with its user, course and course enrollment class files.

These models will mimic the database structure established in the Spring Boot section of the training. Add the code for the following files:

**user.ts**

```tsx
export class User {
    constructor(
        public id?: number,
        public firstName?: string,
        public lastName?: string,
        public email?: string,
        public password?: string,
        public isAdmin?: boolean
    ) { }
}
```

**course.ts**

```tsx
export class Course {
    constructor(
        public id?: number,
        public name?: string,
        public description?: string,
        public price?: string,
        public isActive?: boolean,
        public datetimeCreated?: Date
    ) { }
}
```

**course-enrollment.ts**

```tsx
export class CourseEnrollment {
    constructor(
        public id?: number,
        public courseId?: number,
        public userId?: number,
        public datetimeEnrolled?: Date
    ) { }
}
```

The question mark specifies that a class property in the constructor can have a null value.

## What are Directives?

In Angular, directives extend the functionality of HTML code by providing it with additional syntax which in turn make the code dynamic.

Angular directives are the equivalent of JSX syntax from React.

## Creating a Conditional Directive

Go to **navbar.component.ts** and add the following code:

```tsx
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
    hasToken: boolean = false;

    constructor() { }

    ngOnInit(): void { }

		logout(): void {
        console.log('Logout button has been clicked.');
    }
}
```

Next, go to **navbar.component.html** and add the following code:

```html
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container-fluid">
        <a class="navbar-brand" href="/">Course Booking</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbar-menu" aria-expanded="false">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbar-menu">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link" href="/courses">Courses</a>
                </li>
            </ul>
            <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                <div *ngIf="hasToken; else noToken">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="user-dropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">User Email</a>
                        <ul class="dropdown-menu" aria-labelledby="user-dropdown">
                            <li><a class="dropdown-item" (click)="logout()">Logout</a></li>
                        </ul>
                    </li>
                </div>
                <div>
                    <ng-template #noToken>
                        <li class="nav-item">
                            <a class="nav-link" href="/login">Login</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/register">Register</a>
                        </li>
                    </ng-template>
                </div>
            </ul>
        </div>
    </div>
</nav>
```

To define a conditional directive, we have used the `*ngIf` syntax. Inside that, we are looking whether `hasToken` is true or false. If it is true, it displays the elements contained in the div element.

However, if the `hasToken` is false, it redirects to a template called `noToken` and defined that template below the div that contained the `*ngIf` statement. The template section is defined using the `ng-template` tag along with the identifier `#noToken` (which can be changed accordingly).

The `hasToken` found in the TypeScript file serves as the built-in component model that the template can use for logic building.

Later on, we will use the models we created earlier inside these components.

We should see the same output in the browser once the code has been saved. If we change the value of `hasToken` from **false** to **true**, we can see the app change its UI upon saving the code.

![Untitled](readme-images/Untitled.png)

## Creating a Looping Directive

Go to **highlights.component.ts** and add the following code:

```tsx
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-highlights',
    templateUrl: './highlights.component.html',
    styleUrls: ['./highlights.component.css']
})
export class HighlightsComponent implements OnInit {
    data: Record<string, string>[] = [
        {
            'title': 'Learn from Home',
            'description': 'Pariatur adipisicing aute do amet dolore cupidatat. Eu labore aliqua eiusmod commodo occaecat mollit ullamco labore minim. Minim irure fugiat anim ea sint consequat fugiat laboris id. Lorem elit irure mollit officia incididunt ea ullamco laboris excepteur amet. Cillum pariatur consequat adipisicing aute ex.'
        },
        {
            'title': 'Study Now, Pay Later',
            'description': 'Ex Lorem cillum consequat ad. Consectetur enim sunt amet sit nulla dolor exercitation est pariatur aliquip minim. Commodo velit est in id anim deserunt ullamco sint aute amet. Adipisicing est Lorem aliquip anim occaecat consequat in magna nisi occaecat consequat et. Reprehenderit elit dolore sunt labore qui.'
        },
        {
            'title': 'Be Part of Our Community',
            'description': 'Minim nostrud dolore consequat ullamco minim aliqua tempor velit amet. Officia occaecat non cillum sit incididunt id pariatur. Mollit tempor laboris commodo anim mollit magna ea reprehenderit fugiat et reprehenderit tempor. Qui ea Lorem dolor in ad nisi anim. Culpa adipisicing enim et officia exercitation adipisicing.'
        }
    ]

    constructor() { }

    ngOnInit(): void {
    }
}
```

Next, go to **highlights.component.html** and add the following code:

```html
<div class="row">
    <div class="col-12 col-md-4" *ngFor="let item of data">
        <div class="card h-100">
            <div class="card-body">
                <h3>{{ item['title'] }}</h3>
                <p>{{ item['description'] }}</p>
            </div>
        </div>
    </div>
</div>
```

To define a looping directive, we have used the `*ngFor` syntax. Inside that, we have defined an iteration holder called `item` while looping through each element in `data`.

Since `data` is declared as `Record<string, string>[]`, we can access each object's property by using the bracket notation.

The `Record` type can be used to define variables with objects that are not declared based on models in the application.

## Setting up Routes for Dynamic View

As our app grows, we need to present different parts of the app at a given time. To do that, we need to implement routes.

First, use the command below to create our first page component:

```bash
ng generate component pages/home
```

Add the following inside the newly-created **home.component.html** file:

```html
<app-banner></app-banner>
<app-highlights></app-highlights>
```

Add the following code in **app.module.ts** file:

```tsx
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { BannerComponent } from './components/banner/banner.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HighlightsComponent } from './components/highlights/highlights.component';
import { HomeComponent } from './pages/home/home.component';

const appRoutes: Routes = [
    { path: '', component: HomeComponent }
];

@NgModule({
    declarations: [...],
    imports: [
        BrowserModule,
        RouterModule.forRoot(appRoutes)
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
```

Finally, add the following code in **app.component.html** file:

```html
<app-navbar></app-navbar>
<div class="container mt-3">
    <router-outlet></router-outlet>
</div>
```

Restart the application and the app should display the same output despite the additional code we wrote to implement the route.

## Adding a Login Route

To make our app really dynamic in showing components, we will need to create a new component by executing the command below:

```bash
ng generate component pages/login
```

Then, in the **login.component.html** file, add the following code:

```html
<div class="row justify-content-center">
    <div class="col-12 col-md-6">
        <h3>Login</h3>
        <form>
            <div class="mb-3">
                <label class="form-label">Email</label>
                <input type="email" class="form-control" name="email" required>
            </div>
            <div class="mb-3">
                <label class="form-label">Password</label>
                <input type="password" class="form-control" name="password" required>
            </div>
            <button type="submit" class="btn btn-success">Login</button>
        </form>
    </div>
</div>
```

After that, update the **app.module.ts** to include the login component to the app routing.

```tsx
const appRoutes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'login', component: LoginComponent }
];
```

If the given path starts with the `/` character, Angular will print to the console the following error:

```
Invalid configuration of route '/login': path cannot start with a slash...
```

Lastly, we also need to update **navbar.component.html** so that the navigation links use the routing system of Angular instead of the default href attribute.

```html
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container-fluid">
        <a class="navbar-brand" routerLink="/">Course Booking</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbar-menu" aria-expanded="false">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbar-menu">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link" routerLink="/courses">Courses</a>
                </li>
            </ul>
            <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                <div *ngIf="hasToken; else noToken">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="user-dropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">{{ email }}</a>
                        <ul class="dropdown-menu" aria-labelledby="user-dropdown">
                            <li><a class="dropdown-item" (click)="logout()">Logout</a></li>
                        </ul>
                    </li>
                </div>
                <div>
                    <ng-template #noToken>
                        <li class="nav-item">
                            <a class="nav-link" routerLink="/login">Login</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" routerLink="/register">Register</a>
                        </li>
                    </ng-template>
                </div>
            </ul>
        </div>
    </div>
</nav>
```

By using the routerLink attribute instead of the href attribute, the webpage does not completely reload and will use the routing system defined in our Angular app.

At this point, instructors should present the routing by going back and forth between the home and login component through the use of navbar navigation links.

## Creating an Event Directive

Go to the **login.component.ts** and let's add a few lines of code:

```tsx
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    email: string = '';
    password: string = '';

    constructor() { }

    ngOnInit(): void { }

    onSubmit(): void {
        console.log(this.email);
        console.log(this.password);
    }
}
```

We have included the `email` and `password` form model variables, as well as the `onSubmit()` method that will be used once the login form is submitted.

Next, go to the **login.component.html** and add the following code:

```html
<div class="row justify-content-center">
    <div class="col-12 col-md-6">
        <h3>Login</h3>
        <form (ngSubmit)="onSubmit()">
            <div class="mb-3">
                <label class="form-label">Email</label>
                <input type="email" class="form-control" [ngModel]="email" (ngModelChange)="email = $event" name="email" required>
            </div>
            <div class="mb-3">
                <label class="form-label">Password</label>
                <input type="password" class="form-control" [ngModel]="password" (ngModelChange)="password = $event" name="password" required>
            </div>
            <button type="submit" class="btn btn-success">Login</button>
        </form>
    </div>
</div>
```

There are three new directives used in the added code:

- `(ngSubmit)` defines which method a form will execute once the form is submitted.
- `[ngModel]` defines which model variable value will be shown to the input field.
- `(ngModelChange)` defines which model variable will the updated input value go to.

For easier recall, the `()` syntax is related to events while the `[]` syntax is related to model usage.

Together with `[ngModel]` and `(ngModelChange)`, we achieve what is called **two-way data binding**, where values shown in the interface and saved in the code are synchronized.

However, we will be shown the following error:

![Untitled](readme-images/Untitled%201.png)

To fix that, let's update the **app.module.ts** and use the FormsModule:

```tsx
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';

...

@NgModule({
    declarations: [...],
    imports: [
        BrowserModule,
        FormsModule,
        RouterModule.forRoot(appRoutes)
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
```

Now, if we try to enter details to the email and password of the login form then submit the form, we should see email and password written to the log console.

## Two-Way Data Binding Shorthand

This section is based from a StackOverflow [answer](https://stackoverflow.com/a/42505026).

Earlier, we defined the model variable to be used in the login form, as well as what the code will do if the input value changes. There is a shorthand version of the two directives used earlier. Let's update the **login.component.html** file:

```html
<div class="row justify-content-center">
    <div class="col-12 col-md-6">
        <h3>Login</h3>
        <form (ngSubmit)="onSubmit()">
            <div class="mb-3">
                <label class="form-label">Email</label>
                <input type="email" class="form-control" [(ngModel)]="email" name="email" required>
            </div>
            <div class="mb-3">
                <label class="form-label">Password</label>
                <input type="password" class="form-control" [(ngModel)]="password" name="password" required>
            </div>
            <button type="submit" class="btn btn-success">Login</button>
        </form>
    </div>
</div>
```

This time around, the `[]` and `()` is used simultaneously and this represents the shorthand syntax for implementing two-way data binding in our form.

## Mini-Activity: Page Not Found

This mini-activity is good for at least 15 to 30 minutes.

### Instruction

- Create a page component named **NotFoundComponent**.
- Update the appRoutes so that the created component will be shown if the user goes to a non-declared route.

### Expected Output

![Untitled](readme-images/Untitled%202.png)

## Creating a Page Not Found Component

What if a user goes to a route that is not defined? We can update the app routes to include a page component that will be shown if a user does go to a non-declared route.

Start by creating a new page component:

```bash
ng generate component pages/not-found
```

Then, add the following code inside **not-found.component.html** file:

```html
<h3 class="text-center">Page Not Found</h3>
<p class="text-center">You requested a page that does not exist in this website, Go back to <a routerLink="/">homepage</a>.</p>
```

Finally, go to **app.module.ts** file and update the app routes:

```tsx
const appRoutes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'login', component: LoginComponent },
    { path: '**', component: NotFoundComponent }
];
```

if a given route does not match any of the defined routes before the added line, it will show the Not Found page. Do note that the added line must always be the last element in the array.

We shall see the following output:

![Untitled](readme-images/Untitled%202.png)

# Activity

## Quiz

### Instructions

Answer the quiz form provided by your instructor.

For instructors, create a copy of this [quiz form](https://docs.google.com/forms/d/19OYA0ECweKMp8T6Zx41L4BQGTxdVG4JFaEUwQMYdKQk/edit) to your own batch kit folder.

### Answers

It represents a data structure.

- model, models

By using it, developers can anticipate the data that an application needs to work with.

- model, models

What is the command used to create a class file?

- ng generate class

Angular extends the functionality of HTML code by using __________.

- directive, directives

What is the syntax used to show a model value in a template?

- {{ }}

What is the syntax used to define an event directive?

- ( )

What is the syntax used for two-way binding shorthand?

- [( )]

Angular app routes require the leading "/" in its values.

- False

Adding the leading "/" to app route path values will not result in an error.

- False

What is the path value used to capture every other path not declared in the app routes?

- **

## Coding Exercise

### Instructions

- Create a page component named RegisterComponent.
- The form must have the following fields:
    - First Name
    - Last Name
    - Email
    - Password
    - Confirm Password
- Upon submitting the login form, print the email and password values to the console.
- Stretch goal: whenever the values of password and confirm password do not match, show an alert below the confirm password field using a conditional directive.

The stretch goal intentionally lets the trainees research how to implement such a feature using directives.

### Expected Output

![Untitled](readme-images/Untitled%203.png)

### Solution

**app/pages/register/register.component.html**

```html
<div class="row justify-content-center">
    <div class="col-12 col-md-6">
        <h3>Register</h3>
        <form (ngSubmit)="onSubmit()">
            <div class="mb-3">
                <label class="form-label">First Name</label>
                <input type="text" class="form-control" [(ngModel)]="firstName" name="firstName" required>
            </div>
            <div class="mb-3">
                <label class="form-label">Last Name</label>
                <input type="text" class="form-control" [(ngModel)]="lastName" name="lastName" required>
            </div>
            <div class="mb-3">
                <label class="form-label">Email</label>
                <input type="email" class="form-control" [(ngModel)]="email" name="email" required>
            </div>
            <div class="mb-3">
                <label class="form-label">Password</label>
                <input type="password" class="form-control" [(ngModel)]="password" name="password" required>
            </div>
            <div class="mb-3">
                <label class="form-label">Confirm Password</label>
                <input type="password" class="form-control" [(ngModel)]="confirmPassword" name="confirmPassword" required>
            </div>
            <div class="alert alert-warning" role="alert" [hidden]="password == confirmPassword">
                Password and confirm password values do not match.
            </div>
            <button type="submit" class="btn btn-success">Register</button>
        </form>
    </div>
</div>
```

**app/pages/register/register.component.ts**

```tsx
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
    firstName: String = '';
    lastName: String = '';
    email: String = '';
    password: String = '';
    confirmPassword: string = '';

    constructor() { }

    ngOnInit(): void { }

    onSubmit() {
        console.log(this.firstName);
        console.log(this.lastName);
        console.log(this.email);
        console.log(this.password);
        console.log(this.confirmPassword);
    }
}
```

**src/app/app.module.ts**

```tsx
const appRoutes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: '**', component: NotFoundComponent }
];
```