<table>
    <tr>
        <th>Session Name</th>
        <th>Link</th>
    </tr>
    <tr>
        <td>S33: Introduction to Angular</td>
        <td><a href="readme-contents/s33/README.md">Link</a></td>
    </tr>
    <tr>
        <td>S34: Models, Directives and Routes</td>
        <td><a href="readme-contents/s34/README.md">Link</a></td>
    </tr>
    <tr>
        <td>S35: Consuming API in Angular</td>
        <td><a href="readme-contents/s35/README.md">Link</a></td>
    </tr>
    <tr>
        <td>S36: Authenticated API Requests</td>
        <td><a href="readme-contents/s36/README.md">Link</a></td>
    </tr>
    <tr>
        <td>S37: Capstone Project</td>
        <td><a href="readme-contents/s37/README.md">Link</a></td>
    </tr>
</table>
