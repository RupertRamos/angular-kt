import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

import { User } from '@models/user';
import { UserService } from '@services/user.service';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
    user: User = new User();
    confirmPassword: string = '';

    constructor(
        private userService: UserService,
        private router: Router
    ) { }

    ngOnInit(): void { }

    onSubmit() {
        this.userService.register(this.user).subscribe({
            next: this.successfulRegister.bind(this), 
            error: this.failedRegister.bind(this)
        });
    }

    successfulRegister(result: Record<string, any>) {
        let data: Record<string, any> = result['added'];

        Swal.fire({
            title: 'Registration Successful',
            text: 'You can now login using your new account',
            icon: 'success'
        }).then(() => {
            this.router.navigate(['/login']);
        });
    }

    failedRegister(result: Record<string, any>) {
        let data: Record<string, any> = result['user_already_exist'];

        Swal.fire({
            title: 'Registration Unsuccessful',
            text: 'User already exist. Please use a different email.',
            icon: 'error'
        }).then(() => {
            this.router.navigate(['/register']);
        });
    }
}