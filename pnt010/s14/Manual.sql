===================================
Advanced Selects and Joining Tables
===================================

References:
	SQL Basic
		https://medium.com/sqlgate/sql-basic-lets-learn-some-advanced-join-queries-80e63b6d6250
	SQL LIKE clause
		https://www.tutorialspoint.com/sql/sql-like-clause.htm
    
Definition of terms:
    Terminal - Terminal where MySQL is accessed

==========
Discussion
==========

1. Run the MySQL/Maria DB.
    Terminal

        Connecting to MySQL:
        Syntax
            - mysql -u username -p password

        mysql -u root -p

        IMPORTANT NOTE:
            - "-u" stands for username.
            - "root" is the default username for sql.
            - "-p" stands for password.
            - An empty value is the default password for sql.

2. Add more artists to expand the data we can use.
	Terminal

		INSERT INTO artists (name) VALUES ("Taylor Swift");
		INSERT INTO artists (name) VALUES ("Lady Gaga");
		INSERT INTO artists (name) VALUES ("Justin Bieber");
		INSERT INTO artists (name) VALUES ("Ariana Grande");
		INSERT INTO artists (name) VALUES ("Bruno Mars");

3. Add several albums and songs.
	Terminal

		============
		Taylor Swift
		============
		Check what the ID of Taylor Swift is:
		SELECT * FROM artists;

		INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Fearless", "2008-11-11", 3);
		INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Red", "2012-10-22", 3);

		Check what the ID of the new albums are:
		SELECT * FROM albums;

		INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Fearless", 246, "Pop rock", 3);
		INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Love Story", 213, "Country pop", 3);
		INSERT INTO songs (song_name, length, genre, album_id) VALUES ("State of Grace", 243, "Rock, alternative rock, arena rock", 4);
		INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Red", 204, "Country", 4);

		=========
		Lady Gaga
		=========
		Check what the ID of Lady Gaga is:
		SELECT * FROM artists;

		INSERT INTO albums (album_title, date_released, artist_id) VALUES ("A Star Is Born", "2018-10-10", 4);
		INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Born This Way", "2011-6-29", 4);

		Check what the ID of the new albums are:
		SELECT * FROM albums;

		INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Black Eyes", 181, "Rock and roll", 5);
		INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Shallow", 201, "Country, rock, folk rock", 5);	
		INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Born This Way", 252, "Electropop", 6);

		=============
		Justin Bieber
		=============
		Check what the ID of Justin Bieber is:
		SELECT * FROM artists;

		INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Purpose", "2015-11-13", 5);
		INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Believe", "2012-6-15", 5);

		Check what the ID of the new albums are:
		SELECT * FROM albums;

		INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Sorry", 132, "Dancehall-poptropical housemoombahton", 7);
		INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Boyfriend", 251, "Pop", 8);

		=============
		Ariana Grande
		=============
		Check what the ID of Ariana Grande is:
		SELECT * FROM artists;

		INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Dangerous Woman", "2016-5-20", 6);
		INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Thank U, Next", "2019-2-8", 6);

		Check what the ID of the new albums are:
		SELECT * FROM albums;

		INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Into You", 242, "EDM house", 9);
		INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Thank U Next", 156, "Pop, R&B", 10);

		==========
		Bruno Mars
		==========
		Check what the ID of Bruno Mars is:
		SELECT * FROM artists;

		INSERT INTO albums (album_title, date_released, artist_id) VALUES ("24K Magic", "2016-11-18", 7);
		INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Earth to Mars", "2011-01-20", 7);

		Check what the ID of the new albums are:
		SELECT * FROM albums;

		INSERT INTO songs (song_name, length, genre, album_id) VALUES ("24K Magic", 207, "Funk, disco, R&B", 11);
		INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Lost", 152, "Pop", 12);

4. Exclude records.
	Terminal

		Excluding records:
		Syntax
			SELECT column_name FROM table_name WHERE column_name != value;

		SELECT * FROM songs WHERE id != 11;
		SELECT * FROM songs WHERE album_id != 5 AND album_id != 6;

		IMPORTANT NOTE:
			- The exclamation point (!) is called the Not Condition/Operator

5. Finding records using comparison operators.
	Terminal

		Displaying/retrieving records with comparison operators:

		SELECT * FROM songs WHERE length > 230;
		SELECT * FROM songs WHERE length < 200;
		SELECT * FROM songs WHERE length > 230 OR length < 200;

6. Getting records with specific conditions.
	Terminal

		IN CLAUSE
		Can be used for querying multiple columns:
		SELECT * FROM songs WHERE id = 1 OR id = 3 OR id = 5;

		Best used for querying multiple values in a single column:
		SELECT * FROM songs WHERE id IN (1, 3, 5);
		SELECT * FROM songs WHERE genre IN ("Pop", "K-pop");

7. Show records with a partial match.
    Terminal

        Displaying/retrieving records with a partial match:
        LIKE CLAUSE

        Find values with a match at the start
        SELECT * FROM songs WHERE song_name LIKE "th%";

        Find values with a match at the end
        SELECT * FROM songs WHERE song_name LIKE "%ce";

        Find values with a match at any position
        SELECT * FROM artists WHERE name LIKE "%run%";

        Find values with a match of a specific length/pattern
        SELECT * FROM songs WHERE song_name LIKE "__rr_";

        Find values with a match at certain positions
        SELECT * FROM albums WHERE album_title LIKE "_ur%";

        IMPORTANT NOTE:
        	- The percent (%) and underscore (_) symbols are called wildcard operators 
            - The percent (%) symbol represents zero or multiple characters
            - The underscore (_) represents a single character
            - Refer to "references" section of this file to find the documentation for SQL LIKE clause.

8. Sorting records.
    Terminal

        Sorting records:
        Syntax
        	SELECT column_name FROM table_name ORDER BY column_name ORDER;
        
        SELECT * FROM songs ORDER BY song_name;
        SELECT * FROM songs ORDER BY song_name ASC;
        SELECT * FROM songs ORDER BY length DESC;

        IMPORTANT NOTE:
        	- The value of the ORDER keyword can be "ASC" for ascending and  "DESC" for descending

9. Limiting Records.
	Terminal

		Limiting records:
		SELECT * FROM songs LIMIT 5;

10. Showing records with distinct values.
    Terminal

        Displaying/retrieving records with distinct values:
        SELECT genre FROM songs;
        SELECT DISTINCT genre FROM songs;

11. Joining two tables.
	Terminal

		Joining two tables:
		Syntax
			SELECT column_name FROM table1
				JOIN table2 ON table1.id = table2.foreign_key_column;

		SELECT * FROM artists 
    		JOIN albums ON artists.id = albums.artist_id;

12. Joining multiple tables.
	Terminal

		Joining multiple tables:
		Syntax
			SELECT column_name FROM table1
				JOIN table2 ON table1.id = table2.foreign_key_column
				JOIN table3 ON table2.id = table3.foreign_key_column; 

		SELECT * FROM artists 
		    JOIN albums ON artists.id = albums.artist_id 
		    JOIN songs ON albums.id = songs.album_id;

13. Selecting colums to be displayed from joining multiple tables.
	Terminal

		SELECT name, album_title, date_released, song_name, length, genre FROM artists 
		    JOIN albums ON artists.id = albums.artist_id 
		    JOIN songs ON albums.id = songs.album_id;

14. Providing aliases for joining tables.
	Terminal

		Syntax
			SELECT column_name AS alias FROM table1
				JOIN table2 ON table1.id = table2.foreign_key_column
				JOIN table3 ON table2.id = table3.foreign_key_column;

		SELECT name AS band, album_title AS album, date_released, song_name AS song, length, genre FROM artists 
		    JOIN albums ON artists.id = albums.artist_id 
		    JOIN songs ON albums.id = songs.album_id;

15. Displaying data from joining tables.
	Terminal

		Create user information, a playlist and songs added to the user playlist:
		INSERT INTO users (username, password, full_name, contact_number, email, address) VALUES ("john", "john1234", "John Doe", 09123456789, "john@mail.com", "New York");

		INSERT INTO playlists (user_id, datetime_created) VALUES (1, "2021-01-02 01:00:00");

		INSERT INTO playlists_songs (playlist_id, song_id) VALUES (1, 1), (1, 10), (1, 11);

		Joining multiple tables:
		SELECT * FROM playlists
			JOIN playlists_songs ON playlists.id = playlists_songs.playlist_id
			JOIN songs ON playlists_songs.song_id = songs.id;

		Selecting specific columns to be displayed from the query:
		SELECT user_id, datetime_created, song_name, length, genre, album_id FROM playlists
			JOIN playlists_songs ON playlists.id = playlists_songs.playlist_id
			JOIN songs ON playlists_songs.song_id = songs.id;


		Joining multiple columns to complete all the related data:
		SELECT full_name, datetime_created, song_name, length, genre, album_title FROM playlists
			JOIN users ON playlists.user_id = users.id
			JOIN playlists_songs ON playlists.id = playlists_songs.playlist_id
			JOIN songs ON playlists_songs.song_id = songs.id
			JOIN albums ON songs.album_id = albums.id;

========
Activity
========

1. Find all "artists" that has letter "D" in its "name".
	Terminal

		SELECT * FROM artists WHERE name LIKE "%d%";

2. Find all "songs" that has a "length" of less than "230" and with the "genre" of "rock".
	Terminal

		SELECT * FROM songs WHERE length < 230 AND genre LIKE "%rock%";

3. Join the "albums" and "songs" tables. (Only show the "album name", "song name", and "song length".)
	Terminal

		SELECT album_title, song_name, length FROM albums 
			JOIN songs ON albums.id = songs.album_id;

4. Join the "artists" and "albums" tables. (Find all "albums" that has the string "ar" in its "name".)
	Terminal

		SELECT * FROM artists 
			JOIN albums ON artists.id = albums.artist_id 
			WHERE name LIKE "%ar%";

5. Sort the "albums" in "Z-A" order. (Show only the first 4 records.)
	Terminal

		SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

6. Join the "albums" and "songs" tables. (Sort "albums" from "Z-A" and sort "songs" from "A-Z".)
	Terminal

		SELECT * FROM albums 
			JOIN songs ON albums.id = songs.album_id 
			ORDER BY album_title DESC, song_name ASC;