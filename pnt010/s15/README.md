# MySQL Classic Models

## References

- [Slide Presentation](https://docs.google.com/presentation/d/1jWVpR6v7o8A8h4Yd1sR4kMSM2L1SXM_Hyd5RKIKoxFM/edit#slide=id.g53aad6d9a4_0_728)

## Notes

Before giving the activity to the students, import the <code>classic_models</code> database using the included <code>classic-models-export.sql</code> file in this repository.

Before executing the SQL code from <code>classic-models-export.sql</code>, uncheck the **Enable foreign key checks** option located below the SQL editor pane.

![](images/disable-foreign-key-checks.png)