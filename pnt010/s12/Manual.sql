===========================
Translating Database Design
===========================

References:
    Introduction to SQL
        https://sqlbolt.com/
    What is a Query?
        https://www.hostinger.ph/tutorials/what-is-a-query
    MySQL Data Types
        https://www.mysqltutorial.org/mysql-data-types.aspx

Definition of terms:
    Terminal - Terminal where MySQL is accessed

==========
Discussion
==========

1. Run the MySQL/Maria DB.
    Terminal

        Connecting to MySQL:
        Syntax
            - mysql -u username -p password

        mysql -u root -p

        IMPORTANT NOTE:
            - "-u" stands for username.
            - "root" is the default username for sql.
            - "-p" stands for password.
            - An empty value is the default password for sql.

1. Lists down the databases inside the DBMS.
    Terminal

        Showing/retrieving all databases:

        SHOW DATABASES;

        IMPORTANT NOTE:
            - Commands in SQL will still work with all lowercase letters.
            - Using all caps allows for code readbility to distinguish commands with table names, column names and value inputs.
            - Make sure semi-colons are added at the end of the syntax. 

2. Create a database.
    Terminal

        Creating/adding a database:
        Syntax
            CREATE DATABASE database_name;

        CREATE DATABASE music_store;

        IMPORTANT NOTE:
            - Naming convention in SQL uses the snake_case
            - When creating SQL syntax, typing in all uppercase letters is not required.
            - Capitalizing all letters of an SQL statement makes the code more readable when writing queries.
            - Refer to "references" section of this file to find the documentations for Introduction to SQL and What is a Query?.

3. Drop database.
    Terminal

        Dropping/deleting a database
        DROP DATABASE database_name;

        DROP DATABASE music_store;

        IMPORTANT NOTE:
            - Check if the music_store was created
                SHOW DATABASES;

4. Recreate the database.
    Terminal

        CREATE DATABASE music_db;

        IMPORTANT NOTE:
            - Check if the music_store was deleted.
                SHOW DATABASES;

5. Select a database.
    Terminal

        Using a database:
        Syntax
            USE database_name;

        USE music_db;

        IMPORTANT NOTE:
            - Check if the music_db was created.
                SHOW DATABASES;

6. Create "singers" table.
    Terminal

        Creating/adding tables:
        Syntax
            CREATE TABLE table_name(
                column1, 
                column2, 
                PRIMARY KEY (id) 
            );
        Column Syntax
            column_name data_type other_options
        
        CREATE TABLE singers (
            id INT NOT NULL AUTO_INCREMENT,
            name VARCHAR(50) NOT NULL,
            PRIMARY KEY (id)
        );

        IMPORTANT NOTE:
            - Refer to "references" section of this file to find the documentation for MySQL Data Types.

7. Show tables.
    Terminal

        Showing/retrieving tables:

        SHOW TABLES;

8. Drop "singers" table.
    Terminal

        Dropping/deleting tables:
        Syntax
            DROP TABLE table_name;

        DROP TABLE singers;

9. Create "artists" table.
    Terminal

        CREATE TABLE artists (
            id INT NOT NULL AUTO_INCREMENT,
            name VARCHAR(50) NOT NULL,
            PRIMARY KEY (id)
        );

        IMPORTANT NOTE:
            - Check if singers table was deleted.
                SHOW TABLES;

10. Describe tables.
    Terminal

        Describing tables:

        DESCRIBE artists;

        IMPORTANT NOTE:
            - Describing tables allows to see the tables columns, data_types and extra_options set.
                DESCRIBE table_name



11. Create a "records" table.
    Terminal

        Tables with foreign keys:
        Syntax
            CONSTRAINT foreign_key_name
                FOREIGN KEY (column_name)
                REFERENCES table_name(id)
                ON UPDATE ACTION
                ON DELETE ACTION

        CREATE TABLE records (
            id INT NOT NULL AUTO_INCREMENT,
            album_title VARCHAR(25) NULL,
            artist_id INT NOT NULL,
            PRIMARY KEY (id),
            CONSTRAINT fk_albums_artist_id
                FOREIGN KEY (artist_id) REFERENCES artists(id)
                ON UPDATE CASCADE
                ON DELETE RESTRICT
        );

        IMPORTANT NOTE:
            - Creating tables with foreign keys to tables that don't exist will return an error.
            - Dropping tables with foreign keys will also result to an error if the "ON DELETE" action is set to restrict
            - ON UPDATE actions - NO ACTION, CASCADE, SET NULL, SET DEFAULT
            - ON DELETE actions - RESTRICT, SET NULL

12. Rename "records" table to "albums" table.
    Terminal

        Renaming tables:
        Syntax:
            ALTER TABLE table_name
                RENAME TO new_table_name;

        ALTER TABLE records
            RENAME TO albums;

        IMPORTANT NOTE:
            - Check the names of the current tables.
                SHOW TABLES;

13. Add a column to a table.
    Terminal
    
        Creating/adding columns to a table:
        Syntax
            ALTER TABLE table_name
                ADD column_name data_type extra_options;
        
        ALTER TABLE albums
            ADD date_released DATE NOT NULL;

        Check if the date_released column was added to the table.
        DESCRIBE albums;

        IMPORTANT NOTE:
            - Check how the albums table is currently set up.
                DESCRIBE albums;
            - Columns are added at the end of tables unless specified.

14. Drop a column from a table.
    Terminal

        Dropping/deleting columns:
        Syntax
            ALTER TABLE table_name
                DROP COLUMN column_name;
        
        ALTER TABLE albums
            DROP COLUMN date_released;

        IMPORTANT NOTE:
            - Check if date_released column was deleted.
                DESCRIBE albums;

15. Adding a column to a specific position.
    Terminal

        Creating/adding columns to specific positions:
        Syntax
            ALTER TABLE table_name
                ADD column_name;
                    AFTER column_name;
        
        ALTER TABLE albums
            ADD year DATE NOT NULL
                AFTER album_title;

        IMPORTANT NOTE:
            - Check if date_released column was created in the right position.
                DESCRIBE albums;
            - We can specify the position using the "AFTER" keyword or the "FIRST" keyword if we want the new column to be added as the first column

16. Modifying a column.
    Terminal

        Modifying a column:
        Syntax
            ALTER TABLE table_name
                MODIFY column_name data_type extra_option;
        

        ALTER TABLE albums
            MODIFY album_title VARCHAR(50) NOT NULL;

        IMPORTANT NOTE:
            - Check if album_title column was modified.
                DESCRIBE albums;
            - The "FIRST" and "AFTER" keywords may also be added to change the position of a column

17. Renaming a column.
    Terminal

        Renaming a column:
        Syntax
            ALTER TABLE table_name
                CHANGE COLUMN old_name new_name data_type extra_options;

        ALTER TABLE albums
            CHANGE COLUMN year date_released DATE NOT NULL;

        IMPORTANT NOTE:
            - Check if year column was changed to date_released.
                DESCRIBE albums;
            - Specifying the data_type and the extra_options is required even if the column definitions do not change.

18. Create "songs" table.
    Terminal

        CREATE TABLE songs (
            id INT NOT NULL AUTO_INCREMENT,
            song_name VARCHAR(50) NOT NULL,
            length TIME NOT NULL,
            genre VARCHAR(50) NOT NULL,
            album_id INT NOT NULL,
            PRIMARY KEY (id),
            CONSTRAINT fk_songs_album_id
                FOREIGN KEY(album_id) REFERENCES albums(id)
                ON UPDATE CASCADE
                ON DELETE RESTRICT
        );

        IMPORTANT NOTE:
            - Check if songs table was created
                SHOW TABLES;

19. Create users table.
    Terminal

        CREATE TABLE users (
            id INT NOT NULL AUTO_INCREMENT,
            username VARCHAR(50) NOT NULL,
            password VARCHAR(50) NOT NULL,
            full_name VARCHAR(50) NOT NULL,
            contact_number INT NOT NULL,
            email VARCHAR(50),
            address VARCHAR(50),
            PRIMARY KEY (id)
        );

        IMPORTANT NOTE:
            - Check if users table was created
                SHOW TABLES;

20. Create playlists table.
    Terminal

        CREATE TABLE playlists (
            id INT NOT NULL AUTO_INCREMENT,
            user_id INT NOT NULL,
            datetime_created DATETIME NOT NULL,
            PRIMARY KEY (id),
            CONSTRAINT fk_playlists_user_id
                FOREIGN KEY (user_id) REFERENCES users(id)
                ON UPDATE CASCADE
                ON DELETE RESTRICT
        );

        IMPORTANT NOTE:
            - Check if playlists table was created:
                SHOW TABLES;

21. Create a joining playlists songs table.
    Terminal

        CREATE TABLE playlists_songs (
            id INT NOT NULL AUTO_INCREMENT,
            playlist_id INT NOT NULL,
            song_id INT NOT NULL,
            PRIMARY KEY (id),
            CONSTRAINT fk_playlists_songs_playlist_id
                FOREIGN KEY (playlist_id) REFERENCES playlists(id)
                ON UPDATE CASCADE
                ON DELETE RESTRICT,
            CONSTRAINT fk_playlists_songs_song_id
                FOREIGN KEY (song_id) REFERENCES songs(id)
                ON UPDATE CASCADE
                ON DELETE RESTRICT
        );

        IMPORTANT NOTE:
            Check if playlists_songs table was created
                SHOW TABLES;

========
Activity
========

1. Create a database "blog_db".
    Terminal

        CREATE DATABASE blog_db;

2. Used the "blog_db" database.
    Terminal
    
        USE blog_db;

3. Create a "users" table.
    Terminal

        CREATE TABLE users (
            id INT NOT NULL AUTO_INCREMENT,
            email VARCHAR(100) NOT NULL,
            password VARCHAR(50) NOT NULL,
            datetime_created DATETIME NOT NULL,
            PRIMARY KEY (id)
        );

4. Create a "posts" table.
    Terminal

        CREATE TABLE posts (
            id INT NOT NULL AUTO_INCREMENT,
            user_id INT NOT NULL,
            title VARCHAR(200) NOT NULL,
            content VARCHAR(5000) NOT NULL,
            datetime_posted DATETIME NOT NULL,
            PRIMARY KEY (id),
            CONSTRAINT fk_posts_user_id
                FOREIGN KEY (user_id) 
                REFERENCES users(id)
                ON UPDATE CASCADE
                ON DELETE RESTRICT
        );

5. Create a "posts_likes" table.
    Terminal

        CREATE TABLE post_likes (
            id INT NOT NULL AUTO_INCREMENT,
            user_id INT NOT NULL,
            post_id INT NOT NULL,
            datetime_liked DATETIME NOT NULL,
            PRIMARY KEY (id),
            CONSTRAINT fk_post_likes_user_id
                FOREIGN KEY (user_id) 
                REFERENCES users(id)
                ON UPDATE CASCADE
                ON DELETE RESTRICT,
            CONSTRAINT fk_post_likes_post_id
                FOREIGN KEY (post_id) 
                REFERENCES posts(id)
                ON UPDATE CASCADE
                ON DELETE RESTRICT
        );

6. Create a "posts_comments" table.
    Terminal

        CREATE TABLE post_comments (
            id INT NOT NULL AUTO_INCREMENT,
            user_id INT NOT NULL,
            post_id INT NOT NULL,
            content VARCHAR(5000) NOT NULL,
            datetime_commented DATETIME NOT NULL,
            PRIMARY KEY (id),
            CONSTRAINT fk_post_comments_user_id
                FOREIGN KEY (user_id) 
                REFERENCES users(id)
                ON UPDATE CASCADE
                ON DELETE RESTRICT,
            CONSTRAINT fk_post_comments_post_id
                FOREIGN KEY (post_id) 
                REFERENCES posts(id)
                ON UPDATE CASCADE
                ON DELETE RESTRICT
        );