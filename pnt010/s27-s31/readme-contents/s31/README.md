# Session Objectives

At the end of the session, the students are expected to:

- use the concepts learned in the previous sessions and containerize a ASP.NET API and MySQL database.

# Resources

## Instructional Materials

- [GitLab Repository (App)](https://gitlab.com/zuitt-coding-bootcamp-curricula/corporate-courses/pointwest/pnt010/-/tree/master/s27-s31/CourseBooking-App)
- [GitLab Repository (Dockerized App)](https://gitlab.com/zuitt-coding-bootcamp-curricula/corporate-courses/pointwest/pnt010/-/tree/master/s27-s31/Dockerized-Apps/CourseBooking-App)
- [GitLab Repository (Guide)](https://gitlab.com/zuitt-coding-bootcamp-curricula/corporate-courses/pointwest/pnt010/-/tree/master/s27-s31)
- [Google Slide Presentation](https://docs.google.com/presentation/d/1qH7NppL5PD2zNfKLVJzbrQrHpT47Gfclgqwj2lT0_Dw/edit)

## Supplemental Materials

- [Docker Documentation Guide for C#(.NET)](https://docs.docker.com/language/dotnet/)
- [Tutorial: Containerize a .NET app](https://learn.microsoft.com/en-us/dotnet/core/docker/build-container?tabs=linux)
- [How To Containerize An Asp.NetCore Api and MySQL DataBase with Docker Compose by Elegberun Olugbenga](https://dev.to/gbengelebs/how-to-containerize-an-asp-netcore-api-and-mysql-with-docker-compose-1m5c)

# Lesson Proper

## The Docker extension for Visual Studio Code

Now that we have learned the fundamentals of Docker, it's time we explore a tool that will make our app Dockerization easier. For this session, we will be using the **Docker extension** in  **Visual Studio Code**. In your VS Code extensions panel, search and install **Docker**.

![Untitled](readme-images/Untitled.png)

The Docker icon should appear in your activity bar shortly after the installation.

![Untitled](readme-images/Untitled%200.png)

Later on in this session, we will use some features of this extension.

## Getting the API

Before starting, we first need to get a copy of the ASP.NET API source code.

Share the source code found in this GitLab [repository](https://gitlab.com/zuitt-coding-bootcamp-curricula/corporate-courses/pointwest/pnt010/-/tree/master/s27-s31/CourseBooking-App) to the trainees.

Inside the directory, you will find the source codes for the API.

The application uses Swagger middleware for easier exploration and testing of the API.
More info regarding Swagger middleware can be found in this [link](https://learn.microsoft.com/en-us/aspnet/core/tutorials/getting-started-with-swashbuckle?view=aspnetcore-7.0&tabs=visual-studio).

When we try to run the app using the `dotnet run` command, we can launch the app and navigate to [localhost:5157/swager](http://localhost:5157/swagger/index.html) to explore the API via the Swagger UI. Note that the port number varies.

![Untitled](readme-images/Untitled%201.png)

## Creating the Environment Variable File

Inside the **course-booking-be** folder, create an **.env** file and add the following code.

```bash
MYSQLDB_USER=root
MYSQLDB_ROOT_PASSWORD=123456

MYSQLDB_DATABASE=CourseBookingDb

MYSQLDB_LOCAL_PORT=3306
MYSQLDB_DOCKER_PORT=3306

ASPNET_LOCAL_PORT=5157
ASPNET_DOCKER_PORT=5157

DBHOST=database
ASPNETCORE_ENVIRONMENT=Development
```

The values for this environment variable file will be used in the Docker Compose file we will be creating in the next section.

## Compose Code for MySQL Container

Inside the **course-booking-app** folder, create a file named **docker-compose.yml** and add the following code.

```yaml
version: '3.8'

#create a volume for the my sql container.To ensure Data persists after my sql container is stopped
volumes:
  datafiles:

services:
  #DataBase Service
  database:
    #Pull the latest mysql image
    image: mysql:8.0.33
    #Map port 3306 on the mysql container to port 3306 in the host
    ports:
      - $MYSQLDB_LOCAL_PORT:$MYSQLDB_DOCKER_PORT
    #Specify where the persisted Data should be stored
    volumes:
      - datafiles:/var/lib/mysql
    restart: always
    #Specify Environment Variables for mysql
    env_file:
      - .env
    environment: 
      - MYSQL_ROOT_PASSWORD=${MYSQLDB_ROOT_PASSWORD}
      - MYSQL_DATABASE=${MYSQLDB_DATABASE}
```

Let's discuss the major parts first:
- `version`: Refers to the version of the docker-compose.
- `volumes`: Because containers are designed to be stateless, once a container stops running all data stored in that container is not saved and we definitely do not want that for our database container. In order for your data to be persisted after the container has stopped or been destroyed it is essential to persist your data somewhere outside the container. So what we did is provide a reference to that path to docker. We specify our volume name called **datafile**.
- `services`: This is where we define the application dependencies. In this application, we have the database service and later on, we will incorporate the API service.

Now let's go over the Database service:
- `image`: We define the database image version we want so docker fetches that from docker hub. In this case MySQL 8.0.33.
- `ports`: Here we identify to which the container will be exposed in Docker and in the host machine.
- `volumes`: Here we define our volumes. **datafiles** should be stored at the indicated path. In this case inside the volume's **/var/lib/mysql** directory.
- `restart`: This identifies the approach of restarting the app, in this case, the container will **always** restart.
- `env_file`: This specifies which environment variable file will be used during the container creation.
- `environment`: This specifies the environment variables that will be used by the container; the values come from the **.env** file and is denoted by using the `$` symbol along with the variable name from the environment variable file.

## Testing the Compose Code for MySQL Container

Execute the `docker-compose up -d` then test the database container by using the CLI interface from the Docker Desktop.

![Untitled](readme-images/Untitled%202.png)

Login as root and list the database inside the database server. Enter the password defined in the environment variable file.

```bash
mysql -u root -p
```

Then, execute the following commands:

```sql
SHOW DATABASES;
USE CourseBookingDb;
SHOW TABLES;
```

The codes above should present the following output in the terminal.

![Untitled](readme-images/Untitled%203.png)


## Adding Database Tables Automatically

To create the tables once the database container is built, create a folder named **db** inside **course-booking-be**, create a file named **init.sql** inside the **db** folder and add the following SQL script.

Use your preferred code-sharing tool to share the code below to the trainees.

```sql
-- MySQL dump 10.13  Distrib 8.0.33, for macos11.7 (x86_64)
--
-- Host: localhost    Database: CourseBookingDb
-- ------------------------------------------------------
-- Server version 8.0.33


-- ==================================================
-- Table structure for table `Courses`
-- ==================================================

DROP TABLE IF EXISTS `Courses`;

CREATE TABLE `Courses` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `Name` varchar(100) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Price` double DEFAULT NULL,
  `IsActive` tinyint(1) DEFAULT NULL,
  `DatetimeCreated` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------
-- Dumping data for table `Courses`
-- --------------------------------------------------

LOCK TABLES `Courses` WRITE;

INSERT INTO `Courses` VALUES 
  (1,'Full-Stack Development Course','Learn all about full-stack development.',19000,1,'2023-05-22 18:16:44'),
  (2,'Introduction to Web','Learn all about basics of the web.',123,1,'2023-05-22 18:16:44');

UNLOCK TABLES;


-- ==================================================
-- Table structure for table `Users`
-- ==================================================

DROP TABLE IF EXISTS `Users`;

CREATE TABLE `Users` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `FirstName` varchar(50) DEFAULT NULL,
  `LastName` varchar(50) DEFAULT NULL,
  `Email` varchar(100) DEFAULT NULL,
  `Password` varchar(100) DEFAULT NULL,
  `IsAdmin` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------
-- Dumping data for table `Users`
-- --------------------------------------------------

LOCK TABLES `Users` WRITE;

INSERT INTO `Users` VALUES 
  (1,'Jane','Smith','admin@gmail.com','password123',1),
  (2,'John','Doe','johndoe@gmail.com','password456',0);

UNLOCK TABLES;


-- ==================================================
-- Table structure for table `CourseEnrollments`
-- ==================================================

DROP TABLE IF EXISTS `CourseEnrollments`;

CREATE TABLE `CourseEnrollments` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `CourseId` int DEFAULT NULL,
  `UserId` int DEFAULT NULL,
  `DatetimeEnrolled` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `CourseId` (`CourseId`),
  KEY `UserId` (`UserId`),
  CONSTRAINT `courseenrollments_ibfk_1` FOREIGN KEY (`CourseId`) REFERENCES `Courses` (`Id`),
  CONSTRAINT `courseenrollments_ibfk_2` FOREIGN KEY (`UserId`) REFERENCES `Users` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


-- --------------------------------------------------
-- Dumping data for table `CourseEnrollments`
-- --------------------------------------------------

LOCK TABLES `CourseEnrollments` WRITE;

UNLOCK TABLES;
```

In the **docker-compose.yml** file, add the following code.

```yaml
version: '3.8'

#create a volume for the my sql container.To ensure Data persists after my sql container is stopped
volumes:
  datafiles:

services:
  #DataBase Service
  database:
    ...
    volumes:
      - datafiles:/var/lib/mysql
      - ./db/init.sql:/docker-entrypoint-initdb.d/init.sql
```

The added code transfers the **init.sql** file inside the **docker-entrypoint-initdb.d** folder, which is used by Docker to execute all valid executable files inside it (such as the `.sql` file). By default, all scripts located in this folder will be automatically run during container startup.

Then, delete the container group and execute `docker-compose up -d` again.

## Confirming Automatic SQL Script Execution

You should now see the following from Docker Desktop.

The copied init.sql file in the container:

![Untitled](readme-images/Untitled%204.png)

If you access again the database server with your root credentials, you should now see the created tables in the list:

![Untitled](readme-images/Untitled%205.png)

This means that the SQL script was successfuly executed while building the database container.

## Dockerfile for API

Our next step is to create the **Dockerfile** in the root folder of our source code. For this step, we will be using the Docker extension in VS Code.

- Step 1: Click the Docker icon in the activity bar.
- Step 2: Select **Open Docker Extension Walkthrough**. The basic steps on how to Dockerize an app should appear in the editor groups area of the window.
- Step 3: Select **Add Docker Files to Workspace** and click the button **Add Docker Files**.

![Untitled](readme-images/Untitled%206.png)

- Step 4: Select the appropriate choices in the appearing selections.
    - Application Platform: .NET: ASP.NET Core
    - Operating System: Linux or Windows
    - What port/s does you app listen on?: I used 5157 in my case.
    - Include Optional Docker Compose files?: Yes (Since we will be using the docker compose file again later on.)

The **Dockerfile** and **.dockerignore** files should automatically appear in your source code directory.

The **.dockerignore** file is used to specify files and directories that should be excluded from the context when building a Docker image. The purpose of the .dockerignore file is to optimize the build process and reduce the size of the Docker context, which is the set of files and directories that are sent to the Docker daemon to build the image.

On the other hand, the **Dockerfile** would have the following contents:

```yaml
FROM mcr.microsoft.com/dotnet/aspnet:7.0 AS base
WORKDIR /app
EXPOSE 5157

ENV ASPNETCORE_URLS=http://+:5157

# Creates a non-root user with an explicit UID and adds permission to access the /app folder
# For more info, please refer to https://aka.ms/vscode-docker-dotnet-configure-containers
RUN adduser -u 5678 --disabled-password --gecos "" appuser && chown -R appuser /app
USER appuser

FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build
WORKDIR /src
COPY ["course-booking-be/course-booking-be/course-booking-be.csproj", "course-booking-be/course-booking-be/"]
RUN dotnet restore "course-booking-be/course-booking-be/course-booking-be.csproj"
COPY . .
WORKDIR "/src/course-booking-be/course-booking-be"
RUN dotnet build "course-booking-be.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "course-booking-be.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "course-booking-be.dll"]
```

Modify the generated Dockerfile so that the links to the files from the host machine will be relative to the location of the Dockerfile and also to remove redundant directories in the container. It should now be like this:

```yaml
FROM mcr.microsoft.com/dotnet/aspnet:7.0 AS base
WORKDIR /app
EXPOSE 5157
...

FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build
WORKDIR /src
COPY ["course-booking-be.csproj", "./"]
RUN dotnet restore "course-booking-be.csproj"
COPY . .
WORKDIR "/src/."
RUN dotnet build "course-booking-be.csproj" -c Release -o /app/build
...
```

In summary, this Dockerfile sets up a multi-stage build process for an ASP.NET Core application. It first sets up the base image, creates a non-root user, and exposes a port for the application. Then, it proceeds with building the application by copying the necessary files, restoring the dependencies, and building the project. In the subsequent stage, it publishes the application for deployment. Finally, it creates the final container image by copying the published output and sets the entry point to run the application when the container starts. Below is the detailed line by line explanation of the file.

`FROM mcr.microsoft.com/dotnet/aspnet:7.0 AS base`

This line specifies the base image for the Docker container. It uses the ASP.NET runtime image from Microsoft's container registry as the base image for the subsequent stages in the Dockerfile.

`WORKDIR /app`

This line sets the working directory inside the container to /app. It means that any commands or file operations executed afterwards will be relative to this directory.

`EXPOSE 5157`

This line exposes port 5157 on the container. It informs Docker that the container will listen on this port and allows other containers or the host machine to connect to it.

`ENV ASPNETCORE_URLS=http://+:5157`

This line sets an environment variable named ASPNETCORE_URLS with the value http://+:5157. It configures the ASP.NET Core application inside the container to listen on all network interfaces (+) on port 5157.

`RUN adduser -u 5678 --disabled-password --gecos "" appuser && chown -R appuser /app`

This line creates a non-root user named appuser with a specific UID (5678). It also assigns ownership of the /app directory to this user. This step is done for security reasons, as running the application with a non-root user helps mitigate potential security vulnerabilities.

`USER appuser`

This line sets the user context inside the container to appuser. From this point onward, any commands or processes will be executed under the appuser instead of the root user.

`FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build`

This line starts a new build stage in the Dockerfile, using the .NET SDK image as the base image. This stage is responsible for building the application code.

`WORKDIR /src`

This line sets the working directory for this build stage to /src. It means that subsequent commands will be executed relative to this directory.

`COPY ["./course-booking-be.csproj", "./"]`

This line copies the course-booking-be.csproj file from the host machine to the current working directory (/src) inside the container. This file is the project file for the ASP.NET Core application.

`RUN dotnet restore "course-booking-be.csproj"`

This line runs the dotnet restore command inside the container, which restores the NuGet packages required by the application based on the project file.

`COPY . .`

This line copies all the files and directories from the current directory on the host machine to the current working directory (/src) inside the container. It includes the source code of the ASP.NET Core application.

`WORKDIR "/src/."`

This line changes the working directory to /src/. inside the container. All subsequent commands will be executed relative to this directory.

`RUN dotnet build "course-booking-be.csproj" -c Release -o /app/build`

This line runs the dotnet build command inside the container to build the ASP.NET Core application in release configuration (-c Release). The output is placed in the /app/build directory.

`FROM build AS publish`

This line creates a new build stage based on the previous build stage. It serves as an intermediate stage for publishing the application.

`RUN dotnet publish "course-booking-be.csproj" -c Release -o /app/publish /p:UseAppHost=false`

This line runs the `dotnet publish` command inside the container, which generates a deployment-ready version of the application. It publishes the ASP.NET Core application specified by the course-booking-be.csproj project file in release configuration (-c Release). The published output is placed in the /app/publish directory. The /p:UseAppHost=false flag is used to disable the creation of an application host during the publish process.

Running `dotnet publish` during the Docker build process allows you to leverage the build environment inside the container, ensuring that the application is correctly built and configured in an environment similar to the production environment where the container will be deployed. This is a best practice to ensure that the application is properly prepared and optimized for deployment within the Docker container.

`FROM base AS final`

This line creates a new build stage based on the initial base stage. It serves as the final stage for creating the container image.

`WORKDIR /app`

This line sets the working directory to /app inside the container for the final stage.

`COPY --from=publish /app/publish .`

This line copies the published output from the previous publish stage (the /app/publish directory) to the current working directory (/app) in the final stage.

`ENTRYPOINT ["dotnet", "course-booking-be.dll"]`

This line specifies the command that will be executed when the container starts. It sets the entry point to run the ASP.NET Core application using the dotnet command and specifies the main executable as course-booking-be.dll.

## Testing the Dockerfile for API

Try building an image out of the dockerfile we just created by using the command `docker build -t course-booking-img .`. Make sure that you are in the same directory as the **Dockerfile** which is **course-booking-be**. You should be able to build an image named `course-booking-img`.

![Untitled](readme-images/Untitled%207.png)

Now let's run a container based from the generated image using the command `docker run --name course-booking-cont course-booking-img`. Notice the error in the logs:

![Untitled](readme-images/Untitled%208.png)

This is because we are running the application in the container without the database.

## Compose Code for API Container

Go back to the **docker-compose.yml** file and add the following code.

```yaml
version: '3.8'

#create a volume for the my sql container.To ensure Data persists after my sql container is stopped
volumes:
  datafiles:

services:
  #DataBase Service
  database:
  ...

  #Api  Service
  api:
    build:
      context: .
      #Use the DockerFile in that Directory
      dockerfile: Dockerfile
    #This Service Depends on the database service specifed above
    depends_on: 
      - database
    #Map port 5157 in the docker container to port 5157 in the Api
    ports:
      - $ASPNET_LOCAL_PORT:$ASPNET_DOCKER_PORT
    restart: always
    #Specify Environment Variables for the Api Service
    env_file:
      - .env
    environment: 
      - DBHOST=${DBHOST}
      - ASPNETCORE_ENVIRONMENT=${ASPNETCORE_ENVIRONMENT}
```

Now let's go over the contents of the API service:
- `build`: Docker should build the docker file specified in this context.
- `depends_on`: This indictaes that this service depends on the database service defined earlier above hereby linking the two containers.
- `ports`: This maps the port of the docker container to the port of the host machine.
- `retart`: This specifies how often the container should restart.
- `env_file`: This specifies which environment variable file will be used during the container creation.
- `environment`: This specifies that the DBHOST should be the database service.

By default Compose sets up a single network for your app. Each container for a service joins the default network and is both reachable by other containers on that network, and discoverable by them at a hostname identical to the container name. Further details about Docker networking may be found in this [link](https://docs.docker.com/compose/networking/).

## Testing the Accessibility of Containerized API

Now that we have completely set up our docker files, let's try building and running again a container using the compose file. Run the command `docker-compose up -d` and go to [localhost:5157/swager](http://localhost:5157/swagger/index.html) 

The Swagger UI with the API endpoints should be up and runnning. You can now try accessing the MySQL database by adding, deleting users, courses, etc.

![Untitled](readme-images/Untitled%209.png)

![Untitled](readme-images/Untitled%2010.png)


# Activity

## Quiz

### Instructions

Answer the quiz form provided by your instructor.

For instructors, create a copy of this [quiz form](https://docs.google.com/forms/d/14qSfaXaYpj3y2CXeHpeHX_tadQXaWUE3Mmur01SLjdM/edit) to your own batch kit folder.

### Essay Questions

Why do we need to put values of environment variables to an .env file?

- This is so that the values are not directly exposed inside the `docker-compose.yml` file.

If we want to create a database structure upon container creation, where do we put it and why should it be placed there?

- We should put it inside `docker-entrypoint-initdb.d` since it is where SQL scripts are automatically executed.

What is the purpose of the .dockerignore file?

- The .dockerignore file is used to specify files and directories that should be excluded from the context when building a Docker image. This reduces image size and improves build performance.

Why do we need to publish (RUN dotnet publish) the application in the container?

- Running `dotnet publish` ensures that the application is correctly built and configured in an environment similar to the production environment where the container will be deployed. This is a best practice to ensure that the application is properly prepared and optimized for deployment within the Docker container.

Do we need to create the directories specified in the 'WORKDIR' directive in the Dockerfile? Why?

- When you specify a directory path in the WORKDIR directive, such as WORKDIR /app or WORKDIR /src, it doesn't necessarily mean that those directories have to exist in your actual source code files. Instead, these directories are created inside the container at build time, and any subsequent commands or file operations are executed relative to that directory.