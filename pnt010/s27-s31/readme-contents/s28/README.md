# Session Objectives

At the end of the session, the students are expected to:

- containerize a basic application and make it available to Docker Hub.

# Resources

## Instructional Materials

- [GitLab Repository (App)](https://gitlab.com/zuitt-coding-bootcamp-curricula/corporate-courses/pointwest/pnt010/-/tree/master/s27-s31/NodeJs-App)
- [GitLab Repository (Dockerized App)](https://gitlab.com/zuitt-coding-bootcamp-curricula/corporate-courses/pointwest/pnt010/-/tree/master/s27-s31/Dockerized-Apps/NodeJs-App)
- [GitLab Repository (Guide)](https://gitlab.com/zuitt-coding-bootcamp-curricula/corporate-courses/pointwest/pnt010/-/tree/master/s27-s31)
- [Google Slide Presentation](https://docs.google.com/presentation/d/11HJaCRu104uzk9cU_nGemvvHY8V8AC4xIsxCRxYpDmw/edit)

## Supplemental Materials

- [Part 2: Sample Application (Docker Docs)](https://docs.docker.com/get-started/02_our_app/)
- [Part 3: Update the Application (Docker Docs)](https://docs.docker.com/get-started/03_updating_app/)
- [Part 4: Share the Application (Docker Docs)](https://docs.docker.com/get-started/04_sharing_app/)

# Lesson Proper

The contents of this guide is based from the Getting Started contents of Docker Docs and is built upon it.

## Introduction

For this session, we will be working with a simple todo list manager that is running in Node.js. 

At this point, your development team is quite small and you're simply building an app to prove out your MVP (minimum viable product). You want to show how it works and what it's capable of doing without needing to think about how it will work for a large team, multiple developers, etc.

## Getting the App

Before we can run the application, we need to get the application source code onto our machine. Share a copy of the [GitLab repository](https://gitlab.com/zuitt-coding-bootcamp-curricula/corporate-courses/pointwest/pnt010/-/tree/master/s27-s31/NodeJs-App) to the students.

Once shared, open the project using Visual Studio Code. We should see the following contents of the source code.

![Untitled](readme-images/Untitled.png)

## Build the App Container Image

In order to build the application, we need to use a `Dockerfile`. A Dockerfile is simply a text-based script of instructions that is used to create a container image.

Create a file named `Dockerfile` in the same folder as the file `package.json` with the following contents.

```docker
# syntax=docker/dockerfile:1
FROM node:12-alpine
RUN apk add --no-cache python3 g++ make
WORKDIR /app
COPY . .
RUN yarn install --production
CMD ["node", "src/index.js"]
```

Please check that the file `Dockerfile` has no file extension like `.txt`. Some editors may append this file extension automatically and this would result in an error in the next step.

Open a terminal and go to the `app` directory with the `Dockerfile`. Now build the container image using the `docker build` command.

```bash
docker build -t lastname-nodejs-todo .
```

Change the `lastname` into the actual last name of the user.

The result of the command above is shown below.

![Untitled](readme-images/Untitled%201.png)

![Untitled](readme-images/Untitled%202.png)

This command used the Dockerfile to build a new container image. You might have noticed that a lot of "layers" were downloaded. This is because we instructed the builder that we wanted to start from the `node:12-alpine` image. But, since we didn’t have that on our machine, that image needed to be downloaded.

After the image was downloaded, we copied in our application and used `yarn` to install our application’s dependencies. The `CMD` directive specifies the default command to run when starting a container from this image.

Finally, the `-t` flag tags our image. Think of this simply as a human-readable name for the final image. Since we named the image `lastname-nodejs-todo`, we can refer to that image when we run a container.

The `.` at the end of the `docker build` command tells that Docker should look for the `Dockerfile` in the current directory.

## Start an App Container

Now that we have an image, let’s run the application.

Start your container using the `docker run` command and specify the name of the image we just created:

```bash
docker run -d -p 3000:3000 lastname-nodejs-todo
```

Remember the `d` and `p` flags? We’re running the new container in "detached" mode (in the background) and creating a mapping between the host’s port 3000 to the container's port 3000. Without the port mapping, we would not be able to access the application.

After a few seconds, open your web browser to [http://localhost:3000](http://localhost:3000/). You should see our app.

![Untitled](readme-images/Untitled%203.png)

Go ahead and add an item or two and see that it works as you expect. You can mark items as complete and remove items. Your frontend is successfully storing items in the backend. Pretty quick and easy, huh?

At this point, you should have a running todo list manager with a few items, all built by you. Now, let’s make a few changes and learn about managing our containers.

If you take a quick look at the Docker Dashboard, you should see your two containers running now (this tutorial and your freshly launched app container).

![Untitled](readme-images/Untitled%204.png)

## Update the Source Code

As a small feature request, we have been asked by the product team to change the "empty text" when we don't have any todo list items. They would like to transition it to the following: "You have no todo items yet! Add one above!"

Pretty simple, right? Let's make the change.

In the `src/static/js/app.js` file, update line 56 to use the new empty text.

```html
- <p className="text-center">No items yet! Add one above!</p>
+ <p className="text-center">You have no todo items yet! Add one above!</p>
```

Let's build our updated version of the image, using the same command we used before.

```bash
docker build -t lastname-nodejs-todo .
```

Wait for the command to finish.

![Untitled](readme-images/Untitled%205.png)

Next, let's start a new container using the updated code.

```bash
docker run -d -p 3000:3000 lastname-nodejs-todo
```

However, there will be an error when we try the run command:

![Untitled](readme-images/Untitled%206.png)

We aren't able to start the new container because our old container is still running. The reason this is a problem is because that container is using the host’s port 3000 and only one process on the machine (containers included) can listen to a specific port. To fix this, we need to remove the old container.

## Replace the Old Container

To remove a container, it first needs to be stopped. Once it has stopped, it can be removed. We have two ways that we can remove the old container. Feel free to choose the path that you’re most comfortable with.

### Remove a Container Using CLI

Here are the following steps:

1. Get the ID of the container by using the `docker ps` command.
2. Use the `docker stop <container-id>` command to stop the container.
3. Once the container has stopped, you can remove it by using the `docker rm <container-id>` command.

The screenshot below shows the steps being done in the terminal.

![Untitled](readme-images/Untitled%207.png)

Alternatively, you can stop and remove a container in a single command by adding the "force" flag to the `docker rm` command. For example: `docker rm -f <the-container-id>`.

### Remove a Container Using Docker Desktop

If you open the Docker Desktop, you can remove a container with two clicks! It’s certainly much easier than having to look up the container ID and remove it.

1. With the dashboard opened, hover over the app container and you'll see a collection of action buttons appear on the right.
2. Click on the trash can icon to delete the container.
3. Confirm the removal and you're done!

![Untitled](readme-images/Untitled%208.png)

### Start the Updated App Container

Now, start your updated app.

```bash
docker run -d -p 3000:3000 lastname-nodejs-todo
```

Refresh your browser on [http://localhost:3000](http://localhost:3000/) and you should see your updated help text!

![Untitled](readme-images/Untitled%209.png)

## Create a Repository

Now that we have built an image, let's share it! To share Docker images, you have to use a Docker registry. The default registry is Docker Hub and is where all of the images we’ve used have come from.

A Docker ID allows you to access Docker Hub which is the world's largest library and community for container images. Create a [Docker ID](https://hub.docker.com/signup) for free if you don't have one.

To push an image, we first need to create a repository on Docker Hub. [Sign up](https://www.docker.com/pricing?utm_source=docker&utm_medium=webreferral&utm_campaign=docs_driven_upgrade) or Sign in to [Docker Hub](https://hub.docker.com/).

![Untitled](readme-images/Untitled%2010.png)

Click the **Create Repository** button.

![Untitled](readme-images/Untitled%2011.png)

For the repo name, use `lastname-nodejs-todo`. Make sure the Visibility is `Public`.

Click the **Create** button!

![Untitled](readme-images/Untitled%2012.png)

If you look on the right-side of the page, you'll see a section named **Docker commands**. This gives an example command that you will need to run to push to this repo.

![Untitled](readme-images/Untitled%2013.png)

## Push the Image

In the command line, try running the push command you see on Docker Hub. Note that your command will be using your namespace, not "docker".

```bash
docker push sqcahilogtuitt/lastname-nodejs-todo
```

We will have the following error:

![Untitled](readme-images/Untitled%2014.png)

Why did it fail? The push command was looking for an image named lastname/getting-started, but didn't find one. If you run `docker image ls`, you won't see one either.

![Untitled](readme-images/Untitled%2015.png)

To fix this, we need to "tag" our existing image we’ve built to give it another name.

Login to the Docker Hub using the command `docker login -u YOUR-USER-NAME`.

![Untitled](readme-images/Untitled%2016.png)

Use the `docker tag` command to give the `lastname-nodejs-todo` image a new name. Be sure to swap out `YOUR-USER-NAME` with your Docker ID.

```bash
docker tag lastname-nodejs-todo YOUR-USER-NAME/getting-started
```

Now try your push command again. If you are copying the value from Docker Hub, you can drop the `tagname` portion, as we didn't add a tag to the image name. If you don't specify a tag, Docker will use a tag called `latest`.

```bash
docker push YOUR-USER-NAME/lastname-nodejs-todo
```

The commands above will give the following output:

![Untitled](readme-images/Untitled%2017.png)

Wait for the push command to finish. The command uploads the image (which is around 400MB) so it may take a while.

Once finished, the **Tags and Scans** section of the Docker Hub repository page will be updated upon refresh.

![Untitled](readme-images/Untitled%2018.png)

By clicking the **latest** tag, we can view some more information about the image.

![Untitled](readme-images/Untitled%2019.png)

# Activity

## Quiz

### Instructions

Answer the quiz form provided by your instructor.

For instructors, create a copy of this [quiz form](https://docs.google.com/forms/d/15Q2y3OgkIjQQNPe17AVjhMUsvFPfq6KlJ8tPLiDCi68/edit) to your own batch kit folder.

### Answers

A text-based script of instructions that is used to create a container image.

- Dockerfile

Docker command to build a container image.

- docker build

Docker command to delete a container.

- docker rm

Docker command to delete a container forcibly.

- docker rm -f

Docker command send the image to Docker Hub.

- docker push

Docker command to give a new name to an image.

- docker tag