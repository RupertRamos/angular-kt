#!/bin/bash

# Stop and remove all containers
docker stop $(docker ps -aq)
docker rm $(docker ps -aq)

# Remove all images
docker rmi $(docker images -aq)

# Remove all volumes
docker volume rm $(docker volume ls -q)

# Run Docker Compose command to build and start containers
docker-compose -f docker-compose.yml up -d

