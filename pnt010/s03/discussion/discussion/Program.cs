﻿using System.Xml.Linq;

namespace discussion
{
    class Discussion
    {
        static void Main(string[] args)
        {
            // [Section] Objects, Classes and Instances
            // A class is a blueprint for an object
            // Objects and instances are a block of memory that has been allocated and configured according to the blueprint

            // Instantiating an object without initialization
            Item itemA = new Item();
            itemA.Name = "Pork";
            // Returns an error because the branch field can only be assigned a value during initialization
            // itemA.Branch = "North";
            itemA.BeginningInventory = 100;
            itemA.StockIn = 10;
            itemA.StockOut = 5;
            itemA.TotalBalance = 105;

            Console.WriteLine(itemA.ToString());

            Item itemB = new Item
            {
                Name = "Pork",
                BeginningInventory = 100,
                StockIn = 10,
                StockOut = 5,
                TotalBalance = 105
            };

            Console.WriteLine(itemB.ToString());

            // Instantiating and initializing objects
            Item itemC = new Item("Beef", "South", 0, 0, 0, 0);

            Console.WriteLine(itemC.ToString());

            // Instantiating and Initializing Structures

            /*
            User userA = new User("johndoe", "john@mail.com", "john1234");
            User userB = userA;
            userB.display();
            */

            // [Section] Activity
            Console.WriteLine("Welcome to Zuitt Asset Management App!");
            Console.WriteLine("Choose an option: [1] Create Item, [2] Create User");

            int option = Convert.ToInt32(Console.ReadLine());

            while(option < 1 || option > 2)
            {
                Console.WriteLine("Please choose a valid option");
                option = Convert.ToInt32(Console.ReadLine());
            }

            switch (option)
            {
                case 1:
                    Console.WriteLine("Enter item name:");
                    string itemName = Console.ReadLine();

                    while (string.IsNullOrWhiteSpace(itemName))
                    {
                        Console.WriteLine("Enter a valid product name:");
                        itemName = Console.ReadLine();
                    }

                    Console.WriteLine("Enter item branch:");
                    string itemBranch = Console.ReadLine();

                    while (string.IsNullOrWhiteSpace(itemBranch))
                    {
                        Console.WriteLine("Enter a valid branch:");
                        itemBranch = Console.ReadLine();
                    }

                    Console.WriteLine("Enter item beginning inventory:");
                    int itemBegInv = Convert.ToInt32(Console.ReadLine());

                    while (itemBegInv < 0)
                    {
                        Console.WriteLine("Enter a valid amount:");
                        itemBegInv = Convert.ToInt32(Console.ReadLine());
                    }

                    Console.WriteLine("Enter item stock in:");
                    int itemStockIn = Convert.ToInt32(Console.ReadLine());

                    while (itemStockIn < 0)
                    {
                        Console.WriteLine("Enter a valid amount:");
                        itemStockIn = Convert.ToInt32(Console.ReadLine());
                    }

                    Console.WriteLine("Enter item stock out:");
                    int itemStockOut = Convert.ToInt32(Console.ReadLine());

                    while (itemStockOut < 0)
                    {
                        Console.WriteLine("Enter a valid amount:");
                        itemStockIn = Convert.ToInt32(Console.ReadLine());
                    }

                    int totalBalance = itemBegInv + itemStockIn - itemStockOut;

                    Item newItem = new Item(itemName, itemBranch, itemBegInv, itemStockIn, itemStockOut, totalBalance);

                    Console.WriteLine(newItem.ToString());

                    break;

                case 2:
                    Console.WriteLine("Enter username:");
                    string username = Console.ReadLine();

                    while (string.IsNullOrWhiteSpace(username))
                    {
                        Console.WriteLine("Enter a valid username:");
                        username = Console.ReadLine();
                    }

                    Console.WriteLine("Enter email:");
                    string email = Console.ReadLine();

                    while (string.IsNullOrWhiteSpace(email))
                    {
                        Console.WriteLine("Enter a valid email:");
                        email = Console.ReadLine();
                    }

                    Console.WriteLine("Enter password:");
                    string password = Console.ReadLine();

                    while (string.IsNullOrWhiteSpace(password))
                    {
                        Console.WriteLine("Enter a valid password:");
                        password = Console.ReadLine();
                    }

                    Console.WriteLine("Enter address:");
                    string address = Console.ReadLine();

                    while (string.IsNullOrWhiteSpace(address))
                    {
                        Console.WriteLine("Enter a valid address:");
                        address = Console.ReadLine();
                    }

                    Console.WriteLine("Enter contact number:");
                    string contactNumber = Console.ReadLine();

                    while (string.IsNullOrWhiteSpace(contactNumber))
                    {
                        Console.WriteLine("Enter a valid contact number:");
                        contactNumber = Console.ReadLine();
                    }

                    User newUser = new User(username, email, password, address, contactNumber);

                    Console.WriteLine(newUser.ToString());

                    break;

                default:
                    Console.WriteLine("Invalid Option.");
                    break;
            }

        }

        // [Section] Structures/Structs
        // Structures/structs helps to make a single variable hold related data of various data types

        // Classes vs Structures/Structs
        // Classes are reference types (referencing to the same address in memory) and structs are value types (holds a copy of the entire object)
        // Structures do not support inheritance of fields and methods from other classes
        // Structures cannot have default constructors
        /*
        struct User
        {
            public string username;
            public string email;
            public string password;

            public User(string username, string email, string password)
            {
                this.username = username;
                this.email = email;
                this.password = password;
            }

            public void display()
            {
                Console.WriteLine($"username: {username}");
                Console.WriteLine($"email: {email}");
                Console.WriteLine($"password: {password}");
            }
        }
        */
    }
}