﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace capstone
{
    // This class will serve as a temporary database to store an Array List of items
    internal class ItemsDB
    {
        private ArrayList items = new ArrayList();

        public ArrayList Items { get => items; set => items = value; }

        public ItemsDB() { }
        public ItemsDB(ArrayList items)
        {
            this.items = items;
        }

        public void OpenAdminItemsMenu()
        {
            Console.WriteLine("Choose an option: [1] Retrieve All Items, [2] Create Item, [3] Edit Item, [4] Delete Item, [5] Exit");

            int option = Convert.ToInt32(Console.ReadLine());

            while (option < 1 || option > 5)
            {
                Console.WriteLine("Please choose a valid option");
                option = Convert.ToInt32(Console.ReadLine());
            }

            switch (option)
            {
                case 1:
                    GetAllItems();
                    break;

                case 2:
                    AddItem();
                    break;

                case 3:
                    EditItem();
                    break;

                case 4:
                    DeleteItem();
                    break;

                case 5:
                    Console.WriteLine("Thank you for using the Zuitt Asset Management App!");
                    break;

                default:
                    Console.WriteLine("Invalid Option.");
                    break;
            }
        }

        public void OpenUserItemsMenu()
        {
            Console.WriteLine("Choose an option: [1] Retrieve All Items [2] Exit");

            int option = Convert.ToInt32(Console.ReadLine());

            while (option < 1 || option > 2)
            {
                Console.WriteLine("Please choose a valid option");
                option = Convert.ToInt32(Console.ReadLine());
            }

            switch (option)
            {
                case 1:
                    GetAllItems();
                    break;

                case 2:
                    Console.WriteLine("Thank you for using the Zuitt Asset Management App!");
                    break;

                default:
                    Console.WriteLine("Invalid Option.");
                    break;
            }
        }

        public void GetAllItems()
        {
            if (items.Count == 0)
            {
                Console.WriteLine("No items to display");
            }
            else
            {
                Console.WriteLine("Items List:");
                foreach (Item item in items)
                {
                    Console.WriteLine(item.ToString());
                }
            }
        }

        public void AddItem()
        {
            Console.WriteLine("Enter item name:");
            string itemName = Console.ReadLine();

            while (string.IsNullOrWhiteSpace(itemName))
            {
                Console.WriteLine("Enter a valid product name:");
                itemName = Console.ReadLine();
            }

            foreach (Item item in items)
            {
                if (item.Name == itemName)
                {
                    Console.WriteLine("Item already exists");
                }
                else
                {
                    itemName = Console.ReadLine();
                }
            }

            Console.WriteLine("Enter item branch:");
            string itemBranch = Console.ReadLine();

            while (string.IsNullOrWhiteSpace(itemBranch))
            {
                Console.WriteLine("Enter a valid branch:");
                itemBranch = Console.ReadLine();
            }

            Console.WriteLine("Enter item beginning inventory:");
            int itemBegInv = Convert.ToInt32(Console.ReadLine());

            while (itemBegInv < 0)
            {
                Console.WriteLine("Enter a valid amount:");
                itemBegInv = Convert.ToInt32(Console.ReadLine());
            }

            Console.WriteLine("Enter item stock in:");
            int itemStockIn = Convert.ToInt32(Console.ReadLine());

            while (itemStockIn < 0)
            {
                Console.WriteLine("Enter a valid amount:");
                itemStockIn = Convert.ToInt32(Console.ReadLine());
            }

            Console.WriteLine("Enter item stock out:");
            int itemStockOut = Convert.ToInt32(Console.ReadLine());

            while (itemStockOut < 0)
            {
                Console.WriteLine("Enter a valid amount:");
                itemStockIn = Convert.ToInt32(Console.ReadLine());
            }

            int totalBalance = itemBegInv + itemStockIn - itemStockOut;

            Item newItem = new Item(itemName, itemBranch, itemBegInv, itemStockIn, itemStockOut, totalBalance);

            items.Add(newItem);
            Console.WriteLine("Item successfully added");
            Console.WriteLine(newItem.ToString());
        }

        public void EditItem()
        {
            if (items.Count == 0)
            {
                Console.WriteLine("There are no items to edit. Please add items.");
            }
            else
            {
                Console.WriteLine("Provide the name of the item to edit:");
                string itemNameToEdit = Console.ReadLine();

                int indexItemToEdit = -1;

                foreach (Item item in items)
                {
                    if (itemNameToEdit.ToLower() == item.Name.ToLower())
                    {
                        indexItemToEdit = items.IndexOf(item);
                    }
                }

                if (indexItemToEdit >= 0)
                {
                    Console.WriteLine("Enter new item name:");
                    string newItemName = Console.ReadLine();

                    while (string.IsNullOrWhiteSpace(newItemName))
                    {
                        Console.WriteLine("Enter a valid product name:");
                        newItemName = Console.ReadLine();
                    }

                    Console.WriteLine("Enter item branch:");
                    string newItemBranch = Console.ReadLine();

                    while (string.IsNullOrWhiteSpace(newItemBranch))
                    {
                        Console.WriteLine("Enter a valid branch:");
                        newItemBranch = Console.ReadLine();
                    }

                    Console.WriteLine("Enter item beginning inventory:");
                    int newItemBegInv = Convert.ToInt32(Console.ReadLine());

                    while (newItemBegInv < 0)
                    {
                        Console.WriteLine("Enter a valid amount:");
                        newItemBegInv = Convert.ToInt32(Console.ReadLine());
                    }

                    Console.WriteLine("Enter item stock in:");
                    int newItemStockIn = Convert.ToInt32(Console.ReadLine());

                    while (newItemStockIn < 0)
                    {
                        Console.WriteLine("Enter a valid amount:");
                        newItemStockIn = Convert.ToInt32(Console.ReadLine());
                    }

                    Console.WriteLine("Enter item stock out:");
                    int newItemStockOut = Convert.ToInt32(Console.ReadLine());

                    while (newItemStockOut < 0)
                    {
                        Console.WriteLine("Enter a valid amount:");
                        newItemStockOut = Convert.ToInt32(Console.ReadLine());
                    }

                    int newTotalBalance = newItemBegInv + newItemStockIn - newItemStockOut;

                    Item updatedItem = new Item(newItemName, newItemBranch, newItemBegInv, newItemStockIn, newItemStockOut, newTotalBalance);

                    items[indexItemToEdit] = updatedItem;

                    Console.WriteLine("Item successfully updated");
                    Console.WriteLine(updatedItem.ToString());
                }
            }
        }

        public void DeleteItem()
        {
            if (items.Count == 0)
            {
                Console.WriteLine("There are no items to delete. Please add items.");
            }
            else
            {
                Console.WriteLine("Provide the name of the item to delete:");
                string itemNameToEdit = Console.ReadLine();

                int indexItemToDelete = -1;

                foreach (Item item in items)
                {
                    if (itemNameToEdit.ToLower() == item.Name.ToLower())
                    {
                        indexItemToDelete = items.IndexOf(item);
                    }
                }

                Console.WriteLine("Item successfully Deleted");
                items[indexItemToDelete].ToString();
                items.RemoveAt(indexItemToDelete);

            }
        }
    }
}
