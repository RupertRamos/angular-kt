===============
CRUD Operations
===============

References:
    Tutorials Point documentation
        https://www.tutorialspoint.com/sql/index.htm
    SQL AND/OR Clauses
        https://www.tutorialspoint.com/sql/sql-and-or-clauses.htm
    
Definition of terms:
        Terminal - Terminal where MySQL is accessed

==========
Discussion
==========

1. Run the MySQL/Maria DB.
    Terminal

        Connecting to MySQL:
        Syntax
            - mysql -u username -p password

        mysql -u root -p

        IMPORTANT NOTE:
            - "-u" stands for username.
            - "root" is the default username for sql.
            - "-p" stands for password.
            - An empty value is the default password for sql.

2. Adding a record.
    Terminal

        Adding a record:
        Syntax
            INSERT INTO table_name (column_name) VALUES (value1);

        INSERT INTO artists (name) VALUES ("Incubus");
        INSERT INTO artists (name) VALUES ("Psy");

        IMPORTANT NOTE:
            - Refer to "references" section of this file to find the documentation for Tutorial Point Documentation.

3. Show all records.
    Terminal

        Displaying/retrieving records:
        Syntax
            SELECT column_name FROM table_name;

        IMPORTANT NOTE:   
            - Check if the artists were successfully added
                SELECT * FROM artists;

4. Adding a record with multiple columns.
    Terminal

        Adding a record with multiple columns:
        Syntax
            - INSERT INTO table_name (column_name, column_name) VALUES (value1, value2);

        INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Make Yourself", "1999-10-26", 1);
        INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Psy 6", "2012-1-15", 2);

        IMPORTANT NOTE:
            - Check if the albums were successfully added
                SELECT * FROM albums;

5. Adding multiple records.
    Terminal

        Adding multiple records:
        Syntax
            INSERT INTO table_name (column_name, column_name) VALUES (value1, value2), (value3, value4);

        INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Gangnam Style", 253, "K-Pop", 2), ("Drive", 153, "Rock", 1), ("Pardon Me", 223, "Rock", 1);

        IMPORTANT NOTE:
            - Check if the songs were successfully added
                SELECT * FROM songs;

6. Show records with selected columns.
    Terminal

        Displaying/retrieving records with selected columns:
        Syntax
            SELECT (column_name1, column_name2) FROM table_name;

        SELECT song_name, genre FROM songs;

7. Show records that meet a certain condition.
    Terminal

        Displaying/ retrieving records with certain condtions:
        Syntax
            WHERE clause
            SELECT column_name FROM table_name WHERE condition;

        SELECT song_name FROM songs WHERE genre = "Rock";
        SELECT song_name FROM songs WHERE genre = "rock";
        SELECT song_name FROM songs WHERE length > 223;
        SELECT song_name FROM songs WHERE length >= 223;

        IMPORTANT NOTE:
            - Querying records with strings are case insensitive

8. Show records with multiple conditions.
    Terminal

        Displaying/retrieving records with multiple conditions:
        Syntax
            AND CLAUSE
                SELECT column_name FROM table_name WHERE condition1 AND condition2;
            OR CLAUSE
                SELECT column_name FROM table_name WHERE condition1 OR condition2;

        SELECT song_name FROM songs WHERE genre = "Rock" AND length = 183;
        SELECT song_name FROM songs WHERE genre = "K-Pop" OR length < 200;

        IMPORTANT NOTE:
            - Refer to "references" section of this file to find the documentation for SQL AND/OR Clauses.
            

9. Updating records.
    Terminal

        Add a record to update:
        INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Megalomaniac", 410, "Classical", 2);

        Updating records:
        Syntax
            UPDATE table_name SET column_name = value WHERE condition;

        UPDATE songs SET song_name = "Stellar" WHERE song_name = "Megalomaniac";

        UPDATE table_name SET column_name = value, column_name2 = value2 WHERE condition;
        UPDATE songs SET length = 200, genre = "Rock", album_id = 1 WHERE song_name = "Stellar";

        IMPORTANT NOTE:
            - Removing the WHERE clause will update all the records
            - Check if song was added to the table/updated
                SELECT * FROM songs;

10. Deleting records.
    Terminal

        Deleting records:
        Syntax
            DELETE FROM table_name WHERE condition;

        DELETE FROM songs WHERE genre = "Rock" AND length < 200;

        IMPORTANT NOTE:
            - Check if song was deleted
                SELECT * FROM songs;

========
Activity
========

1. Create a database.
    Terminal

        IMPORTANT NOTE:
            - Refer to the S02 activity solution to create the database


2. Add the user records.
    Terminal

        INSERT INTO users (email, password, datetime_created) VALUES ("johnsmith@gmail.com", "passwordA", "2021-01-01 01:00:00");
        INSERT INTO users (email, password, datetime_created) VALUES ("juandelacruz@gmail.com", "passwordB", "2021-01-01 02:00:00");
        INSERT INTO users (email, password, datetime_created) VALUES ("janesmith@gmail.com", "passwordC", "2021-01-01 03:00:00");
        INSERT INTO users (email, password, datetime_created) VALUES ("mariadelacruz@gmail.com", "passwordD", "2021-01-01 04:00:00");
        INSERT INTO users (email, password, datetime_created) VALUES ("johndoe@gmail.com", "passwordE", "2021-01-01 05:00:00");

3. Add the post records.
    Terminal

        INSERT INTO posts (user_id, title, content, datetime_posted) VALUES (1, "First Code", "Hello World!", "2021-01-02 01:00:00");
        INSERT INTO posts (user_id, title, content, datetime_posted) VALUES (1, "Second Code", "Hello Earth!", "2021-01-02 02:00:00");
        INSERT INTO posts (user_id, title, content, datetime_posted) VALUES (2, "Third Code", "Welcome to Mars!", "2021-01-02 03:00:00");
        INSERT INTO posts (user_id, title, content, datetime_posted) VALUES (4, "Fourth Code", "Bye bye solar system!", "2021-01-02 04:00:00");

4. Get all posts with a user_id of 1.
    Terminal

        SELECT * FROM posts WHERE user_id = 1;

5. Get all the user's email and datetime of creation.
    Terminal

        SELECT email, datetime_created FROM users;

6. Update a post's content with the content of "Hello Earth!".
    Terminal

        UPDATE posts SET content = "Hello to the people of the Earth!" WHERE content = "Hello Earth!";

        IMPORTANT NOTE:
            - Check if post was updated
                SELECT * FROM posts;

7. Delete the user with an email of "johndoe@gmail.com".
    Terminal

        DELETE FROM users WHERE email = "johndoe@gmail.com";

        IMPORTANT NOTE:
            - Check if user was deleted
                SELECT * FROM users;