﻿using System;

namespace discussion
{
    class Discussion
    {

        static void Main(string[] args)
        {

            // [Section] Inheritance
            // Inheritance allows us to inherit or pass fields and methods from one class to another.
            // This allows reusability of code functionality adhering to the DRY principle.
            // The "Crocodile" class has inherited all the properties and methods from the "Animal" and "Reptile" classes
            Crocodile myPet = new Crocodile();
            myPet.Classification = "Reptile";
            myPet.DietType = "Carnivore";
            myPet.Habitat = "Fresh Water";
            myPet.HasScales = true;
            myPet.Name = "Drogon";
            myPet.Age = 7;

            Console.WriteLine("Result of Inheritance:");
            myPet.DescribePet();
            myPet.Swim();
            myPet.Sleep();
            myPet.Eat();

            Crocodile myOtherPet = new Crocodile("Godzilla", 1000, "Reptile", "Carnivore", "Underground", true);

            myOtherPet.DescribePet();
            myOtherPet.Swim();
            myOtherPet.Sleep();
            myOtherPet.Eat();

            // [Section] Inheritance vs Composition
            // Inheritance creates an is-a relationship (e.g. A crocodile is a reptile and is an animal)
            // Composition creates a has-a relationship (e.g. A driver has a car)
            // Inheritance creates a tight coupling of classes meaning when an implementation of the parent/base classes changes, so will the child classes inheriting from them.
            // Composition creates a loose coupling such that when on of the classes change, it may or may not directly affect the other classes.
            Driver myDriver = new Driver("Michael Schumacher", 52);
            Car myCar = new Car("Ferrari", "F430", myDriver);
            // The Name field is not available because the "Car" class did not inherit the "Driver" class but is composed of it
            // Console.WriteLine(myCar.Name);
            Console.WriteLine(myCar.Driver.Name);

            Console.WriteLine("Result of Polymorphism:");
            Turtle myFavoritePet = new Turtle("Rhaegal", 7, "Reptile", "Herbivore", "Salt Water", false);
            myFavoritePet.Swim();
            myPet.Swim();

            Console.WriteLine("Result of Function Overloading:");
            myFavoritePet.Swim();
            myFavoritePet.Swim("Pacific Ocean");

        }

    }
}