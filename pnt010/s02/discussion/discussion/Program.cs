﻿// The "using" keyword is used to include namespaces in a program
// A namespace is a collection of classes
using System;

// The "namespace" keyword is used to declare a namespace
namespace Discussion {
    class Discussion
    {
        // Serves as the entry point of the application
        static void Main(string[] args)
        {
            // Allows to printout values and messages in the console
            Console.WriteLine("Hello World");

            // [Section] Variables, Data Types and Constants

            // Referencing Data Types
            String myString = "Zuitt Asset Manager";

            // Value Types
            int myInt = 1;
            float myFloat = 5.7F;
            double myDouble = 2.5D;
            decimal myDecimal = 8.4M;
            long myLong = 57390L;
            bool myBoolean = false;

            const double interest = 5.3;

            Console.WriteLine("Result of Data Types:");
            Console.WriteLine(myString);
            Console.WriteLine(myInt);
            Console.WriteLine(myFloat);
            Console.WriteLine(myDouble);
            Console.WriteLine(myDecimal);
            Console.WriteLine(myLong);
            Console.WriteLine(myBoolean);
            Console.WriteLine(interest);
            Console.WriteLine("double: {0}, decimal: {1}, boolean: {2}", myDouble, myDecimal, myBoolean);

            // [Section] Type Conversion / TypeCasting
            // Implicit Type Conversion
            // Conversions are done by C# without having to manually define the conversion from one type to the next
            // This works on values converted from smaller to larger integral/number types (e.g. int < float < double)
            float anotherFloat = myInt;
            double anotherDouble = myFloat;

            // Explicit Type Conversion
            // Conversions are done by manually/explicitly defining the data type that a value is to be converted into
            double convertedDouble = (double)myFloat;
            String convertedString = myInt.ToString();

            // [Section] User inputs via the console
            // The "ToInt" method has 3 different bit integer values
            // "ToInt16" converts a number to a short data type
            // "ToInt32" converts a number to an int data type
            // "ToInt64" converts a number to a long data type
            /*
            int userInput = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Please enter a number:");
            Console.WriteLine("The number you provided is {0}", userInput);
            */

            // [Section] Operators
            int numA = 10;
            int numB = 3;

            // Arithmetic Operators
            Console.WriteLine("Result of Arithmetic Operators:");
            Console.WriteLine(numA + numB);
            Console.WriteLine(numA - numB);
            Console.WriteLine(numA * numB);
            Console.WriteLine(numA / numB);
            Console.WriteLine(numA % numB);

            // Increment
            numA++;
            Console.WriteLine(numA);
            // Decrement
            numA--;
            Console.WriteLine(numA);

            // Relational Operators;
            Console.WriteLine("Result of Relational Operators:");
            Console.WriteLine(numA == numB);
            Console.WriteLine(numA != numB);
            Console.WriteLine(numA > numB);
            Console.WriteLine(numA < numB);
            Console.WriteLine(numA >= numB);
            Console.WriteLine(numA <= numB);

            // Logical Operators
            Console.WriteLine("Result of Logical Operators:");
            Console.WriteLine(numA == numB || numA > numB);
            Console.WriteLine(numA == numB && numA > numB);
            Console.WriteLine(!myBoolean);

            // Assignment Operators
            Console.WriteLine("Result of Assignment Operators:");
            Console.WriteLine(numA += numB);
            Console.WriteLine(numA -= numB);
            Console.WriteLine(numA *= numB);
            Console.WriteLine(numA /= numB);
            Console.WriteLine(numA %= numB);

            // [Section] Conditional Statements

            // If, else if and else statements
            Console.WriteLine("Result of If, Else If and Else Statements:");
            if (numA > numB)
            {
                Console.WriteLine("numA is greater than numB");
            } else if (numA == numB)
            {
                Console.WriteLine("numA is equal to numB");
            } else
            {
                Console.WriteLine("numA is less than numB");
            }

            // Switch statements
            // The "ToLower" method returns a copy of the string converted to all lowercase letters
            Console.WriteLine("What day of the week is today:");
            String day = Console.ReadLine().ToLower();

            Console.WriteLine("Result of Switch Statements:");
            switch (day)
            {
                case "monday":
                    Console.WriteLine("Red");
                    break;
                case "tuesday":
                    Console.WriteLine("Blue");
                    break;
                case "wednesday":
                    Console.WriteLine("Green");
                    break;
                default:
                    Console.WriteLine("Black");
                    break;
            }

            // Conditional Operator
            Console.WriteLine("Result of Conditional Operators:");
            Console.WriteLine(numA > numB ? true : false);

            // [Section] Loops

            // While Loops
            Console.WriteLine("Result of While Loops:");
            while (numA > 0)
            {
                Console.WriteLine("value of a: {0}", numA);
                numA--;
            }

            // For Loops
            Console.WriteLine("Result of For Loops:");
            for (int x = 0; x < 5; x++)
            {
                Console.WriteLine("value of x: {0}", x);
            }

            Console.WriteLine("Result of Do-While Loops:");
            do
            {
                Console.WriteLine("value of numA: {0}", numB);
                numB++;
            }
            while (numB < 3);

            // Continue Statements
            Console.WriteLine("Result of Continue Statements:");
            int numC = 0;
            while (numC < 20)
            {
                if (numC % 5 == 0)
                {
                    Console.WriteLine("value of numC: {0}", numC);
                }

                numC++;

                continue;
            }

            // Break Statements
            Console.WriteLine("Result of Break Statements:");
            int numD = 0;
            while (numD < 20)
            {
                if(numD == 10)
                {
                    break;
                }
                if (numC % 5 == 0)
                {
                    Console.WriteLine("value of numD: {0}", numD);
                }

                numD++;
            }
        }
    }
}