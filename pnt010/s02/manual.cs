// ======================================
// Data Types, Expressions and Statements
// ======================================

/*
Other References:
	C# Data Types
		https://www.tutorialspoint.com/csharp/csharp_data_types.htm
	C# Constants
		https://www.tutorialspoint.com/csharp/csharp_constants.htm
	C# Type Conversion
		https://www.tutorialspoint.com/csharp/csharp_type_conversion.htm
	C# Operators
		https://www.tutorialspoint.com/csharp/csharp_operators.htm
	C# Decision Making
		https://www.tutorialspoint.com/csharp/csharp_decision_making.htm
	C# Loops
		https://www.tutorialspoint.com/csharp/csharp_loops.htm
	How to check if a string has only whitespace or is empty in C#
		https://www.educative.io/answers/how-to-check-if-a-string-has-only-whitespace-or-is-empty-in-c-sharp

Definition of terms:
	Batch Folder - The batch folder that will store all files of the students throughout the bootcamp. Normally located in the "Documents" folder of the device.
	Application - Root folder for the application
*/

/*
==========
Discussion
========== 
*/

/*
1. Create a C# console app project.
    Visual Studio
*/

    	// 1a. On the menu bar, choose File > New > Project.
    	// 1b. Choose "C# Console App" from the templates.
    	// 1c. Add the project name as "Discussion" and click on "Next".
    	// 1d. Choose ".NET 6.0" as the framework and click on "Create".

/*
2. Add code to discuss the initial code when creating a C# program.
    Application > discussion > Program.cs
*/

    	// The "using" keyword is used to include namespaces in a program
    	// A namespace is a collection of classes
    	using System;

    	// The "namespace" keyword is used to declare a namespace
    	namespace Discussion {
    	    class Discussion
    	    {
    	        // Serves as the entry point of the application
    	        static void Main(string[] args)
    	        {
    	            // Allows to printout values and messages in the console
    	            Console.WriteLine("Hello World");
    	        }
    	    }
    	}

/*
3. Run the application to see the result of the code.
    Visual Studio
*/

    	/*
    	Important Note:
    		- To run the application, a "green play button" can be found in visual studio which will execute the code added in the previous step.
    		- Alternatively, pressing the "F5" button on the keyboard can be used to run the application.
    		- A debug console will appear to print out the "Hello World" message.
    		- If a project is opened via the "File > Open > Folder" option, running the application will automatically close the debug console.
    		- To open a project, use the "File > Open > Project/Solution" option instead and select the corresponding solution file for the project indicated by the ".sln" extension.
    	*/

/*
4. Add more code to demonstrate the use of variables, data types and constants.
    Application > discussion > Program.cs
*/

    	// ...

    	namespace Discussion {
    	    class Discussion
    	    {
    	        static void Main(string[] args)
    	        {
    	            Console.WriteLine("Hello World");

    	            // [Section] Variables, Data Types and Constants

                    // Referencing Data Types
                    String myString = "Zuitt Asset Manager";

                    // Value Types
                    int myInt = 1;
                    float myFloat = 5.7F;
                    double myDouble = 2.5D;
                    decimal myDecimal = 8.4M;
                    long myLong = 57390L;
                    bool myBoolean = false;

                    const double interest = 5.3;

                    Console.WriteLine("Result of Data Types:");
                    Console.WriteLine(myString);
                    Console.WriteLine(myInt);
                    Console.WriteLine(myFloat);
                    Console.WriteLine(myDouble);
                    Console.WriteLine(myDecimal);
                    Console.WriteLine(myLong);
                    Console.WriteLine(myBoolean);
                    Console.WriteLine(interest);
                    Console.WriteLine("double: {0}, decimal: {1}, boolean: {2}", myDouble, myDecimal, myBoolean);
    	        }
    	    }
    	}

    	/*
    	Important Note:
    		- Refer to "references" section of this file to find the documentations for C# Data Types and C# Constants.
    	*/

/*
5. Add more code to demonstrate the use of type conversion/type casting.
    Application > discussion > Program.cs
*/

    	// ...

    	namespace Discussion {
    	    class Discussion
    	    {
    	        static void Main(string[] args)
    	        {
    	            // ...
                    Console.WriteLine("double: {0}, decimal: {1}, boolean: {2}", myDouble, myDecimal, myBoolean);

                    // [Section] Type Conversion / TypeCasting
                    // Implicit Type Conversion
                    // Conversions are done by C# without having to manually define the conversion from one type to the next
                    // This works on values converted from smaller to larger integral/number types (e.g. int < float < double)
                    float anotherFloat = myInt;
                    double anotherDouble = myFloat;

                    // Explicit Type Conversion
                    // Conversions are done by manually/explicitly defining the data type that a value is to be converted into
                    double convertedDouble = (double)myFloat;
                    String convertedString = myInt.ToString();
    	        }
    	    }
    	}

    	/*
    	Important Note:
    		- Refer to "references" section of this file to find the documentations for C# Type Conversion.
    	*/

/*
6. Add more code to demonstrate the use of user inputs via the console.
    Application > discussion > Program.cs
*/

    	// ...

    	namespace Discussion {
    	    class Discussion
    	    {
    	        static void Main(string[] args)
    	        {
    	            // ...
                    String convertedString = myInt.ToString();

                    // [Section] User inputs via the console
                    // The "ToInt" method has 3 different bit integer values
                    // "ToInt16" converts a number to a short data type
                    // "ToInt32" converts a number to an int data type
                    // "ToInt64" converts a number to a long data type
                    int userInput = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Please enter a number:");
                    Console.WriteLine("The number you provided is {0}", userInput);
    	        }
    	    }
    	}

/*
7. Add more code to demonstrate the use of operators.
    Application > discussion > Program.cs
*/

    	// ...

    	namespace Discussion {
    	    class Discussion
    	    {
    	        static void Main(string[] args)
    	        {
    	            // ...
                    Console.WriteLine("The number you provided is {0}", userInput);

                    // [Section] Operators
                    int numA = 10;
                    int numB = 3;

                    // Arithmetic Operators
                    Console.WriteLine("Result of Arithmetic Operators:");
                    Console.WriteLine(numA + numB);
                    Console.WriteLine(numA - numB);
                    Console.WriteLine(numA * numB);
                    Console.WriteLine(numA / numB);
                    Console.WriteLine(numA % numB);

                    // Increment
                    numA++;
                    Console.WriteLine(numA);
                    // Decrement
                    numA--;
                    Console.WriteLine(numA);

                    // Relational Operators;
                    Console.WriteLine("Result of Relational Operators:");
                    Console.WriteLine(numA == numB);
                    Console.WriteLine(numA != numB);
                    Console.WriteLine(numA > numB);
                    Console.WriteLine(numA < numB);
                    Console.WriteLine(numA >= numB);
                    Console.WriteLine(numA <= numB);

                    // Logical Operators
                    Console.WriteLine("Result of Logical Operators:");
                    Console.WriteLine(numA == numB || numA > numB);
                    Console.WriteLine(numA == numB && numA > numB);
                    Console.WriteLine(!myBoolean);

                    // Assignment Operators
                    Console.WriteLine("Result of Assignment Operators:");
                    Console.WriteLine(numA += numB);
                    Console.WriteLine(numA -= numB);
                    Console.WriteLine(numA *= numB);
                    Console.WriteLine(numA /= numB);
                    Console.WriteLine(numA %= numB);
    	        }
    	    }
    	}

    	/*
    	Important Note:
    		- Refer to "references" section of this file to find the documentations for C# Operators.
    	*/

/*
8. Add more code to demonstrate the use of conditional statements.
    Application > discussion > Program.cs
*/

    	// ...

    	namespace Discussion {
    	    class Discussion
    	    {
    	        static void Main(string[] args)
    	        {
    	            // ...
                    Console.WriteLine(numA %= numB);

                    // [Section] Conditional Statements

                    // If, else if and else statements
                    Console.WriteLine("Result of If, Else If and Else Statements:");
                    if (numA > numB)
                    {
                        Console.WriteLine("numA is greater than numB");
                    } else if (numA == numB)
                    {
                        Console.WriteLine("numA is equal to numB");
                    } else
                    {
                        Console.WriteLine("numA is less than numB");
                    }

                    // Switch statements
                    // The "ToLower" method returns a copy of the string converted to all lowercase letters
                    Console.WriteLine("What day of the week is today:");
                    String day = Console.ReadLine().ToLower();

                    Console.WriteLine("Result of Switch Statements:");
                    switch (day)
                    {
                        case "monday":
                            Console.WriteLine("Red");
                            break;
                        case "tuesday":
                            Console.WriteLine("Blue");
                            break;
                        case "wednesday":
                            Console.WriteLine("Green");
                            break;
                        default:
                            Console.WriteLine("Black");
                            break;
                    }

                    // Conditional Operator
                    Console.WriteLine("Result of Conditional Operators:");
                    Console.WriteLine(numA > numB ? true : false);
    	        }
    	    }
    	}

    	/*
    	Important Note:
    		- Refer to "references" section of this file to find the documentations for C# Decision Making.
    	*/

/*
9. Add more code to demonstrate the use of loops.
    Application > discussion > Program.cs
*/

    	// ...

    	namespace Discussion {
    	    class Discussion
    	    {
    	        static void Main(string[] args)
    	        {
    	            // ...
                    Console.WriteLine(numA > numB ? true : false);

                    // [Section] Loops

                    // While Loops
                    Console.WriteLine("Result of While Loops:");
                    while (numA > 0)
                    {
                        Console.WriteLine("value of a: {0}", numA);
                        numA--;
                    }

                    // For Loops
                    Console.WriteLine("Result of For Loops:");
                    for (int x = 0; x < 5; x++)
                    {
                        Console.WriteLine("value of x: {0}", x);
                    }

                    Console.WriteLine("Result of Do-While Loops:");
                    do
                    {
                        Console.WriteLine("value of numA: {0}", numB);
                        numB++;
                    }
                    while (numB < 3);

                    // Continue Statements
                    Console.WriteLine("Result of Continue Statements:");
                    int numC = 0;
                    while (numC < 20)
                    {
                        if (numC % 5 == 0)
                        {
                            Console.WriteLine("value of numC: {0}", numC);
                        }

                        numC++;

                        continue;
                    }

                    // Break Statements
                    Console.WriteLine("Result of Break Statements:");
                    int numD = 0;
                    while (numD < 20)
                    {
                        if(numD == 10)
                        {
                            break;
                        }
                        if (numC % 5 == 0)
                        {
                            Console.WriteLine("value of numD: {0}", numD);
                        }

                        numD++;
                    }
    	        }
    	    }
    	}

    	/*
    	Important Note:
    		- Refer to "references" section of this file to find the documentations for C# Loops.
    	*/

/*
========
Activity
========
*/

/*
1. Create a simple console application that will take in the user's input to add an asset.
    Application > discussion > Program.cs
*/

    	using System;

    	namespace Activity
    	{
    	    class Activity
    	    {
    	        static void Main(string[] args)
    	        {
    	            Console.WriteLine("Enter product name:");
    	            String name = Console.ReadLine();

    	            while(string.IsNullOrWhiteSpace(name))
    	            {
    	                Console.WriteLine("Enter a valid product name:");
    	                name = Console.ReadLine();
    	            }

    	            Console.WriteLine("Enter beginning inventory amount:");
    	            int beginningInventory = Convert.ToInt32(Console.ReadLine());

    	            while (beginningInventory < 0)
    	            {
    	                Console.WriteLine("Enter a valid beginning inventory amount:");
    	                beginningInventory = Convert.ToInt32(Console.ReadLine());
    	            }

    	            Console.WriteLine("Enter stock in amount:");
    	            int stockIn = Convert.ToInt32(Console.ReadLine());

    	            while (stockIn < 0)
    	            {
    	                Console.WriteLine("Enter a valid stock in amount:");
    	                stockIn = Convert.ToInt32(Console.ReadLine());
    	            }

    	            Console.WriteLine("Enter stock out amount:");
    	            int stockOut = Convert.ToInt32(Console.ReadLine());

    	            while (stockOut < 0)
    	            {
    	                Console.WriteLine("Enter a valid stockOut amount:");
    	                stockOut = Convert.ToInt32(Console.ReadLine());
    	            }

    	            int balance = beginningInventory + stockIn - stockOut;
    	            Console.WriteLine("The total balance is:");
    	            Console.WriteLine(balance);

    	            if (balance > 100)
    	            {
    	                Console.WriteLine("Too many available stocks. Kindly consume first before ordering again.");
    	            } else if (balance < 10)
    	            {
    	                Console.WriteLine("Stocks are low. Kindly contact your suppliers.");
    	            } else
    	            {
    	                Console.WriteLine("Stocks are sufficient.");
    	            }

    	        }
    	    }
    	}

    	/*
    	Important Note:
    		- Provide the students with the documentation on How to check if a string has only whitespace or is empty in C# in order to complete the activity.
    	*/