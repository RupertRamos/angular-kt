﻿using System;

namespace Activity
{
    class Activity
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter product name:");
            string name = Console.ReadLine();

            while(string.IsNullOrWhiteSpace(name))
            {
                Console.WriteLine("Enter a valid product name:");
                name = Console.ReadLine();
            }

            Console.WriteLine("Enter beginning inventory amount:");
            int beginningInventory = Convert.ToInt32(Console.ReadLine());

            while (beginningInventory < 0)
            {
                Console.WriteLine("Enter a valid beginning inventory amount:");
                beginningInventory = Convert.ToInt32(Console.ReadLine());
            }

            Console.WriteLine("Enter stock in amount:");
            int stockIn = Convert.ToInt32(Console.ReadLine());

            while (stockIn < 0)
            {
                Console.WriteLine("Enter a valid stock in amount:");
                stockIn = Convert.ToInt32(Console.ReadLine());
            }

            Console.WriteLine("Enter stock out amount:");
            int stockOut = Convert.ToInt32(Console.ReadLine());

            while (stockOut < 0)
            {
                Console.WriteLine("Enter a valid stockOut amount:");
                stockOut = Convert.ToInt32(Console.ReadLine());
            }

            int balance = beginningInventory + stockIn - stockOut;
            Console.WriteLine("The total balance is:");
            Console.WriteLine(balance);

            if (balance > 100)
            {
                Console.WriteLine("Too many available stocks. Kindly consume first before ordering again.");
            } else if (balance < 10)
            {
                Console.WriteLine("Stocks are low. Kindly contact your suppliers.");
            } else
            {
                Console.WriteLine("Stocks are sufficient.");
            }

        }
    }
}