==============================
Basics of Relational Databases
==============================

References:
	Database Structure and Design Tutorial
		https://www.lucidchart.com/pages/database-diagram/database-design
	Entity Relationship Diagram
		https://www.smartdraw.com/entity-relationship-diagram/
	Google Drawings
		https://docs.google.com/drawings

Definition of terms:
	Batch Folder - The batch folder that will store all files of the students throughout the bootcamp. Normally located in the "Documents" folder of the device.
	Application - Root folder for the application

==========
Discussion
==========

1. Discuss the slides and discuss database design concepts.
	Google Slides

========
Activity
========

1. Have the students answer the following questions and create an ERD and database design tables