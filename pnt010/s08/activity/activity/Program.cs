﻿using activity;
using System;
using System.Collections;

namespace Activity
{
    class Activity
    {

        static void Main(string[] args)
        {

            // Nested Types
            House myHouse = new House("Bungalow", "1746 Jose Rizal St. Manila");
            House.Bedroom myBedroom = new House.Bedroom("Queen Sized Bed", new ArrayList());

            Console.WriteLine("Result from Nested Types:");
            Console.WriteLine(myBedroom.Bed);
            myBedroom.AddFurniture("Study Table");
            myBedroom.AddFurniture("Night Stand");

            myBedroom.CheckFurnitures();

            House.Bathroom myBathroom = new House.Bathroom(true, true);
            Console.WriteLine(myBathroom.HasShower);
            Console.WriteLine(myBathroom.HasBathTub);

            myBathroom.OpenShower();

            // Inheriting Nested Types
            House myOtherHouse = new TwoStoryHouse("Multi Floored", "573 John Street, California", true);
            House.Bedroom myOtherBedroom = new House.Bedroom();
            House.Bathroom myOtherBathroom = new House.Bathroom(true, true);

            Console.WriteLine("Result from Nested Types:");
            Console.WriteLine(myOtherBedroom.Bed);
            myOtherBedroom.AddFurniture("Sofa");
            myOtherBedroom.AddFurniture("TV");

            myOtherBedroom.CheckFurnitures();
            Console.WriteLine(myOtherBathroom.HasShower);
            Console.WriteLine(myOtherBathroom.HasBathTub);

            // Partial Classes
            Factory myFactory = new Factory("Fries", 1, true);
            Console.WriteLine("Result from Partial Classes:");
            Console.WriteLine(myFactory.Product);
            Console.WriteLine(myFactory.QuantityPerPackaging);
            Console.WriteLine(myFactory.IsInspected);

        }

    }
}