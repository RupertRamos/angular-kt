﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace activity
{
    internal class TwoStoryHouse : House
    {
        private bool hasGarden;

        public bool HasGarden { get => hasGarden; set => hasGarden = value; }

        public TwoStoryHouse() { }
        public TwoStoryHouse(string type, string address, bool hasGarden)
        {
            Type = type;
            Address = address;
            this.hasGarden = hasGarden;
        }

    }
}
