﻿using System.Collections;

namespace activity
{
    class Activity
    {

        static void Main(string[] args)
        {

            ItemsDB items = new ItemsDB();
            bool isAppRunning = true;

            while(isAppRunning == true)
            {

                Console.WriteLine("Welcome to Zuitt Asset Management App!");
                Console.WriteLine("Choose an option: [1] Retrieve All Items, [2] Create Item, [3] Edit Item, [4] Delete Item, [5] Exit");

                int option = Convert.ToInt32(Console.ReadLine());

                while (option < 1 || option > 5)
                {
                    Console.WriteLine("Please choose a valid option");
                    option = Convert.ToInt32(Console.ReadLine());
                }

                switch (option)
                {
                    case 1:
                        items.GetAllItems();
                        break;

                    case 2:
                        items.AddItem();
                        break;

                    case 3:
                        items.EditItem();
                        break;

                    case 4:
                        items.DeleteItem();
                        break;

                    case 5:
                        isAppRunning = false;
                        Console.WriteLine("Thank you for using the Zuitt Asset Management App!");
                        break;

                    default:
                        Console.WriteLine("Invalid Option.");
                        break;
                }
            }

        }

    }
}