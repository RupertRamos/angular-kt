// ======================
// Arrays and Collections
// ======================

/*
Other References:
    C# Arrays
        https://www.tutorialspoint.com/csharp/csharp_arrays.htm
    C# ArrayLists
        https://learn.microsoft.com/en-us/dotnet/api/system.collections.arraylist?view=net-6.0
    C# HashMaps
        https://learn.microsoft.com/en-us/dotnet/api/system.collections.hashtable?view=net-6.0

Definition of terms:
	Batch Folder - The batch folder that will store all files of the students throughout the bootcamp. Normally located in the "Documents" folder of the device.
	Application - Root folder for the application
*/

/*
==========
Discussion
==========
*/

/*
1. Create a C# console app project.
    Visual Studio
*/

    	// 1a. On the menu bar, choose File > New > Project.
    	// 1b. Choose "C# Console App" from the templates.
    	// 1c. Add the project name as "Discussion" and click on "Next".
    	// 1d. Choose ".NET 6.0" as the framework and click on "Create".

/*
2. Add code to demonstrate the use of arrays.
    Application > discussion > Program.cs
*/

    	// ...

        namespace discussion
        {
            class Discussion
            {

                static void Main(string[] args)
                {

                    // [Section] Arrays
                    // An array stores a fixed-size sequential collection of elements of the same type.
                    // An array is used to store a collection of data.

                    // Declaring Arrays
                    // An array is a reference type, so the "new" keyword is used to create an instance of an array
                    int[] sales = new int[3];

                    // Assigning values to an array
                    // C# arrays start with the 0 index value as the first element
                    sales[0] = 10;
                    sales[1] = 5;
                    sales[2] = 27;

                    Console.WriteLine("Result from arrays:");
                    Console.WriteLine(sales[0]);
                    Console.WriteLine(sales[1]);
                    Console.WriteLine(sales[2]);
                    Console.WriteLine(sales);
                    Console.WriteLine($"First Element: {sales[0]}, Second Element: {sales[1]}, Third Element: {sales[2]}");

                    // Printing the length of an array
                    Console.WriteLine(sales.Length);

                    // Array declaration and initialization
                    string[] managers = new string[3]
                    {
                        "john", "jane", "joe"
                    };

                    // Using the forEach loop
                    Console.WriteLine("Result from array for loop:");
                    for (int x = 0; x < managers.Length; x++)
                    {
                        Console.WriteLine(managers[x]);
                    }

                    Console.WriteLine("Result from array forEach loop:");
                    foreach (string manager in managers)
                    {
                        Console.WriteLine(manager);
                    }

                }
            }
        }

        /*
        Important Note:
            - Refer to "references" section of this file to find the documentation for C# Arrays.
        */

/*
3. Add more code to demonstrate the use of Array Lists.
    Application > discussion > Program.cs
*/

    	// ...

        namespace discussion
        {
            class Discussion
            {

                static void Main(string[] args)
                {

                    // ...
                    foreach (string manager in managers)
                    {
                        // ...
                    }

                    // [Section] Collections
                    // ArrayList
                    // A resizeable list represents an ordered collection of an object that can be indexed individually.

                    // Declaring a list
                    ArrayList myArrayList = new ArrayList();

                    // Declaring and initializing an array list
                    ArrayList customers = new ArrayList(new string[] { "Donald", "Mickey", "Goofy" });

                    // Getting the index of an element
                    customers.IndexOf("Mickey");

                    // Getting the length of an Array List
                    Console.WriteLine("Result of Count property:");
                    Console.WriteLine(customers.Count);

                    Console.WriteLine("Result from Array List:");
                    foreach (string customer in customers)
                    {
                        Console.WriteLine(customer);
                    }

                    // Adding elements to an array list
                    myArrayList.Add("Harry");
                    myArrayList.Add("Ron");
                    myArrayList.Add("Hermione");

                    Console.WriteLine("Result from adding Array List elements:");
                    foreach (string arrayListItem in myArrayList)
                    {
                        Console.WriteLine(arrayListItem);
                    }

                    // Adding elements in between an array list
                    myArrayList.Insert(1, "Tom");

                    Console.WriteLine("Result from adding Array List elements in between:");
                    foreach (string arrayListItem in myArrayList)
                    {
                        Console.WriteLine(arrayListItem);
                    }

                    // Removing elements to an array list
                    // Removing single elements
                    customers.Remove("Mickey");
                    customers.RemoveAt(0);

                    Console.WriteLine("Result from removing single Array List elements:");
                    foreach (string customer in customers)
                    {
                        Console.WriteLine(customer);
                    }

                    // Removing multiple elements
                    // Removes 2 elements from index 1
                    myArrayList.RemoveRange(1, 2);

                    Console.WriteLine("Result from removing multiple Array List elements:");
                    foreach (string arrayListItem in myArrayList)
                    {
                        Console.WriteLine(arrayListItem);
                    }

                    // Updating array list elements
                    myArrayList[0] = "Luna";
                    myArrayList[1] = "Draco";

                    Console.WriteLine("Result from updating Array List elements:");
                    foreach (string arrayListItem in myArrayList)
                    {
                        Console.WriteLine(arrayListItem);
                    }

                    // Sorting Arrays
                    myArrayList.Sort();

                    Console.WriteLine("Result from sorting Array List elements:");
                    foreach (string arrayListItem in myArrayList)
                    {
                        Console.WriteLine(arrayListItem);
                    }

                }
            }
        }

        /*
        Important Note:
            - Refer to "references" section of this file to find the documentation for C# Array Lists.
        */

/*
4. Create a Transaction Class that will be used to demonstrate the implementation of Array Lists with real world applications.
    Application > discussion > Transaction.cs
*/

    	// ...

        namespace discussion
        {
            internal class Transaction
            {

                private string name;
                private int beginningInventory;
                private int stockIn;
                private int stockOut;
                private int totalBalance;
                private string transactionDate;

                public string Name { get => name; set => name = value; }
                public int BeginningInventory { get => beginningInventory; set => beginningInventory = value; }
                public int StockIn { get => stockIn; set => stockIn = value; }
                public int StockOut { get => stockOut; set => stockOut = value; }
                public int TotalBalance { get => totalBalance; set => totalBalance = value; }
                public string TransactionDate { get => transactionDate; set => transactionDate = value; }

                public Transaction() { }

                public Transaction(string name, int beginningInventory, int stockIn, int stockOut, int totalBalance, string transactionDate)
                {
                    this.name = name;
                    this.beginningInventory = beginningInventory;
                    this.stockIn = stockIn;
                    this.stockOut = stockOut;
                    this.totalBalance = totalBalance;
                    this.transactionDate = transactionDate;
                }

                public override string ToString()
                {
                    return $"{{Name: {name}, Beginning Inventory: {beginningInventory}, Stock In: {stockIn}, Stock Out: {stockOut}, Total Balance: {totalBalance}}}";
                }


            }
        }


/*
5. Add more code to demonstrate the real world use of Array Lists.
    Application > discussion > Program.cs
*/

    	// ...

        namespace discussion
        {
            class Discussion
            {

                static void Main(string[] args)
                {

                    // ...
                    foreach (string arrayListItem in myArrayList)
                    {
                        // ...
                    }

                    // Array List of Objects
                    // It is common practice to manipulate a list of objects when dealing with data
                    // Mastery of manipulating a collection of objects is key to creating efficient applications

                    // Creating individual transactions
                    Transaction transactionA = new Transaction("Chicken", 0, 100, 25, 75, "10/21/2022");
                    Transaction transactionB = new Transaction("Beef", 0, 50, 10, 40, "10/21/2022");
                    Transaction transactionC = new Transaction("Beef", 40, 100, 20, 120, "10/22/2022");
                    Transaction transactionD = new Transaction("Chicken", 75, 150, 50, 175, "10/22/2022");

                    // Creating an array list of transactions that simulate date from a database
                    ArrayList transactions = new ArrayList(new Transaction[] { transactionA, transactionB, transactionC, transactionD });

                    // Loop to view original transaction amounts for checking
                    Console.WriteLine("Result of original transactions:");
                    foreach (Transaction transaction in transactions)
                    {
                        Console.WriteLine(transaction.ToString());
                    }

                    // Creates a list of uniqueTransactionNames
                    ArrayList uniqueTransactionNames = new ArrayList();

                    // Array list to contain reduced/combined instructions
                    ArrayList reducedTransactions = new ArrayList();

                    // Loop to grab the unique item names from the data
                    foreach(Transaction transaction in transactions)
                    {
                        // The "Contains" method checks if the transaction name already exists in the uniqueTransactionNames array list
                        // If false, adds the name to the list. If true, does nothing
                        if (!uniqueTransactionNames.Contains(transaction.Name))
                        {
                            uniqueTransactionNames.Add(transaction.Name);
                        }
                    }

                    // Loop that will grab each unique item name
                    foreach(string uniqueTransactionName in uniqueTransactionNames)
                    {
                        // Variable to store the total values for all transactions with the same name
                        Transaction finalizedTransaction = new Transaction();

                        // Loop to compute for the total amount for all transactions matching the uniqueTransactionName
                        foreach (Transaction transaction in transactions)
                        {
                            // If the transaction name matches then unique item name adds the total values
                            if (uniqueTransactionName == transaction.Name)
                            {
                                finalizedTransaction.Name = transaction.Name;
                                finalizedTransaction.StockIn += transaction.StockIn;
                                finalizedTransaction.StockOut += transaction.StockOut;
                                finalizedTransaction.TotalBalance = finalizedTransaction.TotalBalance + transaction.StockIn - transaction.StockOut;
                            }
                        }
                        reducedTransactions.Add(finalizedTransaction);
                    }

                    Console.WriteLine("Result of reduced transactions:");
                    foreach(Transaction reducedTransaction in reducedTransactions)
                    {
                        Console.WriteLine(reducedTransaction.ToString());
                    }

                }
            }
        }

/*
6. Add more code to demonstrate the use of Hashtables.
    Application > discussion > Program.cs
*/

        // ...

        namespace discussion
        {
            class Discussion
            {

                static void Main(string[] args)
                {

                    // ...
                    foreach(Transaction reducedTransaction in reducedTransactions)
                    {
                        // ...
                    }

                    // Hashtables
                    // Hashtables represents a collection of key-value pairs.
                    // It is used when you need to access elements by using keys.

                    // Declaring hashtables
                    Hashtable myHashtable = new Hashtable();

                    // Initializing Hashtables
                    Hashtable address = new Hashtable();

                    address.Add("houseNumber", "15");
                    address.Add("street", "Apple");
                    address.Add("city", "California");
                    address.Add("zip", "19463");

                    // Getting the keys of a hashtable
                    ICollection addressKeys = address.Keys;

                    // Accessing Hashtable Keys
                    Console.WriteLine("Result from accessing Hashtable keys:");
                    Console.WriteLine(address["street"]);

                    Console.WriteLine("Result from Hashtables:");
                    foreach(string key in addressKeys)
                    {
                        Console.WriteLine(address[key]);
                    }

                }
            }
        }

        /*
        Important Note:
            - Refer to "references" section of this file to find the documentation for C# Hashtables.
        */

/*
========
Activity
========
*/

/*
1. Create a C# console app project.
    Visual Studio
*/

        // 1a. On the menu bar, choose File > New > Project.
        // 1b. Choose "C# Console App" from the templates.
        // 1c. Add the project name as "Discussion" and click on "Next".
        // 1d. Choose ".NET 6.0" as the framework and click on "Create".

/*
2. Provide the students the following code from the previous session's activity to create an Item class.
    Application > discussion > Item.cs
*/

        // ...

        namespace discussion
        {
            internal class Item
            {

                private string name;
                private readonly string branch;
                private int beginningInventory;
                private int stockIn;
                private int stockOut;
                private int totalBalance;

                public string Name { get => name; set => name = value; }
                public string Branch { get => branch; }
                public int BeginningInventory { get => beginningInventory; set => beginningInventory = value; }
                public int StockIn { get => stockIn; set => stockIn = value; }
                public int StockOut { get => stockOut; set => stockOut = value; }
                public int TotalBalance { get => totalBalance; set => totalBalance = value; }


                public Item()
                {
                }

                public Item(string name, string branch, int beginningInventory, int stockIn, int stockOut, int totalBalance)
                {
                    this.name = name;
                    this.branch = branch;
                    this.beginningInventory = beginningInventory;
                    this.stockIn = stockIn;
                    this.stockOut = stockOut;
                    this.totalBalance = totalBalance;
                }

                public override string ToString()
                {
                    return $"{{Name: {name}, Branch: {branch}, Beginning Inventory: {beginningInventory}, Stock In: {stockIn}, Stock Out: {stockOut}, Total Balance: {totalBalance}}}";
                }

            }
        }

        /*
        Important Note:
            - The contents of this file may be found in the template folder found in this session's root folder to be provided to the trainees/students for reference.
        */

/*
3. Create an ItemsDB Class that will have a field of items which will function as the database for our app.
    Application > discussion > ItemsDB.cs
*/

    	// ...

        namespace activity
        {
            // This class will serve as a temporary database to store an Array List of items
            internal class ItemsDB
            {
                private ArrayList items = new ArrayList();

                public ArrayList Items { get => items; set => items = value; }

                public ItemsDB() { }
                public ItemsDB(ArrayList items)
                {
                    this.items = items;
                }

                public void GetAllItems()
                {
                    if (items.Count == 0)
                    {
                        Console.WriteLine("No items to display");
                    }
                    else
                    {
                        Console.WriteLine("Items List:");
                        foreach (Item item in items)
                        {
                            Console.WriteLine(item.ToString());
                        }
                    }
                }

                public void AddItem()
                {
                    Console.WriteLine("Enter item name:");
                    string itemName = Console.ReadLine();

                    while (string.IsNullOrWhiteSpace(itemName))
                    {
                        Console.WriteLine("Enter a valid product name:");
                        itemName = Console.ReadLine();
                    }

                    foreach (Item item in items)
                    {
                        if (item.Name == itemName)
                        {
                            Console.WriteLine("Item already exists");
                        }
                        else
                        {
                            itemName = Console.ReadLine();
                        }
                    }

                    Console.WriteLine("Enter item branch:");
                    string itemBranch = Console.ReadLine();

                    while (string.IsNullOrWhiteSpace(itemBranch))
                    {
                        Console.WriteLine("Enter a valid branch:");
                        itemBranch = Console.ReadLine();
                    }

                    Console.WriteLine("Enter item beginning inventory:");
                    int itemBegInv = Convert.ToInt32(Console.ReadLine());

                    while (itemBegInv < 0)
                    {
                        Console.WriteLine("Enter a valid amount:");
                        itemBegInv = Convert.ToInt32(Console.ReadLine());
                    }

                    Console.WriteLine("Enter item stock in:");
                    int itemStockIn = Convert.ToInt32(Console.ReadLine());

                    while (itemStockIn < 0)
                    {
                        Console.WriteLine("Enter a valid amount:");
                        itemStockIn = Convert.ToInt32(Console.ReadLine());
                    }

                    Console.WriteLine("Enter item stock out:");
                    int itemStockOut = Convert.ToInt32(Console.ReadLine());

                    while (itemStockOut < 0)
                    {
                        Console.WriteLine("Enter a valid amount:");
                        itemStockIn = Convert.ToInt32(Console.ReadLine());
                    }

                    int totalBalance = itemBegInv + itemStockIn - itemStockOut;

                    Item newItem = new Item(itemName, itemBranch, itemBegInv, itemStockIn, itemStockOut, totalBalance);

                    items.Add(newItem);
                    Console.WriteLine("Item successfully added");
                    Console.WriteLine(newItem.ToString());
                }

                public void EditItem()
                {
                    if (items.Count == 0)
                    {
                        Console.WriteLine("There are no items to edit. Please add items.");
                    }
                    else
                    {
                        Console.WriteLine("Provide the name of the item to edit:");
                        string itemNameToEdit = Console.ReadLine();

                        int indexItemToEdit = -1;

                        foreach (Item item in items)
                        {
                            if (itemNameToEdit.ToLower() == item.Name.ToLower())
                            {
                                indexItemToEdit = items.IndexOf(item);
                            }
                        }

                        if (indexItemToEdit >= 0)
                        {
                            Console.WriteLine("Enter new item name:");
                            string newItemName = Console.ReadLine();

                            while (string.IsNullOrWhiteSpace(newItemName))
                            {
                                Console.WriteLine("Enter a valid product name:");
                                newItemName = Console.ReadLine();
                            }

                            Console.WriteLine("Enter item branch:");
                            string newItemBranch = Console.ReadLine();

                            while (string.IsNullOrWhiteSpace(newItemBranch))
                            {
                                Console.WriteLine("Enter a valid branch:");
                                newItemBranch = Console.ReadLine();
                            }

                            Console.WriteLine("Enter item beginning inventory:");
                            int newItemBegInv = Convert.ToInt32(Console.ReadLine());

                            while (newItemBegInv < 0)
                            {
                                Console.WriteLine("Enter a valid amount:");
                                newItemBegInv = Convert.ToInt32(Console.ReadLine());
                            }

                            Console.WriteLine("Enter item stock in:");
                            int newItemStockIn = Convert.ToInt32(Console.ReadLine());

                            while (newItemStockIn < 0)
                            {
                                Console.WriteLine("Enter a valid amount:");
                                newItemStockIn = Convert.ToInt32(Console.ReadLine());
                            }

                            Console.WriteLine("Enter item stock out:");
                            int newItemStockOut = Convert.ToInt32(Console.ReadLine());

                            while (newItemStockOut < 0)
                            {
                                Console.WriteLine("Enter a valid amount:");
                                newItemStockOut = Convert.ToInt32(Console.ReadLine());
                            }

                            int newTotalBalance = newItemBegInv + newItemStockIn - newItemStockOut;

                            Item updatedItem = new Item(newItemName, newItemBranch, newItemBegInv, newItemStockIn, newItemStockOut, newTotalBalance);

                            items[indexItemToEdit] = updatedItem;

                            Console.WriteLine("Item successfully updated");
                            Console.WriteLine(updatedItem.ToString());
                        }
                    }
                }

                public void DeleteItem()
                {
                    if (items.Count == 0)
                    {
                        Console.WriteLine("There are no items to delete. Please add items.");
                    }
                    else
                    {
                        Console.WriteLine("Provide the name of the item to delete:");
                        string itemNameToEdit = Console.ReadLine();

                        int indexItemToDelete = -1;

                        foreach (Item item in items)
                        {
                            if (itemNameToEdit.ToLower() == item.Name.ToLower())
                            {
                                indexItemToDelete = items.IndexOf(item);
                            }
                        }

                        Console.WriteLine("Item successfully Deleted");
                        items[indexItemToDelete].ToString();
                        items.RemoveAt(indexItemToDelete);

                    }
                }
            }
        }

/*
4. Instantiate and invoke the methods of the ItemsDB class in order to make the app work.
    Application > discussion > Program.cs
*/

        // ...

        namespace activity
        {
            class Activity
            {

                static void Main(string[] args)
                {

                    ItemsDB items = new ItemsDB();
                    bool isAppRunning = true;

                    while(isAppRunning == true)
                    {

                        Console.WriteLine("Welcome to Zuitt Asset Management App!");
                        Console.WriteLine("Choose an option: [1] Retrieve All Items, [2] Create Item, [3] Edit Item, [4] Delete Item, [5] Exit");

                        int option = Convert.ToInt32(Console.ReadLine());

                        while (option < 1 || option > 5)
                        {
                            Console.WriteLine("Please choose a valid option");
                            option = Convert.ToInt32(Console.ReadLine());
                        }

                        switch (option)
                        {
                            case 1:
                                items.GetAllItems();
                                break;

                            case 2:
                                items.AddItem();
                                break;

                            case 3:
                                items.EditItem();
                                break;

                            case 4:
                                items.DeleteItem();
                                break;

                            case 5:
                                isAppRunning = false;
                                Console.WriteLine("Thank you for using the Zuitt Asset Management App!");
                                break;

                            default:
                                Console.WriteLine("Invalid Option.");
                                break;
                        }
                    }

                }

            }
        }

