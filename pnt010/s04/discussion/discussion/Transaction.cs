﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace discussion
{
    internal class Transaction
    {

        private string name;
        private int beginningInventory;
        private int stockIn;
        private int stockOut;
        private int totalBalance;
        private string transactionDate;

        public string Name { get => name; set => name = value; }
        public int BeginningInventory { get => beginningInventory; set => beginningInventory = value; }
        public int StockIn { get => stockIn; set => stockIn = value; }
        public int StockOut { get => stockOut; set => stockOut = value; }
        public int TotalBalance { get => totalBalance; set => totalBalance = value; }
        public string TransactionDate { get => transactionDate; set => transactionDate = value; }

        public Transaction() { }

        public Transaction(string name, int beginningInventory, int stockIn, int stockOut, int totalBalance, string transactionDate)
        {
            this.name = name;
            this.beginningInventory = beginningInventory;
            this.stockIn = stockIn;
            this.stockOut = stockOut;
            this.totalBalance = totalBalance;
            this.transactionDate = transactionDate;
        }

        public override string ToString()
        {
            return $"{{Name: {name}, Beginning Inventory: {beginningInventory}, Stock In: {stockIn}, Stock Out: {stockOut}, Total Balance: {totalBalance}}}";
        }


    }
}
