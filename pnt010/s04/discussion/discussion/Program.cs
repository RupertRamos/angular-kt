﻿using System;
using System.Collections;

namespace discussion
{
    class Discussion
    {

        static void Main(string[] args)
        {

            // [Section] Arrays
            // An array stores a fixed-size sequential collection of elements of the same type.
            // An array is used to store a collection of data.

            // Declaring Arrays
            // An array is a reference type, so the "new" keyword is used to create an instance of an array
            int[] sales = new int[3];

            // Assigning values to an array
            // C# arrays start with the 0 index value as the first element
            sales[0] = 10;
            sales[1] = 5;
            sales[2] = 27;

            Console.WriteLine("Result from arrays:");
            Console.WriteLine(sales[0]);
            Console.WriteLine(sales[1]);
            Console.WriteLine(sales[2]);
            Console.WriteLine(sales);
            Console.WriteLine($"First Element: {sales[0]}, Second Element: {sales[1]}, Third Element: {sales[2]}");

            // Printing the length of an array
            Console.WriteLine(sales.Length);

            // Array declaration and initialization
            string[] managers = new string[3]
            {
                "john", "jane", "joe"
            };

            // Using the forEach loop
            Console.WriteLine("Result from array for loop:");
            for (int x = 0; x < managers.Length; x++)
            {
                Console.WriteLine(managers[x]);
            }

            Console.WriteLine("Result from array forEach loop:");
            foreach (string manager in managers)
            {
                Console.WriteLine(manager);
            }

            // [Section] Collections
            // ArrayList
            // A resizeable list that represents an ordered collection of an object that can be indexed individually.

            // Declaring a list
            ArrayList myArrayList = new ArrayList();

            // Declaring and initializing an array list
            ArrayList customers = new ArrayList(new string[] { "Donald", "Mickey", "Goofy" });

            // Getting the index of an element
            customers.IndexOf("Mickey");

            Console.WriteLine("Result from Array List:");
            foreach (string customer in customers)
            {
                Console.WriteLine(customer);
            }

            // Getting the length of an Array List
            Console.WriteLine("Result of Count property:");
            Console.WriteLine(customers.Count);

            // Adding elements to an array list
            myArrayList.Add("Harry");
            myArrayList.Add("Ron");
            myArrayList.Add("Hermione");

            Console.WriteLine("Result from adding Array List elements:");
            foreach (string arrayListItem in myArrayList)
            {
                Console.WriteLine(arrayListItem);
            }

            // Adding elements in between an array list
            myArrayList.Insert(1, "Tom");

            Console.WriteLine("Result from adding Array List elements in between:");
            foreach (string arrayListItem in myArrayList)
            {
                Console.WriteLine(arrayListItem);
            }

            // Removing elements to an array list
            // Removing single elements
            customers.Remove("Mickey");
            customers.RemoveAt(0);

            Console.WriteLine("Result from removing single Array List elements:");
            foreach (string customer in customers)
            {
                Console.WriteLine(customer);
            }

            // Removing multiple elements
            // Removes 2 elements from index 1
            myArrayList.RemoveRange(1, 2);

            Console.WriteLine("Result from removing multiple Array List elements:");
            foreach (string arrayListItem in myArrayList)
            {
                Console.WriteLine(arrayListItem);
            }

            // Updating array list elements
            myArrayList[0] = "Luna";
            myArrayList[1] = "Draco";

            Console.WriteLine("Result from updating Array List elements:");
            foreach (string arrayListItem in myArrayList)
            {
                Console.WriteLine(arrayListItem);
            }

            // Sorting Arrays
            myArrayList.Sort();

            Console.WriteLine("Result from sorting Array List elements:");
            foreach (string arrayListItem in myArrayList)
            {
                Console.WriteLine(arrayListItem);
            }

            // Array List of Objects
            // It is common practice to manipulate a list of objects when dealing with data
            // Mastery of manipulating a collection of objects is key to creating efficient applications

            // Creating individual transactions
            Transaction transactionA = new Transaction("Chicken", 0, 100, 25, 75, "10/21/2022");
            Transaction transactionB = new Transaction("Beef", 0, 50, 10, 40, "10/21/2022");
            Transaction transactionC = new Transaction("Beef", 40, 100, 20, 120, "10/22/2022");
            Transaction transactionD = new Transaction("Chicken", 75, 150, 50, 175, "10/22/2022");

            // Creating an array list of transactions that simulate date from a database
            ArrayList transactions = new ArrayList(new Transaction[] { transactionA, transactionB, transactionC, transactionD });

            // Loop to view original transaction amounts for checking
            Console.WriteLine("Result of original transactions:");
            foreach (Transaction transaction in transactions)
            {
                Console.WriteLine(transaction.ToString());
            }

            // Creates a list of uniqueTransactionNames
            ArrayList uniqueTransactionNames = new ArrayList();

            // Array list to contain reduced/combined instructions
            ArrayList reducedTransactions = new ArrayList();

            // Loop to grab the unique item names from the data
            foreach(Transaction transaction in transactions)
            {
                // The "Contains" method checks if the transaction name already exists in the uniqueTransactionNames array list
                // If false, adds the name to the list. If true, does nothing
                if (!uniqueTransactionNames.Contains(transaction.Name))
                {
                    uniqueTransactionNames.Add(transaction.Name);
                }
            }

            // Loop that will grab each unique item name
            foreach(string uniqueTransactionName in uniqueTransactionNames)
            {
                // Variable to store the total values for all transactions with the same name
                Transaction finalizedTransaction = new Transaction();

                // Loop to compute for the total amount for all transactions matching the uniqueTransactionName
                foreach (Transaction transaction in transactions)
                {
                    // If the transaction name matches then unique item name adds the total values
                    if (uniqueTransactionName == transaction.Name)
                    {
                        finalizedTransaction.Name = transaction.Name;
                        finalizedTransaction.StockIn += transaction.StockIn;
                        finalizedTransaction.StockOut += transaction.StockOut;
                        finalizedTransaction.TotalBalance = finalizedTransaction.TotalBalance + transaction.StockIn - transaction.StockOut;
                    }
                }
                reducedTransactions.Add(finalizedTransaction);
            }

            Console.WriteLine("Result of reduced transactions:");
            foreach(Transaction reducedTransaction in reducedTransactions)
            {
                Console.WriteLine(reducedTransaction.ToString());
            }

            // Hashtables
            // Hashtables represents a collection of key-value pairs.
            // It is used when you need to access elements by using keys.

            // Declaring hashtables
            Hashtable myHashtable = new Hashtable();

            // Initializing Hashtables
            Hashtable address = new Hashtable();

            address.Add("houseNumber", "15");
            address.Add("street", "Apple");
            address.Add("city", "California");
            address.Add("zip", "19463");

            // Getting the keys of a hashtable
            ICollection addressKeys = address.Keys;

            // Accessing Hashtable Keys
            Console.WriteLine("Result from accessing Hashtable keys:");
            Console.WriteLine(address["street"]);

            Console.WriteLine("Result from Hashtables:");
            foreach(string key in addressKeys)
            {
                Console.WriteLine(address[key]);
            }

        }
    }
}